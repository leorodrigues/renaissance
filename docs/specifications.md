# Specifications

This document describes the technologies used in the build process, build
environment, target environment and project dependencies.

With the exception of the applications under the "tools" group, the project is
known to run well on any of the target system configurations, when built in one
of the "Build Environment" configurations according to the specifications
bellow.

The tools are only supposed to run on linux and windows targets.

## System specifications

**Build Tools**

- IDE: JetBrains Intellij IDEA 2017.2.3, running under Open JDK 64bit
Server VM by JetBrains s.r.o
- Java: Oracle Java 1.8.0_131, HotSpot 64bit Server VM
- Build tool: Gradle 3.2.1 with Groovy 2.1.9

**Operating Systems**

- Linux: Debian GNU/Linux 8.9 (Jessie), Kernel 3.16.0-4-amd64
- Windows: MS(TM) Windows 8.1 x64
- Android: Android 7.0, API-24, ARM EABI v7a (32bit)

**Hardware**

- Mobile Hardware: Qualcomm Snapdragon 614, 8-Core Cortex-A59 (64bit)
- Dell Hardware: Intel Core i5-4200M @2.5GHz, Haswell Mobile, 8GB of memory
- Custom Hardware: Intel Core i7-960 @3.2GHz, Nehalem Desktop, 12GB DDR3 1066

**Environment configurations**

1. Dell Hardware + Linux + Build Tools
    * Target: Dell Hardware + Linux
    * Target: Mobile Hardware + Android
    * Java compatibility (target / source): 1.7 / 1.7

2. Custom Hardware + Windows + Build Tools
    * Target: Custom Hardware + Windows
    * Target: Mobile Hardware + Android
    * Java compatibility (target / source): 1.7 / 1.7

## Dependency specifications

Only noteworthy dependency packages are listed here, and not all projects use
all packages. Refer to the main _build.gradle_ file to see which project
uses which package.

- **Compile dependencies**
    * Jackson 2.8.5 - Only the following packages:
        - Core
        - Annotations
        - Data Bind
    * Jetty 9.2.17.v20160517 - Only the following packages:
        - Server
        - Servlet
        - Servlets
        - Annotations
    * Spring Shell 1.1.0.RELEASE
    * Rhino 1.7R5
    * Log4j 1.2.9

- **Build dependencies**
    * Shadow Jar 1.2.4

- **Test dependencies**
    * JUnit 4.12
    * Groovy 2.4.4
    * Spock 1.0 (Groovy 2.4)
    * SL4J API 1.7.21
    * Objenesis 2.4

- **Code coverage**
    * Jacoco 0.7.9
