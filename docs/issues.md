# ISSUES

## Open

- There is a serious architecture issue: The use of feedback listener is inconsistent.
  It is not clear in the code, in what moments the processes should return feedbacks
  and in what moments a exception throw is better.
- Feedback service must be simplified by means of new FeedBackOutput interface.
- Class "ControlPointImpl" is not locking the configurations before activating
  the environments life cycle methods.
- "FileSystemConfigurationStepSpec" must cleanup after itself.
- The class "RhinoUnit" produces a feedback that goes to waste in the method
  "tryClose". It should be directed to a feedback listener.
- The package names must be reviewed along with class distribution in the
  packages.
- Default unit factory manager implementation on the environment API must be
  thoroughly tested against errors invoking the fabrication lock methods.

## Addressed

- DefaultConfiguration.describe may simplified by letting each entry describe itself.
- Default configuration is allowing declaration of the same variable twice with
  different values without overwriting them.
- New environments are being created with wrong "NAME" variables.
- Current implementation of _config --set NAME=VAL_ does not support values
  containing spaces.
- Configuration save/load commands are not working properly.
- Repository implementations and supporting classes must be moved from the shell
  project to a new "aggregating" project, so the implementations may be shared
  between client applications.
- The project's repository name must be changed on bitbucket.
- The shell tool must be corrected and property tested.