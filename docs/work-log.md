
# The story so far

#### 2017-10-23

Since the last log, the project has been refactored a lot! All references to the
'FeedbackListener' and it's friend classes were removed from the core API
modules. But why the references were there in the first place? Well, the project
is designed with a strong take on understanding what is happening with all the
gears inside. Therefore, almost all processes would use the aforementioned
'FeedbackListener' in order to tell the outside world about the details of each
processing step, yielding the spread of references to said interface.

To deal with this situation, a command framework was constructed where every
single command where ran inside a try-catch clause. A success message was issued
after successful completion of a command and an error message was issued in case
of exception. The 'FeedbackListener' was kept centralized inside de command
runner and the messages where produced by the commands themselves. Complex
algorithms where then redesigned in the form of simple instances of the
command interface and the references to FeedbackListener were all eradicated
from the central classes.

A collateral effect took place, however. The number of command classes has
escalated quickly because the design of the command framework focus on the
execution of the most minute individual tasks, leading to a very fine grained
command network. The effort to maintain the code with this granularity is
is maddening.

In order to reduce granularity of the concrete commands the following tasks must
be done:

- Provide each command with a behavior provided by an abstract class which
allows the concrete classes to report all kinds of messages, without the need to
reference other abstractions. This solves the spreading of undesired references,
because in order to be a command, a class will have to at least implement the
interface 'Command' itself. So it is possible to inherit from an abstract class
instead of implementing the interface, this way the behaviour is accessible to
all implementations.
- Agglutinate various commands into one and provide status information between
steps.

Beyond everything that was exposed until now, there is an issue regarding the
initialization of the control point instance. It is spread all over a number of
factories and initialization commands. The responsibilities mus be reviewed and
several commands will have to be agglutinated. Moreover, the orchestration of
persistence for the 'ConfigurationIndex' is being redesigned.

#### 2017-10-13

In the course of SEVERAL commits, a some what inexpensive architecture has
formed but it needs some grooming, specially in the subject of feedbacks and
feedback listeners.

The highlights are:
- An "application" package has been created to house the system dependent tasks
in such a way that they could be shared between os environments.
- A supports library has been added to group general purpose logic.
- The shell commands were simplified.
- Control point state persistence has been implemented.
- There is a new 'setup' command.
- Coverage reports are now consolidated.

#### 2017-09-21

**During the last weekend, the project has undergone some extensive refactorings**

- UnitName class had it's interface extracted. The default implementation is
called 'CommonUnitName' and the interface reference was used wherever it made
sense to do so. The new class has private constructor and factory methods were
provided to allow the instantiation of new names. Also, the NULL Object pattern
was applied, via the class 'BasicUnitName'.
- DurableRepository class had the low level logic to persist data moved to
another abstraction - UnderLyingStorage. A default implementation of that
interface was implemented: FileSystemUnderlyingStorage.

**Two new features have been added**

- UnitName instantiation using complex string -> Implemented via factory method
"newName(String)" on de API, but not available to the shell yet.
- Listing of the available fabrication tokens -> Implemented in the API and
available in the shell.

#### 2017-09-20

The class "RhinoUnit" produces a feedback that goes to waste in the method
"tryClose". It should be directed to a feedback listener.

#### 2017-09-20

The whole project configuration (except for the shell), is able to successfully
compile and run on android. Platform tested was a Snapdragon 8-Core Cortex-A59
from Qualcomm running Android 7.0 32bit ARMEABI-v7a.

Jetty showed a little problem with the log system which must be properly
configured to be compatible with Android's log.

The package names must be reviewed along with class distribution in the
packages.

Environment API may be largely simplified.

The shell tool must be corrected and property tested.

The project's repository name must be changed on bitbucket.

Default unit factory manager implementation on the environment API must be
thoroughly tested against errors invoking the fabrication lock methods.
