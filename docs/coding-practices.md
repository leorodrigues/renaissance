# CODING PRACTICES

In this project, TDD is not used (not all the time at least), but a "test
centric" and "coverage centric" orientation is taken. All the rules described
here have the objective of improving testability and simplicity.

In the context of unit isolation, each method is considered a single unit. Many
methods have more than one execution path and as much as possible, there should
be one test for each single path, resulting in more than one test per unit.

Class design it oriented to reduce the number of non public and/or static
methods. Private ones cannot be tested independently. Protected ones will need
some sort of surrogate test class deriving from the test subject in order
to be accessed and yet such access would still be indirect. Static methods
figure in this list because they cannot be overridden thus hindering
polymorphism.

Still on the subject of design, it's not completely thought through before hand.
The process consists on achieving an objective (adding a feature or piece of
functionality) and RIGHT after, perform a sequence of refactorings using all the
guidelines described here to improve the code. Test coverage improvement is
performed frequently as well and in repeated iterations guided by a
coverage tool. The goal is to maintain a coverage curve above 90% and leaning
towards 100%.

#### General rules (following the object Calisthenics rules)

1. Only One Level Of Indentation Per Method
2. Don’t Use The ELSE Keyword
3. Wrap All Primitives And Strings
4. First Class Collections
5. One Dot Per Line
6. Don’t Abbreviate
7. Keep All Entities Small
8. No Classes With More Than Two Instance Variables
9. No Getters/Setters/Properties

#### For the java language

- Package schema:
    
    _$organization_ DOT _$product_ (DOT _$layer_)? (DOT _$subject_)?

- Keep constructors simple:
    * **Description:** Constructors must only bind required fields or initialize
    them with default values. Also they must not include try-catch clauses nor
    throw statements. They may include calls to methods on the same class as
    long as those methods follow these same premises.
    * **Objective:** Object instantiation should always succeed.
    
- Model the domain avoiding inheritance:
    * **Description:** I wanted to say it's "Composition over inheritance" but I
    don't exactly follow that pattern. In here, whenever I think of inheritance,
    I try to re-design the application domain introducing new abstractions in
    such a way that responsibility is taken out of a class and placed somewhere
    else to be "shared". In "Composition over inheritance", however, the
    individual behaviors are simply moved as they are to other classes.
    * **Objective:** To reduce the amount of collateral coverage and to make
    tests simpler by dealing only with local requirements (i.e. dealing with
    logic declared only within the same class being tested). In other words,
    it is used to reduce the concern of the test.
    
- Place instantiation constraints in factories / builders:
    * **Description:** It is common for a composite object to have some set of
    rules governing the combination of different parts that could be used
    together in compliance with business domain rules. In such cases those rules
    that constrain the possible combinations, should be implemented by a builder
    or factory. The latter will enable construction abstraction and the former
    will enable indirection.
    * **Objective:** Simplifies the replacement of code during tests (mock) and
    enables independent testing of instantiation constraints and other
    domain logic.
