# PROJECT RENAISSANCE

It is meant to be a development platform to go. In other words a development
platform that you take with you wherever you go.

Designed to run on mobile devices so you carry the development tools in your
pocket or in your backpack.

The idea is to use javascript to write the programs using a web application
interface to access the editor, compiler, etc.

## Desired Features

- Editor, compiler and debugger using a web browser.
- Blue/Green deployment of applications.
- Integrated testing framework and deploy pipeline.
- Control of platform via Android app and ReST interfaces.
- Main programming language: ECMA Script
- Secondary programming language (far future): Clojure
- Cloud backup using git and your preferred files API.

## Current priorities

- Rhino Environment API: Develop the minimal facilities
- Android platform: Design the interface

## Links

- [Plan][4] Simple list of next steps
- [Work log][1] pointing out the highlights of project development.
- [Issues][2] encountered during development or testing.
- [Specifications][3] for build/testing environments and technologies used.
- [Coding practices][4] used in this project.

[1]: "docs/work-log.md" "The story so far..."
[2]: "docs/issues.md" "All things that need resolution."
[3]: "docs/specifications.md" "Build/Test environment specs."
[4]: "docs/plan.md" "Click here for next steps."
[4]: "docs/coding-practives.md" "Possibly a little different from what you think"