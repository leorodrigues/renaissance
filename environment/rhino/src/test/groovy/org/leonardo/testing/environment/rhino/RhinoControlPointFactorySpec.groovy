package org.leonardo.testing.environment.rhino

import org.leonardo.environment.ControlPoint
import org.leonardo.environment.ControlPointFactory
import org.leonardo.environment.ControlPointStateBank
import org.leonardo.environment.rhino.RhinoControlPointFactory
import org.leonardo.environment.rhino.RhinoUnitFactory
import org.leonardo.renaissance.command.SerialCommandRunner
import spock.lang.Specification

import static org.leonardo.environment.rhino.RhinoUnitFactory.FABRICATION_TOKEN

class RhinoControlPointFactorySpec extends Specification {

    def "newControlPointInstance; ok"() {
        given:
        def sequence = Spy(SerialCommandRunner)

        when:
        def result = subject.newControlPointInstance(cpsRepository, sequence)

        then:
        result.is(newControlPoint)

        1 * delegateFactory.newControlPointInstance(
                cpsRepository, sequence) >> newControlPoint

        1 * newControlPoint.registerFactoryForToken(
            { f -> f instanceof RhinoUnitFactory },
            { t -> t.equals(FABRICATION_TOKEN) })
    }

    def delegateFactory

    def newControlPoint

    def cpsRepository

    def subject

    void setup() {
        newControlPoint = Mock(ControlPoint)
        delegateFactory = Mock(ControlPointFactory)
        cpsRepository = Mock(ControlPointStateBank)
        subject = new RhinoControlPointFactory(delegateFactory)
    }
}
