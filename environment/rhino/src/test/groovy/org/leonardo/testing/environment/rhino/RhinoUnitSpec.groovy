package org.leonardo.testing.environment.rhino

import org.leonardo.environment.Resource
import org.leonardo.environment.UnitName
import org.leonardo.environment.ecmascript.ECMAFacility
import org.leonardo.environment.ecmascript.ResourceLoader
import org.leonardo.environment.rhino.RhinoUnit
import org.leonardo.renaissance.io.TerminateCloseable
import spock.lang.Specification

import static org.leonardo.environment.ecmascript.ExposedName.newName

/**
 * Created by Lrteixeira on 12/09/2016.
 */
class RhinoUnitSpec extends Specification {

    def "execute; success"() {
        given:
        def resource = Mock(Resource)

        and:
        def script = "var x = 3;"

        and:
        def scriptInput = new ByteArrayInputStream(script.getBytes())

        when:
        subject.execute(resource)

        then:
        1 * resourceLoader.openResource(resource) >> scriptInput
    }

    def "execute; failure"() {
        given:
        def resource = Mock(Resource)

        when:
        subject.execute(resource)

        then:
        resourceLoader.openResource(resource) >> {
            throw new RuntimeException('Thrown on purpose by an unit test.')
        }
    }

    def "execute; failure; IOException in the middle of stream"() {
        given:
        def resource = Mock(Resource)

        when:
        subject.execute(resource)

        then:
        resourceLoader.openResource(resource) >> new InputStream() {
            int count = 0
            String script = "let someVariable = 10;"
            @Override
            int read() throws IOException {
                if (count == 10)
                    throw new IOException("Thrown on purpose")
                return script.codePointAt(count++)
            }
        }
    }

    def "execute; failure; stream is closed prematurely"() {
        given:
        def resource = Mock(Resource)

        when:
        subject.execute(resource)

        then:
        terminateCloseable.invoke(_) >> { }
        resourceLoader.openResource(resource) >> new InputStream() {
            int count = 0
            String script = "let someVariable = 10;"
            @Override
            int read() throws IOException {
                if (count == 10)
                    throw new IOException("Thrown on purpose (1/2)")
                return script.codePointAt(count++)
            }

            @Override
            void close() throws IOException {
                super.close()
                throw new IOException("Thrown on purpose (2/2)")
            }
        }
    }

    def "addFacility; ok"() {
        given:
        def resource = Mock(Resource)

        and:
        def facility = Mock(ECMAFacility)

        and:
        def dummy = new Dummy()

        and:
        def script = "integers.y = integers.x + 1"

        and:
        def scriptInput = new ByteArrayInputStream(script.getBytes())

        when:
        dummy.y == 3
        subject.addFacility(facility)
        subject.execute(resource)

        then:
        1 * resourceLoader.openResource(resource) >> scriptInput
        1 * facility.getExposedName() >> newName("integers")
        1 * facility.getPublicInstance() >> dummy
    }

    TerminateCloseable terminateCloseable
    ResourceLoader resourceLoader
    RhinoUnit subject

    void setup() {
        terminateCloseable = Mock(TerminateCloseable)
        resourceLoader = Mock(ResourceLoader)
        subject = new RhinoUnit(
                terminateCloseable, Mock(UnitName), resourceLoader)
        subject.allocateContext()
    }

    void cleanup() {
        subject.freeContext()
    }

    class Dummy {
        int x
        int y
    }
}
