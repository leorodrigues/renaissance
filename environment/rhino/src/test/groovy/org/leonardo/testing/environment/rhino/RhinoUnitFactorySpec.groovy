package org.leonardo.testing.environment.rhino

import org.leonardo.environment.BasicUnitName
import org.leonardo.environment.rhino.RhinoUnitFactory
import spock.lang.Specification

class RhinoUnitFactorySpec extends Specification {
    def "create; ok"() {
        given:
        def subject = new RhinoUnitFactory()

        expect:
        subject.create(BasicUnitName.INSTANCE)
    }
}
