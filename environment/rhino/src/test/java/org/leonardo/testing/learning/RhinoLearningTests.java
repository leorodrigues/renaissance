package org.leonardo.testing.learning;

import org.junit.*;
import org.leonardo.testing.util.FixtureResourcesHelper;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import java.io.*;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Leonardo Teixeira on 26/08/2015.
 * Email: leorodriguesrj@gmail.com
 *
 */
public class RhinoLearningTests {

    private static final FixtureResourcesHelper helper;

    private static final String JS_RESOURCE_PATH =
        "/org/leonardo/learning/RhinoLearningTests.js";

    private static final String MESSAGE_RESOURCE_PATH =
        "/org/leonardo/learning/message.txt";

    private static ScriptableObject fixtureScope;

    private static Context fixtureContext;

    private static String jsFilePath;

    private static String messageFilePath;

    private Scriptable testScope;

    private Context testContext;

    static {
        helper = new FixtureResourcesHelper(RhinoLearningTests.class);
    }

    @Test
    public void testRunSimpleScriptReturningString() {
        String script = "'7 + 1 is ' + (7 + 1)";
        String expectedOutput = "7 + 1 is 8";
        Object result = testContext.evaluateString(
            testScope, script, "test", 1, null);
        String actualOutput = Context.toString(result);
        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testRunSimpleScriptReturningArray() {
        String script = "['x', 'y', 'z']";
        String[] expectedArray = new String[] {"x", "y", "z"};
        Object result = testContext.evaluateString(
            testScope, script, "test", 1, null);
        Object actualArray = Context.jsToJava(result, String[].class);
        assertTrue(Arrays.equals(expectedArray, (String[]) actualArray));
    }

    @Test
    public void testRunSimpleScriptReturningPrototype() {
        String script = "function f() { return {'name':'name'} } f()";
        String expectedValue = "name";
        Object result = testContext.evaluateString(
            testScope, script, "test", 1, null);
        Object actualValue = ((NativeObject)result).get("name", testScope);
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void testShareObjectsBetweenScripts() {
        String script1 = "var sum = 7 + 1";
        String script2 = "'Sum is ' + sum";
        String expectedValue = "Sum is 8";
        testContext.evaluateString(testScope, script1, "one", 1, null);
        Object result = testContext.evaluateString(
            testScope, script2, "two", 1, null);
        String actualValue = Context.toString(result);
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void testInjectIntegerInAScope() {
        testScope.put("someInteger", testScope, 10);
        Object result = testContext.evaluateString(
                testScope, "someInteger + 1", "four", 1, null);
        assertEquals(11.0, result);
    }

    @Test
    public void testJavaInteropOpenFile() throws IOException {
        loadFixtureScriptIntoTestScope();
        testScope.put("filePath", testScope, messageFilePath);
        Object result = testContext.evaluateString(
            testScope, "loadMessage(filePath)", "three", 1, null);
        String actualValue = Context.toString(result);
        assertEquals("Hello world!", actualValue);
    }

    @BeforeClass
    public static void beginClass() throws IOException {
        fixtureContext = Context.enter();
        fixtureScope = fixtureContext.initStandardObjects();
        jsFilePath = helper.deployTemporaryResource(JS_RESOURCE_PATH);
        messageFilePath = helper.deployTemporaryResource(MESSAGE_RESOURCE_PATH);
    }

    @AfterClass
    public static void endClass() {
        Context.exit();
    }

    @Before
    public void beginTest() {
        testContext = Context.enter();
        testScope = testContext.initStandardObjects(fixtureScope);
    }

    @After
    public void endTest() {
        Context.exit();
    }

    private void loadFixtureScriptIntoTestScope() throws IOException {
        Reader reader = createReader(jsFilePath);
        testContext.evaluateReader(testScope, reader, "three", 1, null);
        reader.close();
    }

    private Reader createReader(String path) throws FileNotFoundException {
        return new InputStreamReader(new FileInputStream(path));
    }
}
