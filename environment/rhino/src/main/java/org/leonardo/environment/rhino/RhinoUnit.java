package org.leonardo.environment.rhino;

import org.leonardo.environment.Resource;
import org.leonardo.environment.ResourceExecutionException;
import org.leonardo.environment.UnitName;
import org.leonardo.environment.ecmascript.ECMAFacility;
import org.leonardo.environment.ecmascript.ECMAUnit;
import org.leonardo.environment.ecmascript.ResourceLoader;
import org.leonardo.renaissance.io.TerminateCloseable;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Lrteixeira on 12/09/2016.
 */
public class RhinoUnit extends ECMAUnit {

    private Scriptable scope;

    private final TerminateCloseable terminateCloseable;

    public RhinoUnit(
            TerminateCloseable terminateCloseable,
            UnitName unitName,
            ResourceLoader resourceLoader) {
        super(unitName, resourceLoader);
        this.terminateCloseable = terminateCloseable;
    }

    @Override
    public void addFacility(ECMAFacility facility) {
        try {
            String exposedName = facility.getExposedName().asString();
            Object publicInstance = facility.getPublicInstance();
            Context.enter();
            Object wrappedInstance = Context.javaToJS(publicInstance, scope);
            ScriptableObject.putProperty(scope, exposedName, wrappedInstance);
        } finally {
            Context.exit();
        }
    }

    @Override
    public void execute(Resource resource) throws ResourceExecutionException {
        InputStreamReader inputReader = null;
        Context ctx = Context.enter();
        try {
            InputStream inputStream =
                    getResourceLoader().openResource(resource);
            inputReader = new InputStreamReader(inputStream);
            ctx.evaluateReader(scope, inputReader, "", 0, null);
        } catch (Exception exception) {
            terminateCloseable.invoke(inputReader);
        } finally {
            Context.exit();
        }
    }

    @Override
    public void allocateContext() {
        Context ctx = Context.enter();
        scope = ctx.initStandardObjects();
        Context.exit();
    }

    @Override
    public void freeContext() {
        scope = null;
    }
}
