package org.leonardo.environment.rhino;

import org.leonardo.environment.config.Configuration;
import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.ControlPointFactory;
import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.renaissance.command.SerialCommandRunner;

import static org.leonardo.environment.rhino.RhinoUnitFactory.FABRICATION_TOKEN;

public class RhinoControlPointFactory implements ControlPointFactory {

    private ControlPointFactory delegate;

    public RhinoControlPointFactory(ControlPointFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public Configuration getDefaultConfiguration() {
        return delegate.getDefaultConfiguration();
    }

    @Override
    public void setDefaultConfiguration(Configuration defaultConfiguration) {
        delegate.setDefaultConfiguration(defaultConfiguration);
    }

    @Override
    public ControlPoint newControlPointInstance(
            ControlPointStateBank stateRepository,
            SerialCommandRunner serialCommandRunner) {
        ControlPoint result = delegate.newControlPointInstance(
                stateRepository, serialCommandRunner);
        RhinoUnitFactory unitFactory = new RhinoUnitFactory();
        result.registerFactoryForToken(unitFactory, FABRICATION_TOKEN);
        return result;
    }
}
