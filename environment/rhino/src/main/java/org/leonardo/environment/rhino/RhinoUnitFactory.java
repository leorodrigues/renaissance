package org.leonardo.environment.rhino;

import org.leonardo.environment.ControlledUnit;
import org.leonardo.environment.UnitFactory;
import org.leonardo.environment.UnitName;
import org.leonardo.environment.ecmascript.LocalResourceLoader;
import org.leonardo.renaissance.io.TryClosingOrFail;

/**
 * Created by leonardo on 14/10/2015.
 */
public class RhinoUnitFactory implements UnitFactory {

    public static final String FABRICATION_TOKEN = "rhino-basic";

    @Override
    public ControlledUnit create(UnitName name) {
        return new RhinoUnit(
                TryClosingOrFail.INSTANCE, name, new LocalResourceLoader());
    }
}
