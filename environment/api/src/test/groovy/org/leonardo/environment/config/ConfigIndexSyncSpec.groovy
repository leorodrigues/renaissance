package org.leonardo.environment.config

import org.leonardo.environment.UnitName
import org.leonardo.renaissance.command.CommandChainRunner
import org.leonardo.renaissance.command.SerialCommandRunner
import spock.lang.Specification

import static org.leonardo.renaissance.command.CompletionLevel.FULL

class ConfigIndexSyncSpec extends Specification {
    def "load; ok; nothing to load"() {
        given:
        Configurations configurations = Spy(Configurations)

        when:
        subject.invoke(configurationIndex, configurations)

        then:
        1 * configurationIndex.names >> []
        1 * configurations.keySet() >> []
        1 * serialCommandRunner.run({ c -> c == [] }) >> FULL
    }

    def "load; ok; one name to load"() {
        given:
        Configurations configurations = Spy(Configurations)

        and:
        def name = Mock(UnitName)

        when:
        subject.invoke(configurationIndex, configurations)

        then:
        1 * configurationIndex.names >> []
        1 * configurations.keySet() >> [name]
        1 * configurations.get(name) >> Mock(Configuration)
        1 * serialCommandRunner.run({ c ->
            isLoadSingleConfiguration(c[0]) }) >> FULL
    }

    def "load; ok; one name to clear"() {
        given:
        Configurations configurations = Spy(Configurations)

        and:
        def name = Mock(UnitName)

        when:
        subject.invoke(configurationIndex, configurations)

        then:
        1 * configurationIndex.names >> [name]
        1 * configurations.keySet() >> []
        1 * serialCommandRunner.run({ c ->
            isClearSingleConfiguration(c[0]) }) >> FULL
    }

    def "load; ok; one name to reload"() {
        given:
        Configurations configurations = Spy(Configurations)

        and:
        def name = Mock(UnitName)

        when:
        subject.invoke(configurationIndex, configurations)

        then:
        1 * configurationIndex.names >> [name]
        1 * configurations.keySet() >> [name]
        1 * configurations.get(name) >> Mock(Configuration)
        1 * serialCommandRunner.run({ c ->
            isReloadConfiguration(c[0]) }) >> FULL
    }

    def serialCommandRunner
    def commandChainRunner
    def configurationIndex
    def subject

    static boolean isReloadConfiguration(c) {
        return isClearSingleConfiguration(c) && \
            isLoadSingleConfiguration(c.next.get())
    }

    static boolean isLoadSingleConfiguration(c) {
        return c instanceof SetSingleConfig
    }

    static boolean isClearSingleConfiguration(c) {
        return c instanceof ClearSingleConfig
    }

    void setup() {
        commandChainRunner = Mock(CommandChainRunner)
        configurationIndex = Mock(ConfigurationIndex)
        serialCommandRunner = Spy(SerialCommandRunner,
                constructorArgs: [commandChainRunner])

        subject = new ConfigIndexSyncImpl(serialCommandRunner)
    }
}
