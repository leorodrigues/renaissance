package org.leonardo.environment.config

import org.leonardo.environment.ControlPointStateBank
import spock.lang.Specification

class CommitChangesSpec extends Specification {
    def "execute; ok; orchestration occurs"() {
        when:
        subject.execute()

        then:
        1 * controlPointStateBank.commit()
    }

    def "execute; error; exception occurs and is rethrown"() {
        given:
        def e = new RuntimeException('thrown on purpose')

        when:
        subject.execute()

        then:
        thrown(RuntimeException)
        1 * controlPointStateBank.commit() >> { throw e }
    }

    def subject
    def controlPointStateBank

    void setup() {
        controlPointStateBank = Mock(ControlPointStateBank)
        subject = new CommitChanges(controlPointStateBank)
    }
}
