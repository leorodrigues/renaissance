package org.leonardo.environment.config

import org.leonardo.environment.ControlPointStateBank
import org.leonardo.environment.UnitName
import spock.lang.Specification

class UpdateConfigSpec extends Specification {
    def "execute; ok; orchestration occurs"() {
        given:
        def configuration = Mock(Configuration)

        when:
        subject.execute()

        then:
        1 * configurationIndex.getConfigurationForName(unitName) >> configuration
        1 * controlPointStateBank.delete(unitName)
        1 * controlPointStateBank.insert(unitName, configuration)
    }

    def "execute; error; exception occurs"() {
        given:
        def configuration = Mock(Configuration)

        and:
        def e = new RuntimeException('thrown on purpose')

        when:
        subject.execute()

        then:
        thrown(ConfigSyncException)
        1 * configurationIndex.getConfigurationForName(unitName) >> configuration
        1 * controlPointStateBank.insert(unitName, configuration) >> { throw e }
    }

    def "getSuccessMessage; ok"() {
        when:
        def msg = subject.getSuccessMessage()

        then:
        1 * unitName.getDisplayString() >> 'x'
        msg == 'UpdateConfig - terminated - success - x'
    }

    def subject
    def unitName
    def configurationIndex
    def controlPointStateBank

    void setup() {
        controlPointStateBank = Mock(ControlPointStateBank)
        configurationIndex = Mock(ConfigurationIndex)
        unitName = Mock(UnitName)
        subject = new UpdateConfig(
            controlPointStateBank, unitName, configurationIndex)
    }
}
