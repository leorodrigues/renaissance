package org.leonardo.environment.config

import spock.lang.Specification

class ConfigEntrySpec extends Specification {

    def "getName; ok;"() {
        expect:
        new ConfigEntry('y', '132134').name == 'y'
    }

    def "getValue; ok;"() {
        expect:
        new ConfigEntry('x', '112358').value == '112358'
    }

    def "getExpandedValue; ok;"() {
        expect:
        new ConfigEntry('z', '5589', 'x').expandedValue == 'x'
    }

    def "isExpanded; true;"() {
        expect:
        new ConfigEntry('w', 'a', 'b').expanded
    }

    def "isExpanded; false; no expanded value is given"() {
        expect:
        !new ConfigEntry('r', 'b').expanded
    }

    def "isExpanded; false; expanded value is empty string"() {
        expect:
        !new ConfigEntry('s', 'c', '').expanded
    }

    def "makeCopy; ok; copies everything"() {
        when:
        def copy = new ConfigEntry('t', 'd', '@').makeCopy()

        then:
        copy.name == 't'
        copy.value == 'd'
        copy.expandedValue == '@'
    }

    def "hashCode; ok; changes if name is different"() {
        given:
        def one = new ConfigEntry('a', 'b', 'c')
        def two = new ConfigEntry('x', 'b', 'c')

        expect:
        one.hashCode() != two.hashCode()
    }

    def "hashCode; ok; changes if expanded value is different"() {
        given:
        def one = new ConfigEntry('z', 'y', null)
        def two = new ConfigEntry('z', 'y', 'z')

        expect:
        one.hashCode() != two.hashCode()
    }

    def "hashCode; ok; changes if value is different"() {
        given:
        def one = new ConfigEntry('r', 's', 't')
        def two = new ConfigEntry('r', 'F', 't')

        expect:
        one.hashCode() != two.hashCode()
    }

    def "getEffectiveValue; ok; it is value if not expanded"() {
        given:
        def subject = new ConfigEntry('r', 's')

        expect:
        subject.value == subject.effectiveValue
    }

    def "getEffectiveValue; ok; it is expandedValue if expanded"() {
        given:
        def subject = new ConfigEntry('r', 's', 't')

        expect:
        subject.expandedValue == subject.effectiveValue
    }

    def "describe; ok; non expanded"() {
        given:
        def descriptor = Mock(ConfigurationDescriptionBuilder)

        when:
        new ConfigEntry('a', 'b').describe(descriptor)

        then:
        1 * descriptor.append('a', 'b')
    }

    def "describe; ok; expanded"() {
        given:
        def descriptor = Mock(ConfigurationDescriptionBuilder)

        when:
        new ConfigEntry('a', 'b', 'c').describe(descriptor)

        then:
        1 * descriptor.append('a', 'b', 'c')
    }

    def "equals; true; object is itself"() {
        given:
        def subject = new ConfigEntry('a', 'b')

        expect:
        subject.equals(subject)
    }

    def "equals; true; another non expanded instance"() {
        given:
        def subject = new ConfigEntry('k', 'v')

        expect:
        subject.equals(new ConfigEntry('k', 'v'))
    }

    def "equals; true; another expanded instance"() {
        given:
        def subject = new ConfigEntry('p', 'k', 'y')

        expect:
        subject.equals(new ConfigEntry('p', 'k', 'y'))
    }

    def "equals; false; the other is not a config entry"() {
        expect:
        !new ConfigEntry('a', 'b').equals(new Object())
    }

    def "equals; false; the other has expanded value"() {
        given:
        def subject = new ConfigEntry('a', 'b')

        expect:
        !subject.equals(new ConfigEntry('a', 'b', 'c'))
    }

    def "equals; false; the subject has expanded value"() {
        given:
        def subject = new ConfigEntry('a', 'b', 'p')

        expect:
        !subject.equals(new ConfigEntry('a', 'b'))
    }

    def "equals; false; differ by expanded value"() {
        given:
        def subject = new ConfigEntry('a', 'b', 'p')

        expect:
        !subject.equals(new ConfigEntry('a', 'b', 'k'))
    }

    def "equals; false; differ by value"() {
        given:
        def subject = new ConfigEntry('a', '2')

        expect:
        !subject.equals(new ConfigEntry('a', '1'))
    }

    def "equals; false; differ by name"() {
        given:
        def subject = new ConfigEntry('h', '2')

        expect:
        !subject.equals(new ConfigEntry('l', '2'))
    }
}
