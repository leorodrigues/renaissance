package org.leonardo.environment.config

import org.leonardo.environment.ControlPointStateBank
import org.leonardo.environment.UnitName
import org.leonardo.renaissance.command.SerialCommandRunner
import spock.lang.Specification

import static org.leonardo.renaissance.command.CompletionLevel.FULL

class ConfigBankSyncImplSpec extends Specification {

    def "update; ok; empty configuration and empty cp state bank"() {
        when:
        subject.invoke(controlPointStateBank, configurationIndex)

        then:
        1 * configurationIndex.getNames() >> []
        1 * controlPointStateBank.allUnitNames() >> []
        1 * serialCommandRunner.run({ c -> c == [] }) >> FULL
    }

    def "update; ok; just one insert"() {
        given:
        def unitName = Mock(UnitName)

        when:
        subject.invoke(controlPointStateBank, configurationIndex)

        then:
        1 * configurationIndex.getNames() >> [unitName]
        1 * controlPointStateBank.allUnitNames() >> []
        1 * serialCommandRunner.run({ c ->
            c[0] instanceof InsertConfig
        }) >> FULL
    }

    def "update; ok; just one remove"() {
        given:
        def unitName = Mock(UnitName)

        when:
        subject.invoke(controlPointStateBank, configurationIndex)

        then:
        1 * configurationIndex.getNames() >> []
        1 * controlPointStateBank.allUnitNames() >> [unitName]
        1 * serialCommandRunner.run({ c ->
            c[0] instanceof DeleteConfig
        }) >> FULL
    }

    def "update; ok; just one update"() {
        given:
        def unitName = Mock(UnitName)

        when:
        subject.invoke(controlPointStateBank, configurationIndex)

        then:
        1 * configurationIndex.getNames() >> [unitName]
        1 * controlPointStateBank.allUnitNames() >> [unitName]
        1 * serialCommandRunner.run({ c ->
            c[0] instanceof UpdateConfig
        }) >> FULL
    }

    def subject
    def configurationIndex
    def serialCommandRunner
    def controlPointStateBank

    void setup() {
        controlPointStateBank = Mock(ControlPointStateBank)
        serialCommandRunner = Mock(SerialCommandRunner)
        configurationIndex = Mock(ConfigurationIndex)
        subject = new ConfigBankSyncImpl(serialCommandRunner)
    }
}
