package org.leonardo.environment.config

import org.leonardo.environment.UnitName
import spock.lang.Specification

class ClearSingleConfigSpec extends Specification {

    def "execute; ok"() {
        when:
        subject.execute()

        then:
        1 * configurationIndex.disposeOfName(unitName)
    }

    def "execute; error"() {
        when:
        subject.execute()

        then:
        thrown(ConfigSyncException)
        1 * configurationIndex.disposeOfName(unitName) >> {
            throw new RuntimeException('thrown on purpose')
        }
    }

    def "getSuccessMessage; ok"() {
        when:
        def msg = subject.getSuccessMessage()

        then:
        msg == 'ClearSingleConfig - terminated - success - x'
        1 * unitName.getDisplayString() >> 'x'
    }

    def subject
    def unitName
    def configurationIndex

    void setup() {
        unitName = Mock(UnitName)
        configurationIndex = Mock(ConfigurationIndex)
        subject = new ClearSingleConfig(configurationIndex, unitName)
    }
}
