package org.leonardo.environment.config

import org.leonardo.environment.ControlPointStateBank
import org.leonardo.environment.UnitName
import spock.lang.Specification

class DeleteConfigSpec extends Specification {
    def "execute; ok; orchestration occurs"() {
        when:
        subject.execute()

        then:
        1 * controlPointStateBank.delete(unitName)
    }

    def "execute; error; exception occurs"() {
        given:
        def e = new RuntimeException('thrown on purpose')

        when:
        subject.execute()

        then:
        thrown(ConfigSyncException)
        1 * controlPointStateBank.delete(unitName) >> { throw e }
    }

    def "getSuccessMessage; ok"() {
        when:
        def msg = subject.getSuccessMessage()

        then:
        1 * unitName.getDisplayString() >> 'x'
        msg == 'DeleteConfig - terminated - success - x'
    }

    def subject
    def unitName
    def controlPointStateBank

    void setup() {
        controlPointStateBank = Mock(ControlPointStateBank)
        unitName = Mock(UnitName)
        subject = new DeleteConfig(
                controlPointStateBank, unitName)
    }
}
