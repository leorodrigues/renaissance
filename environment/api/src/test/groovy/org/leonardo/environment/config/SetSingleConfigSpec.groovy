package org.leonardo.environment.config

import org.leonardo.environment.UnitName
import spock.lang.Specification

class SetSingleConfigSpec extends Specification {

    def "execute; ok; new config is created as copy of loaded config"() {
        given:
        def newConfig = Mock(Configuration)

        when:
        subject.execute()

        then:
        1 * configurationIndex.declareName(unitName) >> newConfig
        1 * newConfig.copyFrom(configuration) >> { }
    }

    def "execute; error; some error during declare"() {
        when:
        subject.execute()

        then:
        thrown(ConfigSyncException)
        1 * configurationIndex.declareName(unitName) >> {
            throw new RuntimeException('thrown on purpose')
        }
    }

    def "getSuccessMessage; ok"() {
        when:
        def msg = subject.getSuccessMessage()

        then:
        msg == 'SetSingleConfig - terminated - success - x'
        1 * unitName.getDisplayString() >> 'x'
    }

    def subject
    def unitName
    def configuration
    def configurationIndex

    void setup() {
        unitName = Mock(UnitName)
        configuration = Mock(Configuration)
        configurationIndex = Mock(ConfigurationIndex)
        subject = new SetSingleConfig(
                configurationIndex, unitName, configuration)
    }


}
