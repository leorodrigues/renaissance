package org.leonardo.testing.unity.environment

import org.leonardo.environment.BasicUnitName
import spock.lang.Specification

import static org.leonardo.environment.CommonUnitName.newName

class BasicUnitNameSpec extends Specification {
    def "equals; true; instance against itself"() {
        expect:
        BasicUnitName.INSTANCE.equals(BasicUnitName.INSTANCE)
    }

    def "equals; true; instance against an equivalent name"() {
        given:
        def equivalentName = newName("basic")

        expect:
        BasicUnitName.INSTANCE.equals(equivalentName)
    }

    def "equals; false; instance against a different name"() {
        given:
        def another = newName("different")

        expect:
        !BasicUnitName.INSTANCE.equals(another)
    }
}
