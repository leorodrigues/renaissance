package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import spock.lang.Specification
import spock.lang.Subject

import static UnitState.DOWN
import static UnitState.RUNNING

/**
 * Created by Lrteixeira on 03/02/2016.*/
@Subject(UnitInstancesImpl)
@SuppressWarnings("GroovyAssignabilityCheck")
class UnitInstancesImplSpec extends Specification {

    def "toArray, ok"() {
        given:
        UnitInstances subject = new UnitInstancesImpl(unitFactoryManager)

        and:
        ControlledUnit someUnit = Mock(ControlledUnit)

        and:
        subject.add(someUnit)

        when:
        ControlledUnit[] subjectAsArray = subject.toArray()

        then:
        subjectAsArray.length == 1

        and:
        subjectAsArray[0] == someUnit
    }

    def "areAllDown, true, they are down"() {
        given:
        testSubject.add(Mock(ControlledUnit) {
            getState() >> DOWN
        })

        expect:
        testSubject.areAllDown()
    }

    def "areAllDown, false, one is running"() {
        given:
        testSubject.add(Mock(ControlledUnit) {
            getState() >> DOWN
        })

        and:
        testSubject.add(Mock(ControlledUnit) {
            getState() >> RUNNING
        })

        expect:
        !testSubject.areAllDown()
    }

    def "findByNameOrFail, ok, unit is found"() {
        given:
        def name = Mock(UnitName)

        and:
        ControlledUnit oneUnit = Mock(ControlledUnit) {
            getName() >> name
        }

        and:
        testSubject.add(oneUnit)

        when:
        ControlledUnit result = testSubject.findByNameOrFail(name)

        then:
        result == oneUnit
    }

    def "findByNameOrFail, error, unit is not found"() {
        when:
        testSubject.findByNameOrFail(Mock(UnitName))

        then:
        thrown(IllegalStateException)
    }

    def "findByNameOrCreate, ok, unit is found"() {
        given:
        def name = Mock(UnitName)

        and:
        ControlledUnit oneUnit = Mock(ControlledUnit) {
            getName() >> name
        }

        and:
        testSubject.add(oneUnit)

        when:
        ControlledUnit result = testSubject.findByNameOrCreate(name)

        then:
        result == oneUnit
    }

    def "findByNameOrCreate, ok, unit is not found"() {
        given:
        def name = Mock(UnitName)

        and:
        UnitFactory factory = Mock(UnitFactory)

        and:
        ControlledUnit oneUnit = Mock(ControlledUnit)

        when:
        ControlledUnit result = testSubject.findByNameOrCreate(name)

        then:
        1 * unitFactoryManager.getFactoryInstanceWithToken(_) >> factory
        1 * factory.create(name) >> oneUnit
        result == oneUnit
    }

    def "remove, ok"() {
        given:
        def name = Mock(UnitName)

        and:
        ControlledUnit oneUnit = Mock(ControlledUnit) {
            getName() >> name
        }

        and:
        testSubject.add(oneUnit)

        when:
        boolean success = testSubject.remove(oneUnit)

        then:
        success

        and:
        !testSubject.isUnitAvailable(name)
    }

    def "add, ok"() {
        given:
        ControlledUnit oneUnit = Mock(ControlledUnit)

        when:
        testSubject.add(oneUnit)

        then:
        testSubject.iterator().hasNext()
    }

    def "findByName, ok, looking for an unit that is in the list"() {
        given:
        def name = Mock(UnitName)

        and:
        testSubject.add(Mock(ControlledUnit) {
            getName() >> name
        })

        expect:
        testSubject.findByName(name) != null
    }

    def "findByName, ok, looking for an unit that's not in the list"() {
        given:
        def name = Mock(UnitName)

        and:
        testSubject.add(Mock(ControlledUnit) {
            getName() >> name
        })

        expect:
        testSubject.findByName(Mock(UnitName)) == null
    }

    def "findByName, ok, looking for an unit in an empty list"() {
        expect:
        testSubject.findByName(Mock(UnitName)) == null
    }

    def "isUnitAvailable, true, the unit has been found"() {
        given:
        def name = Mock(UnitName)

        and:
        testSubject.add(Mock(ControlledUnit) {
            getName() >> name
        })

        expect:
        testSubject.isUnitAvailable(name)
    }

    def "isUnitAvailable, false, the unit has not been found"() {
        given:
        def name = Mock(UnitName)

        and:
        testSubject.add(Mock(ControlledUnit) {
            getName() >> name
        })

        expect:
        !testSubject.isUnitAvailable(Mock(UnitName))
    }

    def "isUnitAvailable, false, the list is empty"() {
        expect:
        !testSubject.isUnitAvailable(Mock(UnitName))
    }

    def "makeCopy, ok"() {
        given:
        ControlledUnit someEnv = Mock(ControlledUnit)
        testSubject.add(someEnv)

        when:
        UnitInstances copy = testSubject.makeCopy()

        then:
        copy != testSubject
    }

    def "allRunning, ok"() {
        given:
        def name1 = Mock(UnitName)

        and:
        def name2 = Mock(UnitName)

        and:
        ControlledUnit one = Mock(ControlledUnit) {
            getState() >> DOWN
            getName() >> name1
        }
        testSubject.add(one)

        and:
        ControlledUnit two = Mock(ControlledUnit) {
            getState() >> RUNNING
            getName() >> name2
        }
        testSubject.add(two)

        when:
        UnitInstances newInstances = testSubject.allRunning()

        then:
        !newInstances.isUnitAvailable(name1)

        and:
        newInstances.isUnitAvailable(name2)
    }

    def "getNames, ok"() {
        given:
        def name = Mock(UnitName)

        and:
        ControlledUnit one = Mock(ControlledUnit) {
            getName() >> name
        }
        testSubject.add(one)

        when:
        def result = testSubject.getNames()

        then:
        result[0] == name
    }

    private UnitInstancesImpl testSubject
    private UnitFactoryManager unitFactoryManager

    void setup() {
        unitFactoryManager = Mock(UnitFactoryManager)

        testSubject = new UnitInstancesImpl(unitFactoryManager)
    }
}
