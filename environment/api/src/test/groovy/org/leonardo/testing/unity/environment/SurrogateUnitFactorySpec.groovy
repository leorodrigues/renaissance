package org.leonardo.testing.unity.environment

import org.leonardo.environment.ControlledUnit
import org.leonardo.environment.SurrogateUnitFactory
import org.leonardo.environment.UnitName
import spock.lang.Specification
import spock.lang.Subject

/**
 * Created by Lrteixeira on 04/02/2016.*/
@Subject(SurrogateUnitFactory)
@SuppressWarnings("GroovyAssignabilityCheck")
class SurrogateUnitFactorySpec extends Specification {
        def "create, ok, regular unit"() {
        given:
        def name = Mock(UnitName)

        and:
        SurrogateUnitFactory factory = new SurrogateUnitFactory()

        when:
        ControlledUnit e = factory.create(name)

        then:
        e.name.equals(name)
    }
}
