package org.leonardo.testing.unity.environment

import org.leonardo.environment.config.Configuration
import org.leonardo.environment.ControlPoint
import org.leonardo.environment.ControlPointFactoryImpl
import org.leonardo.environment.ControlPointStateBank
import org.leonardo.renaissance.command.SerialCommandRunner
import spock.lang.Specification

/**
 * Created by Lrteixeira on 22/03/2016.*/
@SuppressWarnings("GroovyAssignabilityCheck")
class ControlPointFactoryImplSpec extends Specification {

    def "set/get defaultConfiguration; ok"() {
        given:
        def config = Mock(Configuration)

        and:
        subject.defaultConfiguration = config

        expect:
        subject.defaultConfiguration.is(config)
    }

    def "newControlPointInstance; ok"() {
        given:
        def instance = subject.newControlPointInstance(
                Mock(ControlPointStateBank), Spy(SerialCommandRunner))

        expect:
        instance instanceof ControlPoint
    }

    private ControlPointFactoryImpl subject

    void setup() {
        subject = Spy(ControlPointFactoryImpl)
    }
}
