package org.leonardo.testing.unity.environment.ecmascript

import spock.lang.Specification

import static org.leonardo.environment.ecmascript.ExposedName.newName

class ExposedName extends Specification {
    def "equals; true; different instances with same internal value"() {
        expect:
        newName('someName').equals(newName('someName'))
    }

    def "equals; true; same instance"() {
        given:
        def subject = newName('theName')

        expect:
        subject.equals(subject)
    }

    def "equals; false; other reference is null"() {
        expect:
        !newName('someName').equals(null)
    }

    def "equals; false; other reference is something else altogether"() {
        expect:
        !newName('someName').equals(new Object())
    }

    def "equals; false; different instances with different internal values"() {
        expect:
        !newName('someName').equals(newName('anotherName'))
    }

    def "hashCode; success; it is the same as the internal value"() {
        expect:
        newName('theName').hashCode() == 'theName'.hashCode()
    }

    def "asString; success; it holds exactly the input value"() {
        expect:
        newName('oneName').asString() == 'oneName'
    }

    def "newName; success; it returns a new instance if value is valid"() {
        expect:
        newName('goodName') != null
    }

    def "newName; exception; throws exception if value is invalid"() {
        when:
        newName('bad >name')

        then:
        thrown(IllegalArgumentException)
    }
}
