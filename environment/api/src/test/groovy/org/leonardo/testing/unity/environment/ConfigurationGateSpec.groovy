package org.leonardo.testing.unity.environment

import org.leonardo.environment.UnitName
import org.leonardo.environment.ValueExpander
import org.leonardo.environment.config.ConfigEntry
import org.leonardo.environment.config.ConfigLockedException
import org.leonardo.environment.config.Configuration
import org.leonardo.environment.config.ConfigurationGate
import spock.lang.Specification

/**
 * Created by leonardo on 19/03/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class ConfigurationGateSpec extends Specification {

    def "getEntries; ok; just delegate"() {
        given:
        def e = [] as Set<ConfigEntry>

        when:
        def entries = subject.getEntries()

        then:
        entries.is(e)
        1 * delegate.getEntries() >> e
    }

    def "copyFrom; ok; gate is not locked"() {
        given:
        def another = Mock(Configuration)

        when:
        subject.copyFrom(another)

        then:
        delegate.copyFrom(another)
    }

    def "copyFrom; error; gate is locked"() {
        given:
        def another = Mock(Configuration)

        and:
        subject.lock()

        when:
        subject.copyFrom(another)

        then:
        thrown(ConfigLockedException)
        0 * delegate.copyFrom(_)
    }

    def "setValueExpander; exception; this class don't change expanders"() {
        when:
        subject.setValueExpander(Mock(ValueExpander))

        then:
        thrown(UnsupportedOperationException)
    }

    def "unset; ok; gate is not locked"() {
        when:
        subject.unset('x')

        then:
        delegate.unset('x')
    }

    def "unset; error; gate is locked"() {
        given:
        subject.lock()

        when:
        subject.unset('x')

        then:
        thrown(ConfigLockedException)
        0 * delegate.unset(_)
    }

    def "set; success; if the gate is unlocked, the delegate is called"() {
        given: "Making sure the subject instance is unlocked"
        subject.unlock()

        when:
        subject.set('var', 'value')

        then:
        1 * delegate.set('var', 'value') >> { }
    }

    def "set; exception; delegate remains unaltered if gate is locked"() {
        given: "Making sure the subject instance is locked"
        subject.lock()

        when:
        subject.set('var', 'value')

        then:
        thrown(ConfigLockedException)
        0 * delegate.set(*_) >> { }
    }

    def "get; ok; just delegate"() {
        when:
        def value = subject.get('z')

        then:
        value == 'x'
        1 * delegate.get('z') >> 'x'
    }

    def "makeCopy; ok"() {
        when:
        subject.makeCopy(Mock(UnitName))

        then:
        thrown(UnsupportedOperationException)
    }

    def "describe; ok;"() {
        when:
        subject.describe(null)

        then:
        delegate.describe(null)
    }

    def "lock; ok"() {
        when:
        subject.lock()

        then:
        subject.locked
    }

    def "unlock; ok"() {
        when:
        subject.unlock()

        then:
        !subject.locked
    }

    private ConfigurationGate subject

    private Configuration delegate

    void setup() {
        delegate = Mock(Configuration)
        subject = new ConfigurationGate(delegate)
    }
}
