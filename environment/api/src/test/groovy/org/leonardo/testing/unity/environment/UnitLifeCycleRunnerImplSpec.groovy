package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import spock.lang.Specification
import spock.lang.Subject

import static org.leonardo.environment.UnitState.*

/**
 * Created by leonardo on 06/02/2016.
 */
@Subject(UnitLifeCycleRunnerImpl)
@SuppressWarnings("GroovyAssignabilityCheck")
class UnitLifeCycleRunnerImplSpec extends Specification {

    def "tryShuttingDown; ok; unit is instantiated"() {
        when:
        subject.tryShuttingDown(unit)

        then:
        1 * unit.getState() >> INSTANTIATED
        0 * unit.runShutdownSequence() >> { throw new RuntimeException() }
    }

    def "tryShuttingDown; ok; unit is already down"() {
        when:
        subject.tryShuttingDown(unit)

        then:
        1 * unit.getState() >> DOWN
        0 * unit.runShutdownSequence() >> { throw new RuntimeException() }
    }

    def "tryShuttingDown; ok; unit is running"() {
        when:
        subject.tryShuttingDown(unit)

        then:
        1 * unit.getState() >> RUNNING
        1 * unit.runShutdownSequence() >> true
    }

    def "tryShuttingDown; error; exception is thrown"() {
        when:
        subject.tryShuttingDown(unit)

        then:
        thrown(UnitLifeCycleControlException)
        1 * unit.getName() >> Mock(UnitName)
        1 * unit.getState() >> RUNNING
        1 * unit.runShutdownSequence() >> { throwRuntimeException() }
    }

    def "tryBooting; ok; unit is instantiated"() {
        when:
        subject.tryBooting(unit)

        then:
        1 * unit.getState() >> INSTANTIATED
        0 * unit.runShutdownSequence() >> { throw new RuntimeException() }
    }

    def "tryBooting; ok; unit is already running"() {
        when:
        subject.tryBooting(unit)

        then:
        1 * unit.getState() >> RUNNING
        0 * unit.runBootSequence() >> { throw new RuntimeException() }
        0 * unit.runShutdownSequence() >> { throw new RuntimeException() }
    }

    def "tryBooting; ok; unit is down"() {
        when:
        subject.tryBooting(unit)

        then:
        1 * unit.getState() >> DOWN
        1 * unit.runBootSequence() >> true
        0 * unit.runShutdownSequence() >> { throw new RuntimeException() }
    }

    def "tryBooting; error; boot sequence fails"() {
        when:
        subject.tryBooting(unit)

        then:
        thrown(UnitLifeCycleControlException)
        1 * unit.getState() >> DOWN
        1 * unit.runBootSequence() >> { throwRuntimeException() }
        1 * unit.getName() >> Mock(UnitName) { getDisplayString() >> 'x' }
        0 * unit.runShutdownSequence() >> { throw new RuntimeException() }
    }

    private UnitLifeCycleRunnerImpl subject
    private ControlledUnit unit

    void setup() {
        unit = Mock(ControlledUnit)
        subject = Spy(UnitLifeCycleRunnerImpl)
    }

    private static void throwRuntimeException() {
        throw new RuntimeException("thrown on purpose by an unity test")
    }
}
