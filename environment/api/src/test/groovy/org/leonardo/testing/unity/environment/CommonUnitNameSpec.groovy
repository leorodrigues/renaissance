package org.leonardo.testing.unity.environment

import org.leonardo.environment.CommonUnitName
import spock.lang.Specification
import spock.lang.Subject

import static org.leonardo.environment.CommonUnitName.newName

/**
 * Created by Lrteixeira on 03/02/2016.*/
@Subject(CommonUnitName)
@SuppressWarnings("GroovyAssignabilityCheck")
class CommonUnitNameSpec extends Specification {
    def "equals, true"() {
        expect:
        newName("x", 0).equals(newName("x", 0))
    }

    def "equals, false, instance number is different"() {
        expect:
        !newName("x", 0).equals(newName("x", 1))
    }

    def "equals, false, instance identification is different"() {
        expect:
        !newName("y", 1).equals(newName("x", 1))
    }

    def "equals, false, instance type token is different"() {
        expect:
        !newName("x", 1, "t").equals(newName("x", 1))
    }

    def "equals, false, all components are different"() {
        expect:
        !newName("y", 1, "t").equals(newName("x", 2))
    }

    def "equals, false, not the same class"() {
        expect:
        !newName("y", 1).equals(new Object())
    }

    def "hashCode, ok, same as string representation"() {
        expect:
        newName("x").hashCode() == "[env: surrogate x 0]".hashCode()
    }

    def "toString, ok, has specific representation"() {
        expect:
        newName("x").toString().equals("[env: surrogate x 0]")
    }

    def "getDisplayText, ok, has specific display representation"() {
        expect:
        newName("x").getDisplayString().equals("surrogate-x#0")
    }

    def "getIdentification, ok, same as passed in constructor"() {
        expect:
        newName("x").getIdentification().equals("x")
    }

    def "newName; error; a unit name must not contain spaces"() {
        when:
        newName("some name")

        then:
        thrown(IllegalArgumentException)
    }

    def "newName; ok; name with sharp followed number"() {
        when:
        def name = newName("someName#12358")

        then:
        name.identification == 'someName'
        name.fabricationToken == 'surrogate'
        name.instanceNumber == 12358
    }

    def "newName; ok; name with a dash"() {
        when:
        def name = newName("first-name")

        then:
        name.identification == 'name'
        name.fabricationToken == 'first'
        name.instanceNumber == 0
    }

    def "newName; ok; name with two dashes"() {
        when:
        def name = newName("first-name-second")

        then:
        name.identification == 'second'
        name.fabricationToken == 'first-name'
        name.instanceNumber == 0
    }

    def "newName; ok; name with dashes and sharp"() {
        when:
        def name = newName("three-word-token-identity#123")

        then:
        name.identification == 'identity'
        name.fabricationToken == 'three-word-token'
        name.instanceNumber == 123
    }
}
