package org.leonardo.testing.unity.environment

import org.leonardo.environment.Resource
import org.leonardo.environment.ResourceExecutionException
import spock.lang.Specification

/**
 * Created by Lrteixeira on 23/03/2016.*/
@SuppressWarnings("GroovyAssignabilityCheck")
class ResourceExecutionExceptionSpec extends Specification {
    def "getMessage; ok"() {
        given:
        def someResource = Mock(Resource) {
            getDisplayString() >> "x"
        }

        and:
        def subject = new ResourceExecutionException(someResource, "details")

        expect:
        subject.message == "Unable to execute resource 'x': details"

    }
}
