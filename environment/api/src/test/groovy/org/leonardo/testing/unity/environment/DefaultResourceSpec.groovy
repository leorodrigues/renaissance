package org.leonardo.testing.unity.environment

import org.leonardo.environment.DefaultResource
import org.leonardo.environment.InvalidResourcePathException
import spock.lang.Specification

/**
 * Created by Lrteixeira on 22/03/2016.*/
@SuppressWarnings("GroovyAssignabilityCheck")
class DefaultResourceSpec extends Specification {
    def "new; ok; valid path"() {
        given:
        DefaultResource subject = new DefaultResource("/path/to/file")

        expect:
        subject.URL.toString() =~ "file:/{1,3}(\\w(:|\\|)/)?path/to/file"
    }

    def "new; error; empty path"() {
        when:
        new DefaultResource("").URL.toString()

        then:
        thrown(InvalidResourcePathException)
    }

    def "new; error; null path"() {
        when:
        new DefaultResource(null).URL.toString()

        then:
        thrown(InvalidResourcePathException)
    }
}
