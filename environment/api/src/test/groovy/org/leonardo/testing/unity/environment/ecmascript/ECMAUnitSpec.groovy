package org.leonardo.testing.unity.environment.ecmascript

import org.leonardo.environment.Resource
import org.leonardo.environment.ResourceExecutionException
import org.leonardo.environment.UnitName
import org.leonardo.environment.ecmascript.ECMAFacility
import org.leonardo.environment.ecmascript.ECMAUnit
import org.leonardo.environment.ecmascript.ResourceLoader
import spock.lang.Specification

class ECMAUnitSpec extends Specification {

    def "getResourceLoader; ok"() {
        given:
        def name = Mock(UnitName)
        expect:
        new GuineaPig(name, Mock(ResourceLoader)).hasResourceLoader()
    }

    static class GuineaPig extends ECMAUnit {
        GuineaPig(UnitName name, ResourceLoader loader) {
            super(name, loader)
        }

        boolean hasResourceLoader() {
            return getResourceLoader() != null
        }

        @Override
        void addFacility(ECMAFacility facility) { }

        @Override
        void allocateContext() { }

        @Override
        void freeContext() { }

        @Override
        void execute(Resource resource) throws ResourceExecutionException { }
    }
}
