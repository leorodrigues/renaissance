package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import spock.lang.Specification
import spock.lang.Subject
/**
 * Created by Lrteixeira on 03/02/2016.*/
@Subject(UnitLifeCycleControllerImpl)
@SuppressWarnings("GroovyAssignabilityCheck")
class UnitLifeCycleControllerImplSpec extends Specification {

    def "getRunningUnitNames, ok"() {
        given:
        def names = new UnitName[0]

        and:
        def instances = Mock(UnitInstances)

        when:
        def result = subject.getRunningUnitsNames()

        then:
        result.is(names)

        and:
        currentInstances.allRunning() >> instances
        instances.getNames() >> names
    }

    def "shutdownUnit, ok, it is found"() {
        given:
        def name = Mock(UnitName)

        and:
        ControlledUnit someUnit = Mock(ControlledUnit)

        when:
        subject.shutdownUnit(name)

        then:
        1 * currentInstances.findByNameOrFail(name) >> someUnit
        1 * bootRunner.tryShuttingDown(someUnit)
    }

    def "shutdownUnit, error, it is not found"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.shutdownUnit(name)

        then:
        thrown(IllegalStateException)
        0 * bootRunner.tryShuttingDownMany(_)
        1 * currentInstances.findByNameOrFail(name) >> {
            throw new IllegalStateException(
                    "thrown on purpose by an unity test")
        }
    }

    def "bootUnit, ok"() {
        given:
        def name = Mock(UnitName)

        and:
        ControlledUnit unit = Mock(ControlledUnit)

        when:
        Unit result = subject.bootUnit(name)

        then:
        result == unit
        1 * currentInstances.findByNameOrCreate(name) >> unit
        1 * bootRunner.tryBooting(unit)
    }

    def "getUnitByName, ok, call is delegated"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.getUnitByName(name)

        then:
        1 * currentInstances.findByNameOrFail(name)
    }

    def "isUnitAvailable, ok"() {
        given:
        def name = Mock(UnitName)

        when:
        boolean available = subject.isUnitAvailable(name)

        then:
        available
        1 * currentInstances.isUnitAvailable(name) >> true
    }

    private UnitLifeCycleController subject
    private Resource resource
    private UnitInstances currentInstances
    private UnitLifeCycleRunner bootRunner

    def setup() {
        resource = Mock(Resource)

        currentInstances = Mock(UnitInstances)
        bootRunner = Mock(UnitLifeCycleRunner)

        subject = new UnitLifeCycleControllerImpl(bootRunner, currentInstances)
    }
}
