package org.leonardo.testing.unity.environment.ecmascript

import org.leonardo.environment.Resource
import org.leonardo.environment.ecmascript.LocalResourceLoader
import org.leonardo.environment.ecmascript.ResourceLoadException
import spock.lang.Specification

class LocalResourceLoaderSpec extends Specification {
    def "openResource; ok"() {
        given:
        def resource = Mock(Resource)

        and:
        def subject = new LocalResourceLoader()

        when:
        def result = subject.openResource(resource)

        then:
        result != null
        1 * resource.getURL() >> sampleMessageUrl
    }

    def "openResource; error; RuntimeException is thrown"() {
        given:
        def resource = Mock(Resource)

        and:
        def subject = new LocalResourceLoader()

        when:
        subject.openResource(resource)

        then:
        thrown(ResourceLoadException)
        1 * resource.getURL() >> urlToNowhere
    }

    private URL getUrlToNowhere() {
        return new URL("${sampleMessageUrl}.notfound")
    }

    private URL getSampleMessageUrl() {
        def name = "message-sample.txt"
        getClass().getClassLoader().getResource(name).toURI().toURL()
    }
}
