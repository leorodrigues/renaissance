package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import org.leonardo.environment.config.Configuration
import org.leonardo.environment.config.ConfigurationGate
import org.leonardo.environment.config.ConfigurationGateKeeper
import org.leonardo.environment.config.ConfigurationIndex
import spock.lang.Specification

/**
 * Created by leonardo on 20/02/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class ControlPointImplSpec extends Specification {
    def "loadState; ok; delegate to state controller"() {
        when:
        subject.loadState()

        then:
        stateBankController.load()
    }

    def "saveState; ok; delegate to state controller"() {
        when:
        subject.saveState()

        then:
        stateBankController.save()
    }

    def "isNameDeclared; ok; delegate to gate keeper"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.isNameDeclared(name)

        then:
        configurationGateKeeper.isNameDeclared(name) >> { }
    }

    def "disposeOfName; ok; delegate to gate keeper"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.disposeOfName(name)

        then:
        configurationGateKeeper.disposeOfName(name) >> { }
    }

    def "getRegisteredTokens; ok; delegate to registry"() {
        when:
        def result = subject.getRegisteredTokens()

        then:
        result == ['t']
        unitFactoryRegistry.getRegisteredTokens() >> ['t']
    }

    def "hasUnitFactoryForToken; ok; calls the delegate"() {
        when:
        def result = subject.hasUnitFactoryForToken('f')

        then:
        result
        unitFactoryRegistry.hasUnitFactoryForToken('f') >> true
    }

    def "registerFactoryForToken; ok; calls the delegate"() {
        given:
        def factory = Mock(UnitFactory)

        when:
        subject.registerFactoryForToken(factory, 'f')

        then:
        unitFactoryRegistry.registerFactoryForToken(factory, 'f') >> { }
    }

    def "getRunningUnitNames, ok"() {
        given:
        def names = new UnitName[0]

        when:
        def result = subject.getRunningUnitsNames()

        then:
        result.is(names)
        lifeCycleController.getRunningUnitsNames() >> names
    }

    def "isUnitAvailable; ok"() {
        given:
        def name = Mock(UnitName)

        when:
        def result = subject.isUnitAvailable(name)

        then:
        result
        lifeCycleController.isUnitAvailable(name) >> true
    }

    def "shutdownUnit; ok"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.shutdownUnit(name)

        then:
        lifeCycleController.shutdownUnit(name)
        configurationGateKeeper.unlockConfigurationForName(name) >> { }
    }

    def "shutdownUnit; error; shutdown fails with exception"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.shutdownUnit(name)

        then:
        thrown(RuntimeException)
        lifeCycleController.shutdownUnit(name) >> { failOnPurpose() }
        configurationGateKeeper.unlockConfigurationForName(name) >> { }
    }

    def "bootUnit; ok"() {
        given:
        def unit = Mock(Unit)

        and:
        def name = Mock(UnitName)

        when:
        def result = subject.bootUnit(name)

        then:
        result == unit

        and:
        lifeCycleController.bootUnit(name) >> unit
        configurationGateKeeper.lockConfigurationForName(name) >> { }
        configurationGateKeeper.unlockConfigurationForName(_) >> { }
    }

    def "bootUnit; exception; it unlocks the configuration if boot exception occurs"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.bootUnit(name)

        then:
        thrown(RuntimeException)

        and:
        lifeCycleController.bootUnit(name) >> {
            throw new RuntimeException("thrown on purpose by an unit test")
        }
        configurationGateKeeper.lockConfigurationForName(name) >> { }
        configurationGateKeeper.unlockConfigurationForName(name) >> { }
    }

    def "getUnitByName; ok"() {
        given:
        def unit = Mock(Unit)

        and:
        def name = Mock(UnitName)

        when:
        def result = subject.getUnitByName(name)

        then:
        result == unit

        and:
        lifeCycleController.getUnitByName(name) >> unit
    }

    def "getNames; ok"() {
        given:
        def names = new UnitName[0]

        when:
        def result = subject.getNames()

        then:
        result.is(names)

        and:
        configurationIndex.getNames() >> names
    }

    def "declareName; ok"() {
        given:
        Configuration configuration = Mock(Configuration)

        and:
        def name = Mock(UnitName)

        when:
        subject.declareName(name)

        then:
        configurationGateKeeper.declareName(name) >> configuration
    }

    def "getConfigurationForName; ok"() {
        given:
        def name = Mock(UnitName)

        and:
        def configurationGate = Mock(ConfigurationGate)

        when:
        def result = subject.getConfigurationForName(name)

        then:
        configurationGate == result

        and:
        configurationGateKeeper.getConfigurationForName(name) >> configurationGate
    }

    private ConfigurationGateKeeper configurationGateKeeper
    private UnitLifeCycleController lifeCycleController
    private ConfigurationIndex configurationIndex
    private UnitFactoryRegistry unitFactoryRegistry
    private ControlPointImpl subject
    private StateBankController stateBankController

    void setup() {
        unitFactoryRegistry = Mock(UnitFactoryRegistry)

        lifeCycleController = Mock(UnitLifeCycleController)

        configurationIndex = Mock(ConfigurationIndex)

        stateBankController = Mock(StateBankController)

        configurationGateKeeper = Spy(ConfigurationGateKeeper,
                constructorArgs: [configurationIndex])

        subject = Spy(ControlPointImpl,
                constructorArgs: [
                        lifeCycleController,
                        configurationGateKeeper,
                        unitFactoryRegistry,
                        stateBankController
                ])
    }

    private static void failOnPurpose() {
        throw new RuntimeException("thrown on purpose by an unity test")
    }
}
