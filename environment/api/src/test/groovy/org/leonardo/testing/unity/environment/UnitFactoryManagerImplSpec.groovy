package org.leonardo.testing.unity.environment

import org.leonardo.environment.ControlledUnit
import org.leonardo.environment.UnitFactory
import org.leonardo.environment.UnitFactoryManagerImpl
import org.leonardo.environment.UnitName
import spock.lang.Specification

/**
 * Created by leonardo on 02/07/2016.
 */
class UnitFactoryManagerImplSpec extends Specification {

    def "hasUnitFactoryForToken; false; factory is not registeredd"() {
        expect:
        !subject.hasUnitFactoryForToken('w')
    }

    def "hasUnitFactoryForToken; true; factory is registeredd"() {
        given:
        subject.registerFactoryForToken(new FakeFactory(), 'k')

        expect:
        subject.hasUnitFactoryForToken('k')
    }

    def "registerFactoryForToken; ok; factory is registered and returned"() {
        when:
        subject.registerFactoryForToken(new FakeFactory(), 'x')
        
        then:
        subject.getFactoryInstanceWithToken('x').class.is(FakeFactory.class)
    }

    def "registerFactoryForToken; ok; factory is overwritten"() {
        given:
        def factory1 = new FakeFactory()

        and:
        def factory2 = new FakeFactory()

        and:
        subject.registerFactoryForToken(factory1, 'x')

        when:
        def result1 = subject.getFactoryInstanceWithToken('x')
        subject.registerFactoryForToken(factory2, 'x')
        def result2 = subject.getFactoryInstanceWithToken('x')

        then:
        result1.is(factory1)
        result2.is(factory2)
        !result1.is(result2)
    }

    def "getFactoryInstanceWithToken; ok; returns default factory"() {
        expect:
        subject.getFactoryInstanceWithToken("y").is(defaultFactory)
    }

    def "getFactoryInstanceWithToken; ok; returns some instance"() {
        given:
        subject.registerFactoryForToken(new FakeFactory(), "x")

        expect:
        subject.getFactoryInstanceWithToken("x").class.is(FakeFactory.class)
    }

    def "getFactoryInstanceWithToken; ok; returns same instance for a token"() {
        given:
        subject.registerFactoryForToken(new FakeFactory(), "x")

        when:
        def instance1 = subject.getFactoryInstanceWithToken("x")
        def instance2 = subject.getFactoryInstanceWithToken("x")

        then:
        instance2.is(instance1)
    }

    def "getRegisteredTokens; ok"() {
        given:
        subject.registerFactoryForToken(Mock(UnitFactory), 'x')
        subject.registerFactoryForToken(Mock(UnitFactory), 'y')
        subject.registerFactoryForToken(Mock(UnitFactory), 'z')

        when:
        def result = subject.registeredTokens

        then:
        result == ['x', 'y', 'z']
    }

    private UnitFactory defaultFactory
    private UnitFactoryManagerImpl subject

    void setup() {
        defaultFactory = Mock(UnitFactory)
        subject = new UnitFactoryManagerImpl(defaultFactory)
    }

    public static class FakeFactory implements UnitFactory {
        @Override
        ControlledUnit create(UnitName name) {
            return null
        }
    }
}
