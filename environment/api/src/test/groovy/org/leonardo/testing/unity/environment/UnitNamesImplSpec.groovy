package org.leonardo.testing.unity.environment

import org.leonardo.environment.UnitName
import org.leonardo.environment.UnitNames
import org.leonardo.environment.UnitNamesImpl
import spock.lang.Specification

/**
 * Created by leonardo on 07/02/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class UnitNamesImplSpec extends Specification {
    def "join, ok, instance is empty"() {
        expect:
        new UnitNamesImpl().join(",") == ""
    }

    def "join, ok, only one name"() {
        given:
        UnitNamesImpl subject = new UnitNamesImpl()

        and:
        def name = Mock(UnitName)

        and:
        subject.add(name)

        when:
        def result = subject.join(",")

        then:
        result == "somename"
        1 * name.getDisplayString() >> "somename"
    }

    def "join, ok, two names"() {
        given:
        UnitNamesImpl subject = new UnitNamesImpl()

        and:
        def name = Mock(UnitName)

        and:
        subject.add(name)
        subject.add(name)

        when:
        def result = subject.join(",")

        then:
        result == "alpha,beta"
        1 * name.getDisplayString() >> "alpha"
        1 * name.getDisplayString() >> "beta"
    }

    def "join, ok, three names"() {
        given:
        UnitNamesImpl subject = new UnitNamesImpl()

        and:
        def name = Mock(UnitName)

        and:
        subject.add(name)
        subject.add(name)
        subject.add(name)

        when:
        def result = subject.join(",")

        then:
        result == "alpha,beta,gamma"
        1 * name.getDisplayString() >> "alpha"
        1 * name.getDisplayString() >> "beta"
        1 * name.getDisplayString() >> "gamma"
    }

    def "makeCopy, ok"() {
        given:
        UnitNames subject = new UnitNamesImpl()

        and:
        subject.add(Mock(UnitName) { getDisplayString() >> "a" })
        subject.add(Mock(UnitName) { getDisplayString() >> "b" })

        when:
        UnitNames copyOfSubject = subject.makeCopy()

        then:
        subject.join("|").equals(copyOfSubject.join("|"))
    }

    def "clear, ok"() {
        given:
        UnitNames subject = new UnitNamesImpl()

        and:
        subject.add(Mock(UnitName) { getDisplayString() >> "a" })

        when:
        subject.clear()

        then:
        subject.join("|").equals("")
    }
}
