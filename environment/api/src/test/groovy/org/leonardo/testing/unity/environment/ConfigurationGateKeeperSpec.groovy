package org.leonardo.testing.unity.environment

import org.leonardo.environment.UnitName
import org.leonardo.environment.config.*
import spock.lang.Specification

/**
 * Created by leonardo on 21/02/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class ConfigurationGateKeeperSpec extends Specification {

    def "disposeOfName; ok; name is there and is not locked"() {
        given:
        def unitName = Mock(UnitName)

        and:
        def config = Mock(Configuration)

        and:
        subject.declareName(unitName)

        when:
        def result = subject.disposeOfName(unitName)

        then:
        result.is(config)
        1 * configurationIndex.disposeOfName(unitName) >> config
    }

    def "disposeOfName; ok; name is there and but is locked"() {
        given:
        def unitName = Mock(UnitName)

        and:
        subject.declareName(unitName)
        subject.lockConfigurationForName(unitName)

        when:
        subject.disposeOfName(unitName)

        then:
        thrown(ConfigLockedException)
        0 * configurationIndex.disposeOfName(_) >> {}
    }

    def "disposeOfName; error; name is not there"() {
        given:
        def unitName = Mock(UnitName)

        when:
        subject.disposeOfName(unitName)

        then:
        thrown(ConfigKeepingException)
        0 * configurationIndex.disposeOfName(_) >> { }
    }

    def "declareName; error; name is declared at least twice"() {
        given:
        def name = Mock(UnitName)

        and:
        subject.declareName(name)

        when:
        subject.declareName(name)

        then:
        thrown(ConfigKeepingException)
    }

    def "getConfigurationForName; ok; config was declared first"() {
        given:
        def name = Mock(UnitName)

        and:
        def gate = subject.declareName(name)

        when:
        def config = subject.getConfigurationForName(name)

        then:
        config.is(gate)
        config instanceof ConfigurationGate
    }

    def "getConfigurationForName; ok; config does not exist"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.getConfigurationForName(name)

        then:
        thrown(ConfigKeepingException)
    }

    def "isNameDeclared; ok; delegate to persistentConfigurationIndex"() {
        given:
        def name = Mock(UnitName)

        when:
        def result = subject.isNameDeclared(name)

        then:
        result
        configurationIndex.isNameDeclared(name) >> true
    }

    def "unlockConfigurationForName; ok"() {
        given:
        def configuration = Mock(Configuration)

        and:
        def name = Mock(UnitName)

        and:
        def gate = subject.declareName(name)

        and:
        subject.lockConfigurationForName(name)

        and:
        def currentlyLocked = gate.isLocked()

        when:
        subject.unlockConfigurationForName(name)

        then:
        currentlyLocked
        !gate.isLocked()
        configurationIndex.getConfigurationForName(name) >> configuration
        configurationIndex.declareName(name) >> configuration
    }

    def "unlockConfigurationForName; error; configuration is not being kept"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.unlockConfigurationForName(name)

        then:
        thrown(ConfigKeepingException)
    }

    def "lockConfigurationForName; ok"() {
        given:
        def configuration = Mock(Configuration)

        and:
        def name = Mock(UnitName)

        and:
        def gate = subject.declareName(name)

        when:
        subject.lockConfigurationForName(name)

        then:
        gate.isLocked()
        configurationIndex.getConfigurationForName(name) >> configuration
        configurationIndex.declareName(name) >> configuration
    }

    def "lockConfigurationForName; error; configuration is not being kept"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.lockConfigurationForName(name)

        then:
        thrown(ConfigKeepingException)
    }

    private ConfigurationGateKeeper subject
    private ConfigurationIndex configurationIndex

    void setup() {
        configurationIndex = Mock(ConfigurationIndex)
        subject = Spy(ConfigurationGateKeeper,
            constructorArgs: [configurationIndex])
    }
}
