package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import org.leonardo.environment.config.ConfigEntry
import org.leonardo.environment.config.ConfigurationDescriptionBuilder
import org.leonardo.environment.config.DefaultConfiguration
import spock.lang.Specification

class DefaultConfigurationSpec extends Specification {
    def "getEntries; ok; result is readonly"() {
        given:
        def subject = new DefaultConfiguration(Mock(UnitName) {
            getDisplayString() >> 'my-name'
        })

        when:
        subject.getEntries().add(null)

        then:
        thrown(UnsupportedOperationException)
    }

    def "copyFrom; ok; replaces contents completely"() {
        given:
        def subject = new DefaultConfiguration(Mock(UnitName) {
            getDisplayString() >> 'my-name'
        })

        and:
        def another = new DefaultConfiguration(Mock(UnitName) {
            getDisplayString() >> 'another-name'
        })

        when:
        subject.copyFrom(another)

        then:
        subject.get('unit_name') == 'another-name'
    }

    def "get; ok; var exists"() {
        given:
        def subject = new DefaultConfiguration(Mock(UnitName) {
            getDisplayString() >> 'N'
        })

        and:
        subject.set('k', 'w')

        when:
        def val = subject.get('k')

        then:
        val == 'w'
    }

    def "get; ok; var is not declared"() {
        given:
        def name = Mock(UnitName) { getDisplayString() >> 'N' }

        expect:
        new DefaultConfiguration(name).get('p') == null
    }

    def "makeCopy; ok; variables are copied but name is replaced"() {
        given:
        def originalName = Mock(UnitName) {
            getDisplayString() >> "original-name"
        }

        and:
        def newName = Mock(UnitName) {
            getDisplayString() >> "new-name"
        }

        and:
        def subject = new DefaultConfiguration(originalName)

        and:
        subject.set("x", "y")

        when:
        def copyOfSubject = subject.makeCopy(newName)

        then:
        copyOfSubject.get("x") == "y"
        copyOfSubject.get("unit_name") == "new-name"
    }

    def "describe; ok; only direct keys"() {
        given:
        def name = Mock(UnitName) {
            getDisplayString() >> "the-name"
        }

        and:
        def subject = new DefaultConfiguration(name)

        and:
        def descriptor = Mock(ConfigurationDescriptionBuilder)

        when:
        subject.describe(descriptor)

        then:
        1 * descriptor.append("unit_name", "the-name") >> { }
    }

    def "describe; ok; also with expanded keys"() {
        given:
        def name = Mock(UnitName) {
            getDisplayString() >> "the-name"
        }

        and:
        def subject = new DefaultConfiguration(name)

        and:
        def descriptor = Mock(ConfigurationDescriptionBuilder)

        and:
        def expander = Mock(ValueExpander)

        when:
        subject.setValueExpander(expander)
        subject.set("var", "expression")
        subject.describe(descriptor)

        then:
        1 * expander.expand(
            "expression", { e -> e instanceof Set<ConfigEntry>} ) >> "expanded"
        1 * descriptor.append(
            "unit_name", "the-name") >> { }
        1 * descriptor.append(
            "var", "expression", "expanded") >> { }
    }
}
