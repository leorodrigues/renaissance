package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import spock.lang.Specification

import static org.leonardo.environment.UnitState.INSTANTIATED

/**
 * Created by Lrteixeira on 21/03/2016.*/
@SuppressWarnings("GroovyAssignabilityCheck")
class SurrogateUnitSpec extends Specification {

    def "getState; ok; INSTANTIATED at first"() {
        expect:
        subject.getState() == INSTANTIATED
    }

    def "execute; ok"() {
        when:
        subject.execute(Mock(Resource))

        then:
        thrown(ResourceExecutionException)
    }

    private SurrogateUnit subject

    void setup() {
        subject = new SurrogateUnit()
    }
}
