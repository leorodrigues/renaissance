package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import org.leonardo.environment.config.Configuration
import org.leonardo.environment.config.ConfigurationIndexImpl
import org.leonardo.environment.config.NameAlreadyDeclaredException
import org.leonardo.environment.config.NameNotFoundException
import spock.lang.Specification

/**
 * Created by Lrteixeira on 11/02/2016.*/
@SuppressWarnings("GroovyAssignabilityCheck")
class ConfigurationIndexImplSpec extends Specification {
    def "disposeOfName; ok"() {
        given:
        def name = Mock(UnitName)

        and:
        subject.declareName(name)

        and:
        def found = subject.isNameDeclared(name)

        when:
        subject.disposeOfName(name)

        then:
        found
        !subject.isNameDeclared(name)
    }

    def "isNameDeclared; true; name exists"() {
        given:
        def name = Mock(UnitName)

        and:
        subject.declareName(name)

        expect:
        subject.isNameDeclared(name)
    }

    def "isNameDeclared; false; name does not exist"() {
        given:
        def name = Mock(UnitName)

        expect:
        !subject.isNameDeclared(name)
    }

    def "getConfigurationForName; ok; name exists"() {
        given:
        def name = Mock(UnitName)

        and:
        def theConfiguration = subject.declareName(name)

        when:
        def result = subject.getConfigurationForName(name)

        then:
        result == theConfiguration
    }

    def "getConfigurationForName; error; name not declared"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.getConfigurationForName(name)

        then:
        thrown(NameNotFoundException)
    }

    def "declareName; ok; name is new"() {
        given:
        def name = Mock(UnitName)

        and:
        Configuration newConfiguration = Spy(Configuration)

        when:
        Configuration result = subject.declareName(name)

        then:
        result == newConfiguration
        1 * defaultConfiguration.makeCopy(name) >> newConfiguration
    }

    def "declareName; error; name already exists"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.declareName(name)
        subject.declareName(name)

        then:
        thrown(NameAlreadyDeclaredException)
    }

    def "getNames; ok; index is currently empty"() {
        expect:
        subject.getNames() == new UnitName[0]
    }

    def "getNames; ok; there is a name in the index"() {
        given:
        def name = Mock(UnitName)

        and:
        subject.declareName(name)

        expect:
        subject.getNames() == [name]

    }

    void setup() {
        defaultConfiguration = Mock(Configuration)
        subject = Spy(ConfigurationIndexImpl, constructorArgs: [
                defaultConfiguration
        ])
    }

    private ConfigurationIndexImpl subject
    private Configuration defaultConfiguration
}
