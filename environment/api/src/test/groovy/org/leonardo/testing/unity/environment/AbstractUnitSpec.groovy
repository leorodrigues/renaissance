package org.leonardo.testing.unity.environment

import org.leonardo.environment.*
import spock.lang.Specification

import static org.leonardo.environment.UnitState.INSTANTIATED

class AbstractUnitSpec extends Specification {

    def "getName; ok"() {
        given:
        def name = Mock(UnitName)

        expect:
        new GuineaPig(name).getName().is(name)
    }

    def "getState; ok"() {
        expect:
        new GuineaPig(Mock(UnitName)).getState() == INSTANTIATED
    }

    static class GuineaPig extends AbstractUnit {
        GuineaPig(UnitName unitName) {
            setName(unitName)
            setState(INSTANTIATED)
        }

        @Override
        void execute(Resource resource) throws ResourceExecutionException { }

        @Override
        boolean runBootSequence() {
            return false
        }

        @Override
        boolean runShutdownSequence() {
            return false
        }
    }
}
