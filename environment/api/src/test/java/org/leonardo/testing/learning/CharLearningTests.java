package org.leonardo.testing.learning;

import org.junit.Test;
import org.leonardo.testing.util.Memory;

import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

/**
 * Created by lrteixeira on 24/09/2015.
 */
public class CharLearningTests {
    @Test
    public void testIntToChars() throws Exception {
        int i = 65;
        char[] expected = new char[] {'A'};
        char[] actual = Character.toChars(i);
        assertDeepEquals(expected, actual);
    }

    @Test
    public void testMemory() throws Exception {
        Memory m = new Memory(60);

        PrintStream printer = new PrintStream(m.getNewOutput());
        printer.println("hello");
        printer.println("world");

        assertEquals("hello\r\nworld\r\n", m.snapshot());

        Scanner scanner = new Scanner(m.getNewIndependentInput());
        assertEquals("hello", scanner.nextLine());
        assertEquals("world", scanner.nextLine());
    }

    private void assertDeepEquals(char[] expected, char[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++)
            assertEquals(expected[i], actual[i]);
    }
}
