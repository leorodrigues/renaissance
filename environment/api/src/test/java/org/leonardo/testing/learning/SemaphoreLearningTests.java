package org.leonardo.testing.learning;

import org.junit.Test;

import java.util.Calendar;
import java.util.concurrent.Semaphore;

import static org.junit.Assert.assertTrue;

/**
 * Created by leonardo on 07/11/2015.
 */
public class SemaphoreLearningTests {
    @Test
    public void testSignalingToContinue() throws InterruptedException {
        final Semaphore semaphore = new Semaphore(0);

        Thread worker = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(2000);
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        long start = Calendar.getInstance().getTimeInMillis();
        worker.start();
        semaphore.acquire();
        long end = Calendar.getInstance().getTimeInMillis();

        assertTrue(end - start >= 1000);
    }
}
