package org.leonardo.testing.learning;

import org.junit.Test;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Created by leonardo on 07/11/2015.
 */
public class CyclicBarrierLearningTests {

    @Test
    public void testFromInternetExample() throws Exception {
        // URL for this example:
        // http://tutorials.jenkov.com/java-util-concurrent/cyclicbarrier.html

        Runnable barrier1Action = new Runnable() {
            public void run() { System.out.println("Action 1 done!"); }
        };

        Runnable barrier2Action = new Runnable() {
            public void run() { System.out.println("Action 2 done!"); }
        };

        CyclicBarrier barrier1 = new CyclicBarrier(2, barrier1Action);
        CyclicBarrier barrier2 = new CyclicBarrier(2, barrier2Action);

        CyclicBarrierRunnable barrierRunnable1 =
                new CyclicBarrierRunnable(barrier1, barrier2);

        CyclicBarrierRunnable barrierRunnable2 =
                new CyclicBarrierRunnable(barrier1, barrier2);

        Thread t1 = new Thread(barrierRunnable1);
        Thread t2 = new Thread(barrierRunnable2);

        t1.start();
        t2.start();

        Thread.sleep(5000);

    }

    public static class CyclicBarrierRunnable implements Runnable {
        private CyclicBarrier barrier1 = null;
        private CyclicBarrier barrier2 = null;

        public CyclicBarrierRunnable(
                CyclicBarrier barrier1,
                CyclicBarrier barrier2) {

            this.barrier1 = barrier1;
            this.barrier2 = barrier2;
        }

        public void run() {
            try {
                Thread.sleep(1000 + new Random().nextInt(100));
                System.out.println(Thread.currentThread().getName() +
                        " waiting at barrier 1");
                this.barrier1.await();

                Thread.sleep(1000 + new Random().nextInt(100));
                System.out.println(Thread.currentThread().getName() +
                        " waiting at barrier 2");
                this.barrier2.await();

                System.out.println(Thread.currentThread().getName() +
                        " done!");

            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}
