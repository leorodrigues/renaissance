package org.leonardo.testing.learning;

import org.junit.Test;
import org.leonardo.testing.util.Flag;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import static org.junit.Assert.assertTrue;

/**
 * Created by leonardo on 29/11/2015.
 */
public class ProxyTests {

    private final Flag invoked = new Flag();

    @Test
    public void testSimpleDynamicProxy() throws Exception {
        SubjectInterface subjectImpl;
        subjectImpl = (SubjectInterface)Proxy.newProxyInstance(
                SubjectInterface.class.getClassLoader(),
                new Class[] { SubjectInterface.class },
                new SubjectInvocationHandler());
        subjectImpl.someOperation();
        assertTrue(invoked.isSet());
    }

    public class SubjectInvocationHandler implements InvocationHandler {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args)
                throws Throwable {
            invoked.signal();
            return null;
        }
    }

    public interface SubjectInterface {
        void someOperation();
    }
}
