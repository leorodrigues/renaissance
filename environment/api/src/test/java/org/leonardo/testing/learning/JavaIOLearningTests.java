/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.leonardo.testing.learning;

import java.io.*;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 *
 * @author Leonardo Rodrigues Teixeira (leorodriguesrj@gmail.com)
 */
public class JavaIOLearningTests {

    @Test
    public void testPipedEcho() throws IOException {
        PipedReader reader = new PipedReader();
        PipedWriter writer = new PipedWriter();
        PrintWriter printer = new PrintWriter(writer);
        Scanner scanner = new Scanner(reader);
        writer.connect(reader);
        printer.println("11235813");
        String output = scanner.nextLine();
        assertEquals("11235813", output);
    }

    @Test
    public void testStreamedEcho() throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printer = new PrintStream(outputStream);
        printer.println("hello");
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        Scanner scanner = new Scanner(inputStream);
        assertTrue(scanner.hasNextLine());
        assertEquals("hello", scanner.nextLine());
    }
}
