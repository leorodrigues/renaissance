
function loadMessage(path) {
    var scanner = new java.util.Scanner(new java.io.File(path));
    var message = "";
    while (scanner.hasNextLine()) {
        message = message + scanner.nextLine();
    }
    scanner.close();
    return message;
}