package org.leonardo.environment;

/**
 * Created by leonardo on 17/03/2016.
 */
public class ResourceExecutionException extends Exception {
    private final Resource resource;

    public Resource getResource() {
        return resource;
    }

    public ResourceExecutionException(Resource resource, String details) {
        super(details);
        this.resource = resource;
    }

    @Override
    public String getMessage() {
        String url = resource.getDisplayString();
        return String.format(
                "Unable to execute resource '%s': %s", url, super.getMessage());
    }
}
