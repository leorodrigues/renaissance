package org.leonardo.environment.config;

public interface ConfigIndexSync {
    void invoke(
        ConfigurationIndex configurationIndex,
        Configurations configurations);
}
