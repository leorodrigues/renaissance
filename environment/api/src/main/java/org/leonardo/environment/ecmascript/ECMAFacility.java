package org.leonardo.environment.ecmascript;

public interface ECMAFacility {
    ExposedName getExposedName();
    Object getPublicInstance();
}
