package org.leonardo.environment.ecmascript;

import org.leonardo.environment.InvalidResourcePathException;
import org.leonardo.environment.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by leonardo on 11/03/2016.
 */
public class LocalResourceLoader implements ResourceLoader {

    @Override
    public InputStream openResource(Resource resource) {
        try {
            URL url = resource.getURL();
            return url.openStream();
        } catch (IOException | InvalidResourcePathException e) {
            throw new ResourceLoadException("Unable to open resource.", e);
        }
    }
}
