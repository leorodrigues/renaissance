package org.leonardo.environment.config;

public class ConfigKeepingException extends RuntimeException {
    public ConfigKeepingException(String message) {
        super(message);
    }
}
