package org.leonardo.environment.config;

import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.Command;
import org.leonardo.renaissance.command.SerialCommandRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.complement;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.intersection;

public class ConfigBankSyncImpl implements ConfigBankSync {

    private final SerialCommandRunner serialCommandRunner;

    public ConfigBankSyncImpl(SerialCommandRunner serialCommandRunner) {
        this.serialCommandRunner = serialCommandRunner;
    }

    @Override
    public void invoke(
            ControlPointStateBank controlPointStateBank,
            ConfigurationIndex configurationIndex) {

        List<UnitName> currentNames = asList(configurationIndex.getNames());
        Set<UnitName> savedNames = controlPointStateBank.allUnitNames();
        ArrayList<Command> commands = new ArrayList<>();

        collectRemoveCommands(
            commands, controlPointStateBank, savedNames, currentNames);

        collectUpdateCommands(
            commands, controlPointStateBank, savedNames,
                currentNames, configurationIndex);

        collectInsertCommands(
            commands, controlPointStateBank, savedNames,
                currentNames, configurationIndex);

        if (commands.size() > 0)
            commands.add(new CommitChanges(controlPointStateBank));

        serialCommandRunner.run(commands);
    }

    private void collectInsertCommands(
            ArrayList<Command> commands,
            ControlPointStateBank controlPointStateBank,
            Collection<UnitName> savedNames,
            Collection<UnitName> currentNames,
            ConfigurationIndex configurationIndex) {

        for (UnitName n : complement(currentNames, savedNames))
            commands.add(makeInsertCommand(
                    controlPointStateBank, n, configurationIndex));
    }

    private void collectUpdateCommands(
            ArrayList<Command> commands,
            ControlPointStateBank controlPointStateBank,
            Collection<UnitName> savedNames,
            Collection<UnitName> currentNames,
            ConfigurationIndex configurationIndex) {

        for (UnitName n : intersection(currentNames, savedNames))
            commands.add(makeUpdateCommand(
                    controlPointStateBank, n, configurationIndex));
    }

    private void collectRemoveCommands(
            ArrayList<Command> commands,
            ControlPointStateBank controlPointStateBank,
            Collection<UnitName> savedNames,
            Collection<UnitName> currentNames) {

        for (UnitName n : complement(savedNames, currentNames))
            commands.add(new DeleteConfig(controlPointStateBank, n));
    }

    private Command makeUpdateCommand(
            ControlPointStateBank controlPointStateBank,
            UnitName unitName,
            ConfigurationIndex configurationIndex) {

        return new UpdateConfig(
            controlPointStateBank, unitName, configurationIndex);
    }

    private Command makeInsertCommand(
            ControlPointStateBank controlPointStateBank,
            UnitName unitName,
            ConfigurationIndex configurationIndex) {
        return new InsertConfig(
            controlPointStateBank, unitName, configurationIndex);
    }
}
