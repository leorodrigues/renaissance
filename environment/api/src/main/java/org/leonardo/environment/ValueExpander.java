package org.leonardo.environment;

import org.leonardo.environment.config.ConfigEntry;

import java.util.Set;

public interface ValueExpander {
    String expand(String value, Set<ConfigEntry> entries);
}
