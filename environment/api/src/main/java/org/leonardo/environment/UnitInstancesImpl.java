package org.leonardo.environment;

import org.lenardo.renaissance.collectionworks.Mapping;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.lang.String.format;
import static org.lenardo.renaissance.collectionworks.MapperReducer.map;
import static org.leonardo.environment.UnitState.DOWN;

/**
 * Created by Lrteixeira on 02/02/2016.
 */
public class UnitInstancesImpl implements UnitInstances {

    private static final String UNIT_NOT_AVAILABLE_TEMPLATE =
            "Unit '%s' is not available.";

    private final UnitFactoryManager unitFactoryManager;

    private final LinkedList<ControlledUnit> controlledUnits =
            new LinkedList<>();

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public UnitInstancesImpl(UnitFactoryManager unitFactoryManager) {
        this.unitFactoryManager = unitFactoryManager;
    }

    @Override
    public boolean add(ControlledUnit controlledUnit) {
        try {
            lock.writeLock().lock();
            return controlledUnits.add(controlledUnit);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public boolean remove(ControlledUnit controlledUnit) {
        try {
            lock.writeLock().lock();
            return controlledUnits.remove(controlledUnit);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public boolean isUnitAvailable(UnitName name) {
        try {
            lock.readLock().lock();
            return findByName(name) != null;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public UnitInstances makeCopy() {
        try {
            lock.readLock().lock();
            UnitInstancesImpl result =
                    new UnitInstancesImpl(unitFactoryManager);
            result.controlledUnits.addAll(controlledUnits);
            return result;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public UnitInstances allRunning() {
        try {
            lock.readLock().lock();
            UnitInstancesImpl result =
                    new UnitInstancesImpl(unitFactoryManager);
            for (ControlledUnit e : controlledUnits)
                if (e.getState() == UnitState.RUNNING)
                    result.controlledUnits.add(e);
            return result;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public ControlledUnit findByNameOrFail(UnitName name) {
        try {
            lock.readLock().lock();
            ControlledUnit result = findByName(name);
            if (result != null)
                return result;
            throw new IllegalStateException(makeMessageUnitNotAvailable(name));
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public ControlledUnit findByNameOrCreate(UnitName name) {
        lock.readLock().lock();
        ControlledUnit result = findByName(name);
        if (result == null) {
            lock.readLock().unlock();
            lock.writeLock().lock();
            try {
                result = findByName(name);
                if (result == null) {
                    UnitFactory factory =
                            acquireFactory(name.getFabricationToken());
                    result = factory.create(name);
                    controlledUnits.add(result);
                }
                lock.readLock().lock();
            } finally {
                lock.writeLock().unlock();
            }
        }
        try {
            return result;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public ControlledUnit[] toArray() {
        try {
            lock.readLock().lock();
            return controlledUnits.toArray(new ControlledUnit[0]);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public UnitName[] getNames() {
        try {
            lock.readLock().lock();
            List<UnitName> names = map(controlledUnits, unitToNameMapping);
            return names.toArray(new UnitName[0]);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public boolean areAllDown() {
        try {
            lock.readLock().lock();
            for (ControlledUnit controlledUnit : controlledUnits)
                if (controlledUnit.getState() != DOWN)
                    return false;
            return true;
        } finally {
            lock.readLock().unlock();
        }
    }

    private UnitFactory acquireFactory(String token) {
        return unitFactoryManager.getFactoryInstanceWithToken(token);
    }

    private ControlledUnit findByName(UnitName name) {
        for (ControlledUnit controlledUnit : controlledUnits)
            if (controlledUnit.getName().equals(name))
                return controlledUnit;
        return null;
    }

    private String makeMessageUnitNotAvailable(UnitName name) {
        return format(
                UNIT_NOT_AVAILABLE_TEMPLATE,
                name.getDisplayString());
    }

    private static final Mapping<UnitName, ControlledUnit> unitToNameMapping =
            new Mapping<UnitName, ControlledUnit>() {
                @Override
                public UnitName apply(ControlledUnit nextElement) {
                    return nextElement.getName();
                }
            };
}
