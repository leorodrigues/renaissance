package org.leonardo.environment.ecmascript;

import org.leonardo.environment.*;

/**
 * Created by Lrteixeira on 05/10/2015.
 */
public abstract class ECMAUnit extends AbstractUnit {

    private ResourceLoader resourceLoader;

    public ECMAUnit(UnitName unitName, ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
        setName(unitName);
    }

    @Override
    public boolean runBootSequence() {
        return true;
    }

    @Override
    public boolean runShutdownSequence() {
        return true;
    }

    public abstract void addFacility(ECMAFacility facility);

    public abstract void allocateContext();

    public abstract void freeContext();

    protected final ResourceLoader getResourceLoader() {
        return resourceLoader;
    }
}
