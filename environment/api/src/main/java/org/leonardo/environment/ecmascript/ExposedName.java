package org.leonardo.environment.ecmascript;

public class ExposedName {

    private String value;

    private ExposedName(String value) {
        this.value = value;
    }

    public String asString() {
        return value;
    }

    public static ExposedName newName(String value) {
        if (!value.matches("[\\w-]+"))
            throw new IllegalArgumentException("Invalid name");
        return new ExposedName(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExposedName that = (ExposedName)o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
