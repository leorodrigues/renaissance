package org.leonardo.environment;

/**
 * Created by Lrteixeira on 28/09/2015.
 */
public class UnitLifeCycleControllerImpl implements UnitLifeCycleController {

    private final UnitInstances unitInstances;

    private final UnitLifeCycleRunner unitLifeCycleRunner;

    public UnitLifeCycleControllerImpl(
            UnitLifeCycleRunner unitLifeCycleRunner,
            UnitInstances unitInstances) {
        this.unitLifeCycleRunner = unitLifeCycleRunner;
        this.unitInstances = unitInstances;
    }

    @Override
    public Unit getUnitByName(UnitName name) {
        return unitInstances.findByNameOrFail(name);
    }

    @Override
    public Unit bootUnit(UnitName name) {
        ControlledUnit result = unitInstances.findByNameOrCreate(name);
        unitLifeCycleRunner.tryBooting(result);
        return result;
    }

    @Override
    public void shutdownUnit(UnitName name) {
        ControlledUnit unit = unitInstances.findByNameOrFail(name);
        unitLifeCycleRunner.tryShuttingDown(unit);
    }

    @Override
    public boolean isUnitAvailable(UnitName name) {
        return unitInstances.isUnitAvailable(name);
    }

    @Override
    public UnitName[] getRunningUnitsNames() {
        UnitInstances runningInstances = unitInstances.allRunning();
        return runningInstances.getNames();
    }
}
