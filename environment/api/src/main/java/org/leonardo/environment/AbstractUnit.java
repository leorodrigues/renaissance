package org.leonardo.environment;

import static org.leonardo.environment.UnitState.INSTANTIATED;

/**
 * Created by leonardo on 30/09/2015.
 */
public abstract class AbstractUnit implements ControlledUnit {

    private UnitName name;
    private UnitState state = INSTANTIATED;

    @Override
    public UnitState getState() {
        return state;
    }

    @Override
    public UnitName getName() {
        return name;
    }

    @Override
    public void setName(UnitName name) {
        this.name = name;
    }

    public void setState(UnitState state) {
        this.state = state;
    }
}
