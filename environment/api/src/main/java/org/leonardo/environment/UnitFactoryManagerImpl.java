package org.leonardo.environment;

import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by leonardo on 24/06/2016.
 */

public class UnitFactoryManagerImpl implements UnitFactoryManager {

    private UnitFactory defaultUnitFactory;
    private HashMap<String, UnitFactory> instancesMap;
    private ReentrantLock fabricationLock = new ReentrantLock();

    public UnitFactoryManagerImpl(UnitFactory defaultUnitFactory) {
        this.defaultUnitFactory = defaultUnitFactory;
        instancesMap = new HashMap<>();
    }

    @Override
    public UnitFactory getFactoryInstanceWithToken(String fabricationToken) {
        try {
            fabricationLock.lock();
            if (instancesMap.containsKey(fabricationToken))
                return instancesMap.get(fabricationToken);
            return defaultUnitFactory;
        } finally {
            fabricationLock.unlock();
        }
    }

    @Override
    public void registerFactoryForToken(
            UnitFactory unitFactory,
            String fabricationToken) {
        try {
            fabricationLock.lock();
            if (instancesMap.containsKey(fabricationToken))
                instancesMap.remove(fabricationToken);
            instancesMap.put(fabricationToken, unitFactory);
        } finally {
            fabricationLock.unlock();
        }
    }

    @Override
    public boolean hasUnitFactoryForToken(String fabricationToken) {
        try {
            fabricationLock.lock();
            return instancesMap.containsKey(fabricationToken);
        } finally {
            fabricationLock.unlock();
        }
    }

    @Override
    public String[] getRegisteredTokens() {
        try {
            fabricationLock.lock();
            Set<String> keys = instancesMap.keySet();
            String result[] = new String[0];
            return keys.toArray(result);
        } finally {
            fabricationLock.unlock();
        }
    }
}
