package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;

import static java.lang.String.format;

public class ConfigSyncException extends RuntimeException {

    private static final String ERROR_MESSAGE_TEMPLATE =
        "Unable sync configuration for unit '%s': %s";

    public ConfigSyncException(UnitName unitName, Throwable e) {
        super(makeErrorMessage(unitName, e), e);
    }

    private static String makeErrorMessage(UnitName unitName, Throwable e) {
        String name = unitName.getDisplayString();
        return format(ERROR_MESSAGE_TEMPLATE, name, e.getMessage());
    }
}
