package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.AbstractCommand;

import static java.lang.String.format;

public class ClearSingleConfig extends AbstractCommand {

    private ConfigurationIndex configurationIndex;
    private UnitName unitName;

    public ClearSingleConfig(
            ConfigurationIndex configurationIndex, UnitName unitName) {
        this.configurationIndex = configurationIndex;
        this.unitName = unitName;
    }

    @Override
    public void execute() {
        try {
            configurationIndex.disposeOfName(unitName);
        } catch (Throwable e) {
            throw new ConfigSyncException(unitName, e);
        }
    }

    @Override
    public String getSuccessMessage() {
        String name = unitName.getDisplayString();
        return format("%s - %s", super.getSuccessMessage(), name);
    }
}
