package org.leonardo.environment;

/**
 * Created by Lrteixeira on 22/07/2016.
 */
public interface UnitFactoryRegistry {
    void registerFactoryForToken(UnitFactory unitFactory, String surrogate);
    boolean hasUnitFactoryForToken(String fabricationToken);
    String[] getRegisteredTokens();
}
