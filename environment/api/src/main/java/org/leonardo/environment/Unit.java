package org.leonardo.environment;

/**
 * Created by Lrteixeira on 31/08/2015.
 */
public interface Unit {
    UnitName getName();
    UnitState getState();
    void execute(Resource resource) throws ResourceExecutionException;
}
