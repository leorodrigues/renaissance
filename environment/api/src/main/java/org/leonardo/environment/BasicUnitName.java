package org.leonardo.environment;

public final class BasicUnitName extends CommonUnitName {

    public static final transient BasicUnitName INSTANCE = new BasicUnitName();

    private static final String IDENTIFICATION = "basic";

    private static final int INSTANCE_NUMBER = 0;

    private BasicUnitName() {
        super(IDENTIFICATION, INSTANCE_NUMBER, DEFAULT_FABRICATION_TOKEN);
    }

    @Override
    public boolean equals(Object object) {
        return object == INSTANCE || super.equals(object);
    }
}
