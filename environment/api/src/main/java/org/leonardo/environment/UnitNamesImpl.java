package org.leonardo.environment;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by leonardo on 31/01/2016.
 */
public class UnitNamesImpl implements UnitNames {

    private LinkedList<UnitName> unitNames =
            new LinkedList<>();

    @Override
    public void clear() {
        unitNames.clear();
    }

    @Override
    public void add(UnitName unitName) {
        unitNames.add(unitName);
    }

    @Override
    public String join(String separator) {
        if (unitNames.size() == 0)
            return "";
        StringBuilder buffer = new StringBuilder();
        Iterator<UnitName> iterator = unitNames.iterator();
        serializeFirstName(buffer, iterator.next());
        while (iterator.hasNext())
            serializeNextName(buffer, iterator.next(), separator);
        return buffer.toString();
    }

    @Override
    public UnitNames makeCopy() {
        UnitNamesImpl result = new UnitNamesImpl();
        result.unitNames.addAll(unitNames);
        return result;
    }

    private void serializeNextName(
            StringBuilder buffer,
            UnitName name,
            String separator) {
        buffer.append(separator);
        buffer.append(name.getDisplayString());
    }

    private void serializeFirstName(
            StringBuilder buffer, UnitName name) {
        buffer.append(name.getDisplayString());
    }
}
