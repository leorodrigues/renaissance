package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.AbstractCommand;

import static java.lang.String.format;

public class SetSingleConfig extends AbstractCommand {

    private ConfigurationIndex configurationIndex;
    private UnitName unitName;
    private Configuration configuration;

    public SetSingleConfig(
            ConfigurationIndex configurationIndex,
            UnitName unitName,
            Configuration configuration) {
        this.configurationIndex = configurationIndex;
        this.unitName = unitName;
        this.configuration = configuration;
    }

    @Override
    public void execute() {
        try {
            configurationIndex.declareName(unitName).copyFrom(configuration);
        } catch (Throwable e) {
            throw new ConfigSyncException(unitName, e);
        }
    }

    @Override
    public String getSuccessMessage() {
        String name = unitName.getDisplayString();
        return format("%s - %s", super.getSuccessMessage(), name);
    }
}
