package org.leonardo.environment;

/**
 * Created by Lrteixeira on 28/09/2015.
 */
public interface UnitFactory {
    ControlledUnit create(UnitName name);
}
