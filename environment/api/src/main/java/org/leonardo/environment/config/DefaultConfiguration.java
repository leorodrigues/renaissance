package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;
import org.leonardo.environment.ValueExpander;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;

/**
 * Created by leonardo on 17/02/2016.
 */
public class DefaultConfiguration implements Configuration, Serializable {

    private transient ValueExpander valueExpander = null;

    private HashSet<ConfigEntry> entries = new HashSet<>();

    private DefaultConfiguration() { }

    public DefaultConfiguration(UnitName name) {
        setName(name);
    }

    public DefaultConfiguration(Configuration c) {
        this.copyFrom(c);
    }

    @Override
    public Configuration makeCopy(UnitName name) {
        DefaultConfiguration result = new DefaultConfiguration();
        for (ConfigEntry e : entries) result.entries.add(e.makeCopy());
        result.setName(name);
        return result;
    }

    @Override
    public String get(String name) {
        ConfigEntry entry = findEntryByName(name);
        if (entry == null)
            return null;
        return entry.getEffectiveValue();
    }

    @Override
    public void set(String name, String value) {
        unset(name);
        entries.add(makeEntry(name, value));
    }

    @Override
    public void unset(String name) {
        ConfigEntry entry = findEntryByName(name);
        if (entry != null)
            entries.remove(entry);
    }

    @Override
    public void setValueExpander(ValueExpander valueExpander) {
        this.valueExpander = valueExpander;
    }

    @Override
    public void describe(ConfigurationDescriptionBuilder descriptionBuilder) {
        for (ConfigEntry e : entries)
            e.describe(descriptionBuilder);
    }

    @Override
    public void copyFrom(Configuration anotherConfiguration) {
        entries.clear();
        entries.addAll(anotherConfiguration.getEntries());
    }

    @Override
    public Set<ConfigEntry> getEntries() {
        return unmodifiableSet(entries);
    }

    private ConfigEntry findEntryByName(String name) {
        for (ConfigEntry e : entries)
            if (e.getName().equals(name))
                return e;
        return null;
    }

    private ConfigEntry makeEntry(String name, String value) {
        if (valueExpander == null)
            return new ConfigEntry(name, value);
        String expandedValue = valueExpander.expand(value, entries);
        return new ConfigEntry(name, value, expandedValue);
    }

    private void setName(UnitName name) {
        set("unit_name", name.getDisplayString());
    }
}
