package org.leonardo.environment;

/**
 * Created by leonardo on 07/04/2016.
 */
public interface UnitFactoryManager extends UnitFactoryRegistry {
    UnitFactory getFactoryInstanceWithToken(String fabricationToken);
}
