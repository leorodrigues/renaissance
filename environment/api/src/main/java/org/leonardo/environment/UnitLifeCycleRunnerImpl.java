package org.leonardo.environment;

import java.util.concurrent.locks.ReentrantLock;

import static java.lang.String.format;
import static org.leonardo.environment.UnitState.RUNNING;

/**
 * Created by Lrteixeira on 05/02/2016.
 */
public class UnitLifeCycleRunnerImpl implements UnitLifeCycleRunner {

    private static final String BOOT_ERROR_TEMPLATE =
        "Failed to boot unit '%s': %s";

    private static final String SHUTDOWN_ERROR_TEMPLATE =
        "Failed to shutdown unit '%s': %s";

    private ReentrantLock bootLock = new ReentrantLock();

    @Override
    public boolean tryBooting(ControlledUnit controlledUnit) {
        try {
            bootLock.lock();
            if (controlledUnit.getState() == RUNNING)
                return false;
            return controlledUnit.runBootSequence();
        } catch(Throwable e) {
            String message = makeBootErrorMessage(controlledUnit.getName(), e);
            throw new UnitLifeCycleControlException(message, e);
        } finally {
            bootLock.unlock();
        }
    }

    @Override
    public boolean tryShuttingDown(ControlledUnit controlledUnit) {
        try {
            bootLock.lock();
            if (controlledUnit.getState() != RUNNING)
                return false;
            return controlledUnit.runShutdownSequence();
        } catch (Throwable e) {
            UnitName name = controlledUnit.getName();
            String message = makeShutdownErrorMessage(name, e);
            throw new UnitLifeCycleControlException(message, e);
        } finally {
            bootLock.unlock();
        }
    }

    private String makeBootErrorMessage(UnitName name, Throwable e) {
        return format(BOOT_ERROR_TEMPLATE,
            name.getDisplayString(), e.getMessage());
    }

    private String makeShutdownErrorMessage(UnitName name, Throwable e) {
        return format(SHUTDOWN_ERROR_TEMPLATE,
            name.getDisplayString(), e.getMessage());
    }
}
