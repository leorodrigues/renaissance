/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.leonardo.environment;

/**
 *
 * @author aluno
 */
public interface UnitInstances {

    boolean add(ControlledUnit controlledUnit);

    boolean remove(ControlledUnit controlledUnit);

    boolean areAllDown();

    boolean isUnitAvailable(UnitName name);

    UnitInstances makeCopy();

    ControlledUnit findByNameOrFail(UnitName name);

    ControlledUnit findByNameOrCreate(UnitName name);

    ControlledUnit[] toArray();

    UnitName[] getNames();

    UnitInstances allRunning();
}
