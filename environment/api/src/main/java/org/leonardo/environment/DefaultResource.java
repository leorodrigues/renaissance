package org.leonardo.environment;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by leonardo on 08/03/2016.
 */
public class DefaultResource implements Resource {

    private String path;

    public DefaultResource(String path) {
        this.path = path;
    }

    @Override
    public String getDisplayString() {
        return path;
    }

    @Override
    public URL getURL() throws InvalidResourcePathException {
        if (path == null)
            throw new InvalidResourcePathException("Path cannot be null");
        if (path.isEmpty())
            throw new InvalidResourcePathException("Path cannot be empty");
        try {
            return new File(path).toURI().toURL();
        } catch (MalformedURLException e) {
            throw new InvalidResourcePathException(e);
        }
    }
}
