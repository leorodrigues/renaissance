package org.leonardo.environment;

public interface UnitName {

    String DEFAULT_FABRICATION_TOKEN = "surrogate";

    String getDisplayString();

    String getIdentification();

    String getFabricationToken();

    int getInstanceNumber();
}
