package org.leonardo.environment;

public class UnitLifeCycleControlException extends RuntimeException {
    public UnitLifeCycleControlException(String message, Throwable cause) {
        super(message, cause);
    }
}
