package org.leonardo.environment;

/**
 * Created by leonardo on 02/10/2015.
 */
public interface ControlledUnit extends Unit {
    void setName(UnitName name);
    boolean runBootSequence();
    boolean runShutdownSequence();
}
