package org.leonardo.environment.config;

import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.environment.StateBankController;

public class StateBankControllerImpl implements StateBankController {

    private final ControlPointStateBank controlPointStateBank;
    private final ConfigurationIndex configurationIndex;
    private final ConfigBankSync configBankSync;
    private final ConfigIndexSync configIndexSync;

    public StateBankControllerImpl(
            ControlPointStateBank controlPointStateBank,
            ConfigurationIndex configurationIndex,
            ConfigBankSync configBankSync,
            ConfigIndexSync configIndexSync) {

        this.controlPointStateBank = controlPointStateBank;
        this.configurationIndex = configurationIndex;
        this.configBankSync = configBankSync;
        this.configIndexSync = configIndexSync;
    }

    @Override
    public void load() {
        Configurations configurations =
                controlPointStateBank.allConfigurationsByName();
        configIndexSync.invoke(configurationIndex, configurations);
    }

    @Override
    public void save() {
        configBankSync.invoke(controlPointStateBank, configurationIndex);
    }
}
