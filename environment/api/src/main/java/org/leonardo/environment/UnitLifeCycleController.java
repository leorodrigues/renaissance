package org.leonardo.environment;

/**
 * Created by Lrteixeira on 18/12/2015.
 */
public interface UnitLifeCycleController {
    Unit getUnitByName(UnitName name);
    Unit bootUnit(UnitName name);
    void shutdownUnit(UnitName name);
    boolean isUnitAvailable(UnitName name);
    UnitName[] getRunningUnitsNames();
}
