package org.leonardo.environment.config;

import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.AbstractCommand;

import static java.lang.String.format;

public class UpdateConfig extends AbstractCommand {

    private final ControlPointStateBank controlPointStateBank;
    private final UnitName unitName;
    private ConfigurationIndex configurationIndex;

    public UpdateConfig(
            ControlPointStateBank controlPointStateBank,
            UnitName unitName,
            ConfigurationIndex configurationIndex) {
        this.controlPointStateBank = controlPointStateBank;
        this.unitName = unitName;
        this.configurationIndex = configurationIndex;
    }

    @Override
    public void execute() {
        try {
            Configuration configuration =
                    configurationIndex.getConfigurationForName(unitName);
            controlPointStateBank.delete(unitName);
            controlPointStateBank.insert(unitName, configuration);
        } catch (Throwable e) {
            throw new ConfigSyncException(unitName, e);
        }
    }

    @Override
    public String getSuccessMessage() {
        return format("%s - %s", super.getSuccessMessage(),
            unitName.getDisplayString());
    }
}
