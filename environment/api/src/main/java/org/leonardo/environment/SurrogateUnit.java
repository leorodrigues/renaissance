package org.leonardo.environment;

/**
 * Created by leonardo on 30/09/2015.
 */
public class SurrogateUnit extends AbstractUnit {

    @Override
    public void execute(Resource resource) throws ResourceExecutionException {
        String message = "Operation not available in surrogate unit.";
        throw new ResourceExecutionException(resource, message);
    }

    @Override
    public boolean runBootSequence() {
        return true;
    }

    @Override
    public boolean runShutdownSequence() {
        return true;
    }
}
