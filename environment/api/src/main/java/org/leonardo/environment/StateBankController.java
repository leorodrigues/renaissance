package org.leonardo.environment;

public interface StateBankController {
    void load();
    void save();
}
