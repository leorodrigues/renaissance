package org.leonardo.environment;

/**
 * Created by Lrteixeira on 05/02/2016.
 */
public interface UnitLifeCycleRunner {
    boolean tryBooting(ControlledUnit controlledUnit);
    boolean tryShuttingDown(ControlledUnit controlledUnit);
}
