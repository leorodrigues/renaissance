package org.leonardo.environment;

import java.net.URL;

/**
 * Created by Lrteixeira on 13/10/2015.
 */
public interface Resource {
    URL getURL() throws InvalidResourcePathException;
    String getDisplayString();
}
