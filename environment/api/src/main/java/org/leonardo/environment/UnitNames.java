package org.leonardo.environment;

/**
 * Created by leonardo on 10/01/2016.
 */
public interface UnitNames {
    void clear();
    UnitNames makeCopy();
    void add(UnitName unitName);
    String join(String s);
}
