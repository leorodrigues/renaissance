package org.leonardo.environment;

import org.leonardo.environment.config.Configuration;
import org.leonardo.renaissance.command.SerialCommandRunner;

/**
 * Created by leonardo on 20/02/2016.
 */
public interface ControlPointFactory {

    Configuration getDefaultConfiguration();

    void setDefaultConfiguration(Configuration defaultConfiguration);

    ControlPoint newControlPointInstance(
            ControlPointStateBank stateRepository,
            SerialCommandRunner serialCommandRunner);
}
