package org.leonardo.environment;

import org.leonardo.environment.config.Configuration;
import org.leonardo.environment.config.Configurations;

import java.util.Set;

public interface ControlPointStateBank {
    Configurations allConfigurationsByName();
    Set<UnitName> allUnitNames();
    void insert(UnitName unitName, Configuration c);
    void delete(UnitName unitName);
    void commit();
}
