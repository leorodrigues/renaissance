package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;

/**
 * Created by Lrteixeira on 17/02/2016.
 */
public class NameAlreadyDeclaredException extends Exception {

    public NameAlreadyDeclaredException(UnitName unitName) {
        super(makeMessage(unitName));
    }

    private static String makeMessage(UnitName unitName) {
        return String.format("The name '%s' is already declared.",
                unitName.getDisplayString());
    }
}
