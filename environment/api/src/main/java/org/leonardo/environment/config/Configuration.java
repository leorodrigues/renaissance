package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;
import org.leonardo.environment.ValueExpander;

import java.util.Set;

/**
 * Created by Lrteixeira on 12/02/2016.
 */
public interface Configuration {
    String get(String name);
    void set(String name, String value);
    void unset(String name);
    void setValueExpander(ValueExpander valueExpander);
    Configuration makeCopy(UnitName name);
    void copyFrom(Configuration anotherConfiguration);
    void describe(ConfigurationDescriptionBuilder descriptionBuilder);
    Set<ConfigEntry> getEntries();
}
