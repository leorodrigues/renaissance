package org.leonardo.environment.config;

import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.AbstractCommand;

import static java.lang.String.format;

public class InsertConfig extends AbstractCommand {

    private final ControlPointStateBank controlPointStateBank;
    private final ConfigurationIndex configurationIndex;
    private final UnitName unitName;

    public InsertConfig(
        ControlPointStateBank controlPointStateBank,
        UnitName unitName,
        ConfigurationIndex configurationIndex) {

        this.controlPointStateBank = controlPointStateBank;
        this.configurationIndex = configurationIndex;
        this.unitName = unitName;
    }

    @Override
    public void execute() {
        try {
            Configuration configuration =
                    configurationIndex.getConfigurationForName(unitName);
            controlPointStateBank.insert(unitName, configuration);
        } catch (Throwable e) {
            throw new ConfigSyncException(unitName, e);
        }
    }

    @Override
    public String getSuccessMessage() {
        return format("%s - %s", super.getSuccessMessage(),
            unitName.getDisplayString());
    }
}
