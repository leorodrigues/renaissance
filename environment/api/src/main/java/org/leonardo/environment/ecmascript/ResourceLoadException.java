package org.leonardo.environment.ecmascript;

public class ResourceLoadException extends RuntimeException {
    public ResourceLoadException(String message, Throwable cause) {
        super(message, cause);
    }
}
