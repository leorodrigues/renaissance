package org.leonardo.environment;

/**
 * Created by leonardo on 03/10/2015.
 */
public enum UnitState {
    INSTANTIATED, DOWN, RUNNING
}
