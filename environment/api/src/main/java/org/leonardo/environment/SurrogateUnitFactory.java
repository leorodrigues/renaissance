package org.leonardo.environment;

/**
 * Created by Lrteixeira on 29/09/2015.
 */
public class SurrogateUnitFactory implements UnitFactory {

    public SurrogateUnitFactory() {
    }

    @Override
    public ControlledUnit create(UnitName name) {
        SurrogateUnit result = new SurrogateUnit();
        result.setName(name);
        return result;
    }
}
