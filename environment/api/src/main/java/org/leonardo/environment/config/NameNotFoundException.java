package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;

/**
 * Created by Lrteixeira on 17/02/2016.
 */
public class NameNotFoundException extends Exception {
    public NameNotFoundException(UnitName name) {
        super(makeMessage(name));
    }

    private static String makeMessage(UnitName unitName) {
        return String.format("Name '%s' was not declared.",
                unitName.getDisplayString());
    }
}
