package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;

import static java.lang.String.format;

public class ConfigLockedException extends RuntimeException {

    private static final String TEMPLATE = "Unit '%s' is currently locked.";

    public ConfigLockedException(UnitName name) {
        super(format(TEMPLATE, name.getDisplayString()));
    }

    public ConfigLockedException(String message) {
        super(message);
    }
}
