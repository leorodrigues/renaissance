package org.leonardo.environment.ecmascript;

import org.leonardo.environment.Resource;

import java.io.InputStream;

/**
 * Created by leonardo on 11/03/2016.
 */
public interface ResourceLoader {
    InputStream openResource(Resource resource);
}
