package org.leonardo.environment;

import org.leonardo.environment.config.Configuration;
import org.leonardo.environment.config.ConfigurationGateKeeper;
import org.leonardo.environment.config.NameAlreadyDeclaredException;
import org.leonardo.environment.config.NameNotFoundException;

/**
 * Created by leonardo on 20/02/2016.
 */
public class ControlPointImpl implements ControlPoint {

    private UnitLifeCycleController unitLifeCycleController;

    private StateBankController stateBankController;

    private UnitFactoryRegistry unitFactoryRegistry;

    private ConfigurationGateKeeper configurationGateKeeper;

    public ControlPointImpl(
            UnitLifeCycleController unitLifeCycleController,
            ConfigurationGateKeeper configurationGateKeeper,
            UnitFactoryRegistry unitFactoryRegistry,
            StateBankController stateBankController) {
        this.unitLifeCycleController = unitLifeCycleController;
        this.stateBankController = stateBankController;
        this.unitFactoryRegistry = unitFactoryRegistry;
        this.configurationGateKeeper = configurationGateKeeper;
    }

    @Override
    public Configuration getConfigurationForName(UnitName name)
            throws NameNotFoundException {
        return configurationGateKeeper.getConfigurationForName(name);
    }

    @Override
    public Configuration declareName(UnitName name)
            throws NameAlreadyDeclaredException {
        return configurationGateKeeper.declareName(name);
    }

    @Override
    public UnitName[] getNames() {
        return configurationGateKeeper.getNames();
    }

    @Override
    public Configuration disposeOfName(UnitName name)
            throws NameNotFoundException {
        return configurationGateKeeper.disposeOfName(name);
    }

    @Override
    public boolean isNameDeclared(UnitName unitName) {
        return configurationGateKeeper.isNameDeclared(unitName);
    }

    @Override
    public Unit getUnitByName(UnitName name) {
        return unitLifeCycleController.getUnitByName(name);
    }

    @Override
    public Unit bootUnit(UnitName name) {
        try {
            return unitLifeCycleController.bootUnit(name);
        } catch (Throwable t) {
            configurationGateKeeper.unlockConfigurationForName(name);
            throw t;
        }
    }

    @Override
    public void shutdownUnit(UnitName name) {
        try {
            unitLifeCycleController.shutdownUnit(name);
        } finally {
            configurationGateKeeper.unlockConfigurationForName(name);
        }
    }

    @Override
    public boolean isUnitAvailable(UnitName name) {
        return unitLifeCycleController.isUnitAvailable(name);
    }

    @Override
    public UnitName[] getRunningUnitsNames() {
        return unitLifeCycleController.getRunningUnitsNames();
    }

    @Override
    public void registerFactoryForToken(
            UnitFactory unitFactory, String fabricationToken) {
        unitFactoryRegistry.registerFactoryForToken(
                unitFactory, fabricationToken);
    }

    @Override
    public boolean hasUnitFactoryForToken(String fabricationToken) {
        return unitFactoryRegistry.hasUnitFactoryForToken(fabricationToken);
    }

    @Override
    public String[] getRegisteredTokens() {
        return unitFactoryRegistry.getRegisteredTokens();
    }

    @Override
    public void loadState() {
        stateBankController.load();
    }

    @Override
    public void saveState() {
        stateBankController.save();
    }
}
