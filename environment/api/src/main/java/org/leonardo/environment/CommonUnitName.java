package org.leonardo.environment;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

/**
 * Created by Lrteixeira on 28/09/2015.
 */
public class CommonUnitName implements UnitName, Serializable {

    private static final transient Pattern validPattern =
        Pattern.compile("((\\w+(-\\w+)*)-)?(\\w+)(#(\\d+))?");

    private static final transient String ILLEGAL_PATTERN_MSG =
        "Unit name has irregular pattern.";

    private final String identification;
    private final int instanceNumber;
    private final String fabricationToken;

    protected CommonUnitName(String identification, int instanceNumber) {
        this(identification, instanceNumber, DEFAULT_FABRICATION_TOKEN);
    }

    protected CommonUnitName(
            String identification,
            int instanceNumber,
            String fabricationToken) {
        this.identification = identification;
        this.instanceNumber = instanceNumber;
        this.fabricationToken = fabricationToken;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CommonUnitName)) {
            return false;
        }
        CommonUnitName anotherName = (CommonUnitName)object;
        return anotherName.identification.equals(identification) &&
            anotherName.instanceNumber == instanceNumber &&
            anotherName.fabricationToken.equals(fabricationToken);
    }

    @Override
    public String toString() {
        return format("[env: %s %s %d]",
            fabricationToken, identification, instanceNumber);
    }

    @Override
    public String getDisplayString() {
        return format("%s-%s#%d",
            fabricationToken, identification, instanceNumber);
    }

    @Override
    public String getIdentification() {
        return identification;
    }

    @Override
    public String getFabricationToken() {
        return fabricationToken;
    }

    @Override
    public int getInstanceNumber() {
        return instanceNumber;
    }

    public static UnitName newName(String input) {
        Matcher matcher = validPattern.matcher(input);

        if (!matcher.matches())
            throw new IllegalArgumentException(ILLEGAL_PATTERN_MSG);

        return new CommonUnitName(
            getIdentification(matcher),
            getInstanceNumber(matcher),
            getFabricationToken(matcher));
    }

    public static UnitName newName(
            String identification, int instanceNumber) {
        return new CommonUnitName(identification, instanceNumber);
    }

    public static UnitName newName(
            String identification, int instanceNumber,
            String fabricationToken) {

        return new CommonUnitName(
                identification, instanceNumber, fabricationToken);
    }

    private static int getInstanceNumber(Matcher matcher) {
        String content = matcher.group(6);
        if (content == null)
            return 0;
        return Integer.parseInt(content);
    }

    private static String getFabricationToken(Matcher matcher) {
        String content = matcher.group(2);
        if (content == null)
            return DEFAULT_FABRICATION_TOKEN;
        return content;
    }

    private static String getIdentification(Matcher matcher) {
        return matcher.group(4);
    }
}
