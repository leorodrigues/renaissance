package org.leonardo.environment;

import org.leonardo.environment.config.*;
import org.leonardo.renaissance.command.SerialCommandRunner;

/**
 * Created by leonardo on 06/03/2016.
 */
public class ControlPointFactoryImpl implements ControlPointFactory {

    private Configuration defaultConfiguration =
            new DefaultConfiguration(BasicUnitName.INSTANCE);

    @Override
    public Configuration getDefaultConfiguration() {
        return defaultConfiguration;
    }

    @Override
    public void setDefaultConfiguration(Configuration defaultConfiguration) {
        this.defaultConfiguration = defaultConfiguration;
    }

    @Override
    public ControlPoint newControlPointInstance(
            ControlPointStateBank controlPointStateBank,
            SerialCommandRunner serialCommandRunner) {

        ConfigurationIndexImpl configurationIndex =
                new ConfigurationIndexImpl(getDefaultConfiguration());

        SurrogateUnitFactory surrogateUnitFactory =
                new SurrogateUnitFactory();

        UnitFactoryManagerImpl unitFactoryManager =
                new UnitFactoryManagerImpl(surrogateUnitFactory);

        unitFactoryManager.registerFactoryForToken(
                surrogateUnitFactory, UnitName.DEFAULT_FABRICATION_TOKEN);

        UnitLifeCycleControllerImpl lifeCycleController =
                new UnitLifeCycleControllerImpl(
                    new UnitLifeCycleRunnerImpl(),
                    new UnitInstancesImpl(unitFactoryManager));

        ConfigurationGateKeeper configurationGateKeeper =
                new ConfigurationGateKeeper(configurationIndex);

        StateBankControllerImpl stateBankController =
                new StateBankControllerImpl(
                    controlPointStateBank,
                        configurationGateKeeper,
                        new ConfigBankSyncImpl(serialCommandRunner),
                        new ConfigIndexSyncImpl(serialCommandRunner));

        return new ControlPointImpl(
            lifeCycleController,
            configurationGateKeeper,
            unitFactoryManager,
            stateBankController);
    }
}
