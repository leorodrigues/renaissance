package org.leonardo.environment.config;

import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.AbstractCommand;

import static java.lang.String.format;

public class DeleteConfig extends AbstractCommand {

    private final ControlPointStateBank controlPointStateBank;
    private final UnitName unitName;

    public DeleteConfig(
        ControlPointStateBank controlPointStateBank,
        UnitName unitName) {

        this.controlPointStateBank = controlPointStateBank;
        this.unitName = unitName;
    }

    @Override
    public void execute() {
        try {
            controlPointStateBank.delete(unitName);
        } catch (Throwable e) {
            throw new ConfigSyncException(unitName, e);
        }
    }

    @Override
    public String getSuccessMessage() {
        return format("%s - %s", super.getSuccessMessage(),
            unitName.getDisplayString());
    }
}
