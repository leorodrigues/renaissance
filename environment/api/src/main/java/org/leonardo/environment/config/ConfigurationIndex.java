package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;

/**
 * Created by Lrteixeira on 11/02/2016.
 */
public interface ConfigurationIndex {
    Configuration getConfigurationForName(UnitName name)
            throws NameNotFoundException;
    Configuration declareName(UnitName name)
            throws NameAlreadyDeclaredException;
    Configuration disposeOfName(UnitName name)
            throws NameNotFoundException;
    UnitName[] getNames();
    boolean isNameDeclared(UnitName unitName);
}
