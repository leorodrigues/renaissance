package org.leonardo.environment;

/**
 * Created by leonardo on 11/03/2016.
 */
public class InvalidResourcePathException extends Exception {
    public InvalidResourcePathException(Throwable cause) {
        super(cause);
    }

    public InvalidResourcePathException(String message) {
        super(message);
    }
}
