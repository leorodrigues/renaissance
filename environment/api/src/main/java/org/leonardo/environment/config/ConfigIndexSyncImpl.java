package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.Command;
import org.leonardo.renaissance.command.SerialCommandRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.complement;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.intersection;

public class ConfigIndexSyncImpl implements ConfigIndexSync {

    private SerialCommandRunner serialCommandRunner;

    public ConfigIndexSyncImpl(SerialCommandRunner serialCommandRunner) {
        this.serialCommandRunner = serialCommandRunner;
    }

    @Override
    public void invoke(
            ConfigurationIndex configurationIndex,
            Configurations configurations) {

        List<UnitName> currentNames = asList(configurationIndex.getNames());
        Set<UnitName> loadedNames = configurations.keySet();
        ArrayList<Command> commands = new ArrayList<>();

        addClearCommands(
            commands, configurationIndex, loadedNames, currentNames);

        addReloadCommands(
            commands, configurationIndex, loadedNames,
                currentNames, configurations);

        addLoadCommands(
            commands, configurationIndex, loadedNames,
                currentNames, configurations);

        serialCommandRunner.run(commands);
    }

    private void addClearCommands(
            ArrayList<Command> commands,
            ConfigurationIndex configurationIndex,
            Collection<UnitName> loadedNames,
            Collection<UnitName> currentNames) {

        for (UnitName n : complement(currentNames, loadedNames))
            commands.add(makeClearCommand(configurationIndex, n));
    }

    private void addReloadCommands(
            ArrayList<Command> commands,
            ConfigurationIndex configurationIndex,
            Collection<UnitName> loadedNames,
            Collection<UnitName> currentNames,
            Configurations configurations) {

        for (UnitName n : intersection(loadedNames, currentNames))
            commands.add(makeReloadCommand(
                    configurationIndex, n, configurations.get(n)));
    }

    private void addLoadCommands(
            ArrayList<Command> commands,
            ConfigurationIndex configurationIndex,
            Collection<UnitName> loadedNames,
            Collection<UnitName> currentNames,
            Configurations configurations) {

        for (UnitName n : complement(loadedNames, currentNames))
            commands.add(makeLoadCommand(
                configurationIndex, n, configurations.get(n)));
    }

    private Command makeReloadCommand(
            ConfigurationIndex configurationIndex,
            UnitName unitName,
            Configuration configuration) {

        ClearSingleConfig command =
            makeClearCommand(configurationIndex, unitName);

        command.setNext(makeLoadCommand(
            configurationIndex, unitName, configuration));

        return command;
    }

    private ClearSingleConfig makeClearCommand(
            ConfigurationIndex configurationIndex,
            UnitName unitName) {

        return new ClearSingleConfig(
                configurationIndex, unitName);
    }

    private Command makeLoadCommand(
            ConfigurationIndex configurationIndex,
            UnitName unitName,
            Configuration configuration) {

        return new SetSingleConfig(
                configurationIndex, unitName, configuration);
    }
}
