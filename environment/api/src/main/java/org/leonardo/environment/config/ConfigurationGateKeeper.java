package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;

import java.util.HashMap;

import static java.lang.String.format;

/**
 * Created by leonardo on 21/02/2016.
 */
public class ConfigurationGateKeeper implements ConfigurationIndex {

    private static final String NAME_ALREADY_KEPT_TEMPLATE =
            "Name '%s' is already being kept.";

    private static final String NAME_NOT_KEPT_TEMPLATE =
            "Name '%s' is not being kept.";

    private ConfigurationIndex configurationIndex;
    private HashMap<UnitName, ConfigurationGate> configurationGates =
            new HashMap<>();

    public ConfigurationGateKeeper(ConfigurationIndex configurationIndex) {
        this.configurationIndex = configurationIndex;
    }

    @Override
    public Configuration getConfigurationForName(UnitName name)
            throws NameNotFoundException {
        return getGateForName(name);
    }

    @Override
    public Configuration declareName(UnitName name)
            throws NameAlreadyDeclaredException {
        return keep(configurationIndex.declareName(name), name);
    }

    @Override
    public Configuration disposeOfName(UnitName name)
            throws NameNotFoundException {
        if (getGateForName(name).isLocked())
            throw new ConfigLockedException(name);
        configurationGates.remove(name);
        return configurationIndex.disposeOfName(name);
    }

    @Override
    public UnitName[] getNames() {
        return configurationIndex.getNames();
    }

    @Override
    public boolean isNameDeclared(UnitName unitName) {
        return configurationIndex.isNameDeclared(unitName);
    }

    public void lockConfigurationForName(UnitName name) {
        if (configurationGates.containsKey(name)) {
            configurationGates.get(name).lock();
            return;
        }
        throw new ConfigKeepingException(makeMsgNameNotBeingKept(name));
    }

    public void unlockConfigurationForName(UnitName name) {
        if (configurationGates.containsKey(name)) {
            configurationGates.get(name).unlock();
            return;
        }
        throw new ConfigKeepingException(makeMsgNameNotBeingKept(name));
    }

    private ConfigurationGate getGateForName(UnitName name) {
        if (configurationGates.containsKey(name))
            return configurationGates.get(name);
        throw new ConfigKeepingException(makeMsgNameNotBeingKept(name));
    }

    private ConfigurationGate keep(
            Configuration delegate, UnitName name) {
        if (!configurationGates.containsKey(name))
            return wrapConfiguration(delegate, name);
        throw new ConfigKeepingException(makeMsgNameIsAlreadyBeingKept(name));
    }

    private ConfigurationGate wrapConfiguration(
            Configuration delegate, UnitName name) {
        ConfigurationGate result = new ConfigurationGate(delegate);
        configurationGates.put(name, result);
        return result;
    }

    private String makeMsgNameIsAlreadyBeingKept(UnitName name) {
        return format(NAME_ALREADY_KEPT_TEMPLATE, name.getDisplayString());
    }

    private String makeMsgNameNotBeingKept(UnitName name) {
        return format(NAME_NOT_KEPT_TEMPLATE, name.getDisplayString());
    }
}
