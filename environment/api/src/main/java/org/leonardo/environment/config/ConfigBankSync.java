package org.leonardo.environment.config;

import org.leonardo.environment.ControlPointStateBank;

public interface ConfigBankSync {
    void invoke(
        ControlPointStateBank controlPointStateBank,
        ConfigurationIndex configurationIndex);
}
