package org.leonardo.environment.config;

import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.renaissance.command.AbstractCommand;

public class CommitChanges extends AbstractCommand {
    private final ControlPointStateBank controlPointStateBank;

    public CommitChanges(ControlPointStateBank controlPointStateBank) {
        this.controlPointStateBank = controlPointStateBank;
    }

    @Override
    public void execute() {
        controlPointStateBank.commit();
    }
}

