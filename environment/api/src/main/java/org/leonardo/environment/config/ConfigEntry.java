package org.leonardo.environment.config;

import java.io.Serializable;

public class ConfigEntry implements Serializable, Cloneable {
    private String name;
    private String value;
    private String expandedValue;

    public ConfigEntry(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public ConfigEntry(String name, String value, String expandedValue) {
        this(name, value);
        this.expandedValue = expandedValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConfigEntry)) return false;

        ConfigEntry that = (ConfigEntry) o;

        if (!getName().equals(that.getName())) return false;
        if (!getValue().equals(that.getValue())) return false;
        return getExpandedValue() != null
            ? getExpandedValue().equals(that.getExpandedValue())
            : that.getExpandedValue() == null;
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getValue().hashCode();
        result = 31 * result + computeExpandedValuesHashCode();
        return result;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getExpandedValue() {
        return expandedValue;
    }

    public boolean isExpanded() {
        return expandedValue != null && !expandedValue.isEmpty();
    }

    public ConfigEntry makeCopy() {
        return new ConfigEntry(name, value, expandedValue);
    }

    public void describe(ConfigurationDescriptionBuilder descriptionBuilder) {
        if (!isExpanded()) {
            descriptionBuilder.append(getName(), getValue());
            return;
        }
        descriptionBuilder.append(getName(), getValue(), getExpandedValue());
    }

    public String getEffectiveValue() {
        if (isExpanded())
            return getExpandedValue();
        return getValue();
    }

    private int computeExpandedValuesHashCode() {
        return getExpandedValue() != null ? getExpandedValue().hashCode() : 0;
    }
}
