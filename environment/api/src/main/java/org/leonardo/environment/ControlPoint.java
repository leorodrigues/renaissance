package org.leonardo.environment;

import org.leonardo.environment.config.ConfigurationIndex;

/**
 * Created by leonardo on 20/02/2016.
 */
public interface ControlPoint
        extends UnitLifeCycleController, ConfigurationIndex, UnitFactoryRegistry {
    void loadState();
    void saveState();
}
