package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;
import org.leonardo.environment.ValueExpander;

import java.util.Set;

import static java.lang.String.format;

/**
 * Created by leonardo on 20/02/2016.
 */
public class ConfigurationGate implements Configuration {

    private static final String ACCESS_DENIED_FOR_SET_TEMPLATE =
        "Unable to set variable '%s'. This instance is locked.";

    private static final String ACCESS_DENIED_FOR_UNSET_TEMPLATE =
        "Unable to unset variable '%s'. This instance is locked.";

    private static final String ACCESS_DENIED_FOR_COPY_MESSAGE =
        "Unable to copy values from another instance. This instance is locked.";

    private static final String SET_EXPANDER_NOT_SUPPORTED_MESSAGE =
        "This instance does not support changing the ValueExpander.";

    private static final String COPY_NOT_SUPPORTED_MESSAGE =
        "This instance does not support copying itself.";

    private Configuration delegate;

    private boolean isLocked = false;

    public ConfigurationGate(Configuration delegate) {
        this.delegate = delegate;
    }

    @Override
    public Configuration makeCopy(UnitName name) {
        throw new UnsupportedOperationException(COPY_NOT_SUPPORTED_MESSAGE);
    }

    @Override
    public void describe(ConfigurationDescriptionBuilder descriptionBuilder) {
        delegate.describe(descriptionBuilder);
    }

    @Override
    public String get(String name) {
        return delegate.get(name);
    }

    @Override
    public void set(String name, String value) {
        if (isLocked) {
            String message = format(ACCESS_DENIED_FOR_SET_TEMPLATE, name);
            throw new ConfigLockedException(message);
        }
        delegate.set(name, value);
    }

    @Override
    public void unset(String name) {
        if (isLocked) {
            String message = format(ACCESS_DENIED_FOR_UNSET_TEMPLATE, name);
            throw new ConfigLockedException(message);
        }
        delegate.unset(name);
    }

    @Override
    public void setValueExpander(ValueExpander valueExpander) {
        throw new UnsupportedOperationException(
                SET_EXPANDER_NOT_SUPPORTED_MESSAGE);
    }

    @Override
    public void copyFrom(Configuration anotherConfiguration) {
        if (isLocked)
            throw new ConfigLockedException(ACCESS_DENIED_FOR_COPY_MESSAGE);
        delegate.copyFrom(anotherConfiguration);
    }

    @Override
    public Set<ConfigEntry> getEntries() {
        return delegate.getEntries();
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void lock() {
        isLocked = true;
    }

    public void unlock() {
        isLocked = false;
    }
}
