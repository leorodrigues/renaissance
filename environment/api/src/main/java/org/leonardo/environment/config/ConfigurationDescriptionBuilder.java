package org.leonardo.environment.config;

public interface ConfigurationDescriptionBuilder {
    void append(String name, String value, String expandedValue);
    void append(String name, String value);
}
