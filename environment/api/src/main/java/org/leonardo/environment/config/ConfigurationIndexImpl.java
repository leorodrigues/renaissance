package org.leonardo.environment.config;

import org.leonardo.environment.UnitName;

import java.util.Set;

public class ConfigurationIndexImpl implements ConfigurationIndex {

    private Configuration defaultConfiguration;

    private Configurations configurations = new Configurations();

    public ConfigurationIndexImpl(
            Configuration defaultConfiguration) {
        this.defaultConfiguration = defaultConfiguration;
    }

    @Override
    public Configuration disposeOfName(UnitName unitName) {
        return getConfigurations().remove(unitName);
    }

    @Override
    public Configuration getConfigurationForName(UnitName name)
            throws NameNotFoundException {
        Configurations configurations = getConfigurations();

        if (!configurations.containsKey(name))
            throw new NameNotFoundException(name);
        return configurations.get(name);
    }

    @Override
    public Configuration declareName(UnitName name)
            throws NameAlreadyDeclaredException {
        Configurations configurations = getConfigurations();

        if (configurations.containsKey(name))
            throw new NameAlreadyDeclaredException(name);
        Configuration configuration = defaultConfiguration.makeCopy(name);
        configurations.put(name, configuration);
        return configuration;
    }

    @Override
    public UnitName[] getNames() {
        Set<UnitName> names = getConfigurations().keySet();
        return names.toArray(new UnitName[names.size()]);
    }

    @Override
    public boolean isNameDeclared(UnitName unitName) {
        return configurations.containsKey(unitName);
    }

    protected final Configurations getConfigurations() {
        return configurations;
    }
}
