package org.leonardo.davinci.server;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by leonardo on 10/04/2017.
 */
public class DefaultRequestFactory extends AbstractRequestFactory {
    public AbstractRequest makeRequest(HttpServletRequest servletRequest) {
        return new AbstractRequest() {
            @Override
            public String toString() {
                return super.toString();
            }
        };
    }
}
