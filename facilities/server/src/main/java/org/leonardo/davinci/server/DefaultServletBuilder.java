package org.leonardo.davinci.server;

public class DefaultServletBuilder {

    private SimpleServer server;
    private Configuration configuration;
    private Controller controller;
    private AbstractRequestFactory requestFactory;
    private AbstractResponseFactory responseFactory;

    public void setServer(SimpleServer server) {
        this.server = server;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void setRequestFactory(AbstractRequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    public void setResponseFactory(AbstractResponseFactory responseFactory) {
        this.responseFactory = responseFactory;
    }

    public DefaultServlet newServlet() {
        if (server == null)
            throw new RuntimeException("Server is missing.");
        if (configuration == null)
            throw new RuntimeException("Configuration is missing.");
        if (controller == null)
            throw new RuntimeException("Controller is missing.");
        if (requestFactory == null)
            throw new RuntimeException("Request factory is missing.");
        if (responseFactory == null)
            throw new RuntimeException("Response factory is missing.");
        return new DefaultServlet(server, configuration, controller,
                requestFactory, responseFactory);
    }
}
