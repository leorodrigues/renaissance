package org.leonardo.davinci.server;

/**
 * Created by leonardo on 07/04/2017.
 */
public class SystemController implements Controller {
    public void get(
            SimpleServer embeddedServer,
            Configuration configuration,
            Request request,
            Response response) {
        response.setRepresentation(configuration);
    }
}
