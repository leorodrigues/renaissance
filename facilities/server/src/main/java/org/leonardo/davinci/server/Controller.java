package org.leonardo.davinci.server;

/**
 * Created by leonardo on 07/04/2017.
 */
public interface Controller {
    void get(
            SimpleServer embeddedServer,
            Configuration configuration,
            Request request,
            Response response);
}
