package org.leonardo.davinci.server;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by leonardo on 07/04/2017.
 */
public abstract class AbstractResponse implements Response {

    private Object representation;

    public void setRepresentation(Object representation) {
        this.representation = representation;
    }

    public Object getRepresentation() {
        return representation;
    }

    public void putHeadersOn(HttpServletResponse servletResponse) {

    }

    public abstract void writeRepresentationTo(HttpServletResponse servletResponse)
            throws IOException;
}
