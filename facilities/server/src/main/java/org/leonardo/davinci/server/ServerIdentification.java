package org.leonardo.davinci.server;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by jayme on 02/04/2017.
 */
public class ServerIdentification {

    @JsonIgnore
    public static final ServerIdentification NO_NAME =
            new ServerIdentification("No Name");

    private String name;

    public String getName() {
        return name;
    }

    public ServerIdentification(String name) {
        this.name = name;
    }
}
