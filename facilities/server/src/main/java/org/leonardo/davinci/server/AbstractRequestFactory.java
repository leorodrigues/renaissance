package org.leonardo.davinci.server;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by leonardo on 10/04/2017.
 */
public abstract class AbstractRequestFactory {
    public abstract AbstractRequest makeRequest(HttpServletRequest servletRequest);
}
