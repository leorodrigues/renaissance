package org.leonardo.davinci.server;

/**
 * Created by jayme on 01/04/2017.
 */
public class Greeting {
    private String message;

    public Greeting(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
