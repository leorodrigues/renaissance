package org.leonardo.davinci.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by leonardo on 10/04/2017.
 */
public class DefaultResponseFactory extends AbstractResponseFactory {

    private static final ObjectMapper mapper = new ObjectMapper();

    public AbstractResponse makeResponse() {
        return new AbstractResponse() {
            @Override
            public void writeRepresentationTo(HttpServletResponse servletResponse)
                    throws IOException {

                Object representation = getRepresentation();
                if (representation == null)
                    return;
                ObjectWriter writer = mapper.writer();
                ServletOutputStream outputStream = servletResponse.getOutputStream();
                writer.writeValue(outputStream, representation);
            }
        };
    }
}
