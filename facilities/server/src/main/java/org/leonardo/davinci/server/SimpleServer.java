package org.leonardo.davinci.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Logger;
import org.eclipse.jetty.util.log.StdErrLog;

import javax.servlet.http.HttpServlet;

import static java.lang.String.format;
import static org.eclipse.jetty.servlet.ServletContextHandler.SESSIONS;

/**
 * Created by leonardo on 29/03/2017.
 */
public class SimpleServer {

    private static final Logger log = new StdErrLog("root");

    public static final String APPLICATION_CONFIGURED_MSG =
            "Application configured at URI '/%s'.";

    public static final String CONFIGURING_ENDPOINT_MSG =
            "Configuring maintenance endpoint uri '%s'";

    public static final String SYSTEM_MAINTENANCE_URI =
            "/maintenance/system/configuration";

    public static final int DEFAULT_PORT = 8080;

    private Server jettyInstance;

    private final AbstractRequestFactory requestFactory;

    private final AbstractResponseFactory responseFactory;

    private final Configuration configuration;

    public SimpleServer(int port, String applicationName,
        AbstractRequestFactory requestFactory,
        AbstractResponseFactory responseFactory) {
        configuration = new Configuration(port, applicationName);
        this.requestFactory = requestFactory;
        this.responseFactory = responseFactory;
    }

    public static void main(String[] args) throws Exception {
        new SimpleServer(DEFAULT_PORT, "server",
                new DefaultRequestFactory(),
                new DefaultResponseFactory()).start();
    }

    public void start() throws Exception {
        jettyInstance = new Server(configuration.getPort());

        ServletContextHandler servletContextHandler =
                createRootContextHandler(configuration.getContextPath());

        jettyInstance.setHandler(servletContextHandler);

        HttpServlet servlet = createSystemServlet(
                this, configuration, requestFactory,
                responseFactory);

        configureEndpoint(
                servletContextHandler, servlet, SYSTEM_MAINTENANCE_URI);

        jettyInstance.start();
        jettyInstance.join();
    }

    public void stop() throws Exception {
        jettyInstance.stop();
    }

    protected HttpServlet createSystemServlet(
            SimpleServer server, Configuration configuration,
            AbstractRequestFactory requestFactory,
            AbstractResponseFactory responseFactory) {
        DefaultServletBuilder builder = new DefaultServletBuilder();
        builder.setServer(server);
        builder.setConfiguration(configuration);
        builder.setController(new SystemController());
        builder.setRequestFactory(requestFactory);
        builder.setResponseFactory(responseFactory);
        return builder.newServlet();
    }

    protected ServletContextHandler createRootContextHandler(String baseName) {
        ServletContextHandler result = new ServletContextHandler(SESSIONS);
        log.info(format(APPLICATION_CONFIGURED_MSG, baseName));
        result.setContextPath("/" + baseName);
        return result;
    }

    private static void configureEndpoint(
            ServletContextHandler servletContextHandler,
            HttpServlet servlet, String uri) {
        log.info(format(CONFIGURING_ENDPOINT_MSG, uri));
        ServletHolder holder = new ServletHolder(servlet);
        servletContextHandler.addServlet(holder, uri);
    }
}
