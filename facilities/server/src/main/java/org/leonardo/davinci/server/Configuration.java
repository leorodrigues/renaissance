package org.leonardo.davinci.server;

import static org.leonardo.davinci.server.ServerIdentification.NO_NAME;

/**
 * Created by jayme on 02/04/2017.
 */
public class Configuration {
    private static final int DEFAULT_PORT = 8080;

    private int port = DEFAULT_PORT;
    private String contextPath = "/application";
    private ServerIdentification identification = NO_NAME;

    public Configuration(int port, String contextPath) {
        this.port = port;
        this.contextPath = contextPath;
    }

    public int getPort() {
        return port;
    }

    public String getContextPath() {
        return contextPath;
    }

    public ServerIdentification getIdentification() {
        return identification;
    }
}
