package org.leonardo.davinci.server;

/**
 * Created by leonardo on 07/04/2017.
 */
public interface Response {

    Object getRepresentation();
    void setRepresentation(Object representation);

}
