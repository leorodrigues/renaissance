package org.leonardo.davinci.server;

/**
 * Created by leonardo on 10/04/2017.
 */
public abstract class AbstractResponseFactory {
    public abstract AbstractResponse makeResponse();
}
