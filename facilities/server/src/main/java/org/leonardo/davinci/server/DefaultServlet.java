package org.leonardo.davinci.server;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by leonardo on 31/03/2017.
 */
public class DefaultServlet extends HttpServlet {

    private static final ObjectMapper mapper = new ObjectMapper();

    private SimpleServer server;
    private Configuration configuration;
    private Controller controller;
    private AbstractRequestFactory requestFactory;
    private AbstractResponseFactory responseFactory;

    public DefaultServlet(
            SimpleServer server,
            Configuration configuration,
            Controller controller,
            AbstractRequestFactory requestFactory,
            AbstractResponseFactory responseFactory) {
        this.server = server;
        this.configuration = configuration;
        this.controller = controller;
        this.requestFactory = requestFactory;
        this.responseFactory = responseFactory;
    }

    @Override
    protected void doGet(HttpServletRequest servletRequest,
        HttpServletResponse servletResponse)
            throws ServletException, IOException {

        AbstractRequest request = requestFactory.makeRequest(servletRequest);

        AbstractResponse response = responseFactory.makeResponse();

        controller.get(server, configuration, request, response);

        response.putHeadersOn(servletResponse);
        response.writeRepresentationTo(servletResponse);
    }
}
