package org.leonardo.testing.learning;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Logger;
import org.eclipse.jetty.util.log.StdErrLog;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static java.lang.String.format;
import static org.eclipse.jetty.servlet.ServletContextHandler.SESSIONS;

public class MultiConnectorJetty {

    private static final Logger log =
            new StdErrLog("root");

    private static final String APP_CONFIG_MSG =
            "Configured message app at 'http://%s:%d%s%s'";

    private Server server;

    private HandlerCollection handlerCollection = new HandlerCollection();

    public MultiConnectorJetty() {
        server = new Server();
        server.setHandler(handlerCollection);
        log.info("Server instantiated");
    }

    public void run() throws Exception {
        server.start();
        server.join();
    }

    public ServletContextHandler configureApp(
            int port, String host, String contextPath,
            String servletPath, String message) throws Exception {

        ConnectorBinding connectorBinding =
                makeServerConnector(server, port, host);

        String connectorName = format("@%s", connectorBinding.name);
        ServletContextHandler contextHandler =
                makeServletContextHandler(connectorName, contextPath);

        Servlet servletInstance = makeMessageServlet(message);
        bindServlet(contextHandler, servletInstance, servletPath);

        handlerCollection.addHandler(contextHandler);
        server.addConnector(connectorBinding.connectorInstance);

        contextHandler.start();

        log.info(makeConfigAppMessage(port, host, contextPath, servletPath));

        return contextHandler;
    }

    public static void main(String[] args) throws Exception {

        final MultiConnectorJetty server = new MultiConnectorJetty();

        server.configureApp(
            8081,
            "127.0.0.1",
            "/greetings",
            "/english",
            "Hello world!");

        server.configureApp(
                8082,
                "127.0.0.1",
                "/greetings",
                "/portuguese",
                "Olá mundo!");

        server.run();

    }

    private static void bindServlet(
            ServletContextHandler contextHandler, Servlet servletInstance,
            String servletPath) {
        ServletHolder servletHolder = new ServletHolder(servletInstance);
        contextHandler.addServlet(servletHolder, servletPath);
    }

    private static ServletContextHandler makeServletContextHandler(
            String connectorName, String contextPath) {
        ServletContextHandler contextHandler =
                new ServletContextHandler(SESSIONS);
        contextHandler.setVirtualHosts(new String[] { connectorName });
        contextHandler.setContextPath(contextPath);
        return contextHandler;
    }

    private static ConnectorBinding makeServerConnector(
            Server server, int port, String host) {
        String connectorName = UUID.randomUUID().toString();
        ServerConnector internalConnector = new ServerConnector(server);
        internalConnector.setPort(port);
        internalConnector.setHost(host);
        internalConnector.setName(connectorName);
        return new ConnectorBinding(internalConnector, connectorName);
    }

    private static Servlet makeMessageServlet(final String message) {
        return new HttpServlet() {
            @Override
            protected void doGet(
                    HttpServletRequest req, HttpServletResponse resp)
                    throws ServletException, IOException {
                resp.getWriter().append(message);
            }
        };
    }

    private String makeConfigAppMessage(
            int port, String host, String contextPath, String servletPath) {
        return format(APP_CONFIG_MSG, host, port, contextPath, servletPath)
                .replace("//", "/");
    }

    public static class ConnectorBinding {
        String name;
        ServerConnector connectorInstance;

        public ConnectorBinding(
                ServerConnector connectorInstance, String name) {
            this.name = name;
            this.connectorInstance = connectorInstance;
        }
    }
}
