package org.leonardo.davinci.durability

import spock.lang.Specification

class DefaultBucketFactorySpec extends Specification {
    def "makeBucket; success"() {
        given:
        def subject = new DefaultBucketFactory()

        when:
        def product = subject.makeBucket(Mock(KeyMaker))

        then:
        product != null
        product instanceof DefaultBucket
    }
}
