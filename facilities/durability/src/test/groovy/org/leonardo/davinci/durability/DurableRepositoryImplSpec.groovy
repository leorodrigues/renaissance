package org.leonardo.davinci.durability

import spock.lang.Specification

class DurableRepositoryImplSpec extends Specification {

    def "newBucket; ok"() {
        given:
        def newBucket = Mock(Bucket)

        and:
        def proxyBucket = Mock(Bucket)

        when:
        def result = subject.newBucket("strings", keyMaker)

        then:
        result != null
        result.is(proxyBucket)
        bucketFactory.makeBucket(keyMaker) >> newBucket
        bucketProxyFactory.makeProxy(subject, newBucket) >> proxyBucket
    }

    def "getBucket; ok; triggers load event"() {
        given:
        def existingBucket = Mock(Bucket)

        and:
        def proxyBucket = Mock(Bucket)

        when:
        def result = subject.getBucket("strings")

        then:
        result != null
        result.is(proxyBucket)
        bucketProxyFactory.makeProxy(subject, existingBucket) >> proxyBucket
        underlyingStorage.restore() >> {
            def buckets = new HashMap<String, Bucket>()
            buckets.put("strings", existingBucket)
            return buckets
        }
    }

    def "getBucket; ok; second call does not trigger load event"() {
        given:
        def stringsBucket = Mock(Bucket)
        def messageBucket = Mock(Bucket)

        and:
        def proxyBucket = Mock(Bucket)

        when:
        subject.getBucket("messages")
        def result = subject.getBucket("strings")

        then:
        result != null
        result.is(proxyBucket)
        bucketProxyFactory.makeProxy(subject, messageBucket) >> Mock(Bucket)
        bucketProxyFactory.makeProxy(subject, stringsBucket) >> proxyBucket
        underlyingStorage.restore() >> {
            def buckets = new HashMap<String, Bucket>()
            buckets.put("messages", messageBucket)
            buckets.put("strings", stringsBucket)
            return buckets
        }
    }

    def "getBucket; error; bucket does not exist"() {
        when:
        subject.getBucket("strings")

        then:
        thrown(BucketNotFoundException)
        underlyingStorage.restore() >> new HashMap<String, Bucket>()
    }

    def "save; ok; only calls the underlying storage"() {
        when:
        subject.save()

        then:
        underlyingStorage.persist(_) >> { }
    }

    def "load; success; underlying system works just fine"() {
        when:
        subject.load()

        then:
        underlyingStorage.restore() >> new HashMap<String, Bucket>()
    }

    def "load; failure; underlying system throws exception"() {
        when:
        subject.load()

        then:
        thrown(BucketLoadException)
        underlyingStorage.restore() >> {
            throw new Exception("thrown on purpose")
        }
    }

    def "discard; ok; ask the underlying storage to dispose of the data"() {
        when:
        subject.discard()

        then:
        underlyingStorage.dispose() >> { }
    }

    def bucketProxyFactory
    def underlyingStorage
    def bucketFactory
    def subject
    def keyMaker

    void setup() {
        bucketProxyFactory = Mock(BucketProxyFactory)
        keyMaker = Mock(KeyMaker)
        bucketFactory = Mock(BucketFactory)
        underlyingStorage = Mock(UnderlyingStorage)

        subject = new DurableRepositoryImpl(
            underlyingStorage, bucketFactory, bucketProxyFactory)
    }
}
