package org.leonardo.davinci.durability

import org.lenardo.renaissance.collectionworks.Predicate
import spock.lang.Specification

/**
 * Created by leonardo on 02/10/2016.
 */
class DefaultBucketSpec extends Specification {
    def "remove; ok; bucket is empty"() {
        given:
        def subject = new DefaultBucket(Mock(KeyMaker))

        and:
        def message = new Message("y")

        when:
        def success = subject.remove(message)

        then:
        !success
    }

    def "remove; ok; bucket has an object, object is not found"() {
        given:
        def subject = new DefaultBucket(Mock(KeyMaker) {
            nextKey() >> "x"
            getKeyClass() >> String.class
        })

        and:
        subject.make(Message.class, "some text")

        and:
        def anotherMessage = new Message("y")

        when:
        def success = subject.remove(anotherMessage)

        then:
        !success
    }

    def "remove; ok; bucket has an object, object is found"() {
        given:
        def subject = new DefaultBucket(Mock(KeyMaker) {
            nextKey() >> "x"
            getKeyClass() >> String.class
        })

        and:
        def message = subject.make(Message.class, "some text")

        when:
        def success = subject.remove(message)

        then:
        success
    }

    def "all; ok; bucket is empty"() {
        given:
        def subject = new DefaultBucket(Mock(KeyMaker))

        when:
        def result = subject.all()

        then:
        !result.present
        result.get().size() == 0
    }

    def "all; ok; bucket has objects"() {
        given:
        def keys = ['a', 'b']
        def keyMaker = Mock(KeyMaker) {
            nextKey() >> keys.pop()
            getKeyClass() >> String.class
        }

        and:
        def subject = new DefaultBucket(keyMaker)

        and:
        subject.make(Message.class, "m1")
        subject.make(Message.class, "m2")

        when:
        def result = subject.all()

        then:
        result.present
        result.get().size() == 2
    }

    def "applyQuery; ok; on empty bucket"() {
        given:
        def keyMaker = Mock(KeyMaker)

        and:
        def subject = new DefaultBucket(keyMaker)

        when:
        def result = subject.applyQuery(new Predicate() {
            @Override
            boolean verifies(Object argument) {
                return false
            }
        })

        then:
        !result.present
    }

    def "applyQuery; ok; with objects on bucket, but no match"() {
        given:
        def keyMaker = Mock(KeyMaker) {
            nextKey() >> "xpto"
            getKeyClass() >> String.class
        }

        and:
        def subject = new DefaultBucket(keyMaker)

        and:
        subject.make(Message.class, "text")

        when:
        def result = subject.applyQuery(new Predicate() {
            @Override
            boolean verifies(Object argument) {
                return false
            }
        })

        then:
        !result.present
    }

    def "applyQuery; ok; with objects on bucket, and a match"() {
        given:
        def keyMaker = Mock(KeyMaker) {
            nextKey() >> "xpto"
            getKeyClass() >> String.class
        }

        and:
        def subject = new DefaultBucket(keyMaker)

        and:
        def message = subject.make(Message.class, "text")

        when:
        def result = subject.applyQuery(new Predicate<Message>() {
            @Override
            boolean verifies(Message argument) {
                return argument.message.equals("text")
            }
        })

        then:
        result.present
        result.get().is(message)
    }

    def "make; ok; with constructor arguments"() {
        given:
        def keyMaker = Mock(KeyMaker)

        and:
        def subject = new DefaultBucket(keyMaker)

        when:
        def message = subject.make(Message.class, "text")

        then:
        message.id == "xpto"
        message.message == "text"
        1 * keyMaker.nextKey() >> "xpto"
        1 * keyMaker.getKeyClass() >> String.class
        1 * keyMaker.commit()
    }

    def "make; ok; without constructor arguments"() {
        given:
        def keyMaker = Mock(KeyMaker)

        and:
        def subject = new DefaultBucket(keyMaker)

        when:
        def message = subject.make(Message.class)

        then:
        message.id == "abcd"
        message.message == "no message given"
        1 * keyMaker.nextKey() >> "abcd"
        1 * keyMaker.getKeyClass() >> String.class
        1 * keyMaker.commit()
    }

    def "make; error; keyMaker is unsuitable for class"() {
        given:
        def keyMaker = Mock(KeyMaker)

        and:
        def subject = new DefaultBucket(keyMaker)

        when:
        subject.make(Message.class)

        then:
        thrown(NoSuitableConstructorFoundException)
        1 * keyMaker.nextKey() >> { }
        1 * keyMaker.getKeyClass() >> Integer.class
        1 * keyMaker.rollback()
    }

    static class Message {
        String id
        String message

        Message(String id) {
            this.id = id
            this.message = "no message given"
        }

        Message(String id, String message) {
            this.message = message
            this.id = id
        }
    }
}
