package org.leonardo.davinci.durability

import spock.lang.Specification

import static java.nio.file.Files.createTempFile

class FileSystemRepositoryKitSpec extends Specification {

    def "single-test; make all products"() {
        when:
        def us = subject.makeUnderlyingStorage()
        def re = subject.makeRepository(us)

        then:
        us instanceof FileSystemUnderlyingStorage
        re instanceof DurableRepositoryImpl
    }

    def subject

    void setup() {
        def tempFile = createTempFile("test-run-", ".tmp").toFile()
        subject = new FileSystemRepositoryKit(tempFile)
    }
}
