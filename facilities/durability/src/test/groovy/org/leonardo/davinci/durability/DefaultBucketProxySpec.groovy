package org.leonardo.davinci.durability

import org.lenardo.renaissance.collectionworks.Predicate
import spock.lang.Specification

class DefaultBucketProxySpec extends Specification {
    def "applyQuery; ok; only calls delegate"() {
        given:
        def verification = Spy(Predicate)

        when:
        subject.applyQuery(verification)

        then:
        delegate.applyQuery(verification) >> { }
    }

    def "all; ok; only call delegate"() {
        when:
        subject.all()

        then:
        delegate.all() >> { }
    }

    def "make; ok; just call delegate"() {
        when:
        subject.make(Object.class, "a", "b")

        then:
        delegate.make(Object.class, "a", "b") >> { }
        repository.save() >> { }
    }

    def "remove; ok; just call delegate"() {
        given:
        def o = new Object()

        when:
        def success = subject.remove(o)

        then:
        success
        delegate.remove(o) >> true
    }

    void setup() {
        repository = Mock(DurableRepository)
        delegate = Mock(Bucket)
        subject = new DefaultBucketProxy(repository, delegate)
    }

    def repository
    def delegate
    def subject
}
