package org.leonardo.davinci.durability

import spock.lang.Specification

import static java.io.File.createTempFile

class FileSystemUnderlyingStorageSpec extends Specification {
    def "Complete run"() {
        given:
        def file = createTempFile("test-run-", ".ser")

        and:
        def subject = new FileSystemUnderlyingStorage(
                file, { c -> c.close() })

        when:
        subject.persist(new HashMap<String, Bucket>())
        subject.restore()
        subject.dispose()

        then:
        !file.exists()
    }
}
