package org.leonardo.testing.learning;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by leonardo on 29/10/2016.
 */
public class ObjectSerializationTests {
    @Test
    public void testSaveParentChild() throws Exception {
        ByteArrayOutputStream outputStream =
                new ByteArrayOutputStream(4096);

        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(outputStream);

        Child child = new Child("message A", "message B");

        objectOutputStream.writeObject(child);
        objectOutputStream.close();

        ByteArrayInputStream inputStream =
                new ByteArrayInputStream(outputStream.toByteArray());

        ObjectInputStream objectInputStream =
                new ObjectInputStream(inputStream);

        Child newChild = (Child)objectInputStream.readObject();

        assertEquals("message A", newChild.getParentMessage());
        assertEquals("message B", newChild.getChildMessage());
    }

    public static class Parent implements Serializable {
        private String parentMessage = "no message given";

        public Parent(String parentMessage) {
            this.parentMessage = parentMessage;
        }

        public String getParentMessage() {
            return parentMessage;
        }
    }

    public static class Child extends Parent {
        private String childMessage = "no message given";

        public Child(String parentMessage, String childMessage) {
            super(parentMessage);
            this.childMessage = childMessage;
        }

        public String getChildMessage() {
            return childMessage;
        }
    }
}
