package org.leonardo.davinci.durability;

public interface BucketProxyFactory {
    Bucket makeProxy(DurableRepository durableRepository, Bucket delegate);
}
