package org.leonardo.davinci.durability;

public class DefaultBucketFactory implements BucketFactory {
    @Override
    public Bucket makeBucket(KeyMaker keyMaker) {
        return new DefaultBucket(keyMaker);
    }
}
