package org.leonardo.davinci.durability;

public interface BucketFactory {
    Bucket makeBucket(KeyMaker keyMaker);
}
