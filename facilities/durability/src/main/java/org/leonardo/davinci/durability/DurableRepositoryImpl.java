package org.leonardo.davinci.durability;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by leonardo on 02/10/2016.
 */
public class DurableRepositoryImpl implements DurableRepository {

    private Map<String, Bucket> bucketProxies = new HashMap<>();
    private Map<String, Bucket> bucketInstances = new HashMap<>();
    private final BucketFactory bucketFactory;
    private final UnderlyingStorage underlyingStorage;
    private final BucketProxyFactory bucketProxyFactory;

    public DurableRepositoryImpl(
            UnderlyingStorage underlyingStorage,
            BucketFactory bucketFactory,
            BucketProxyFactory bucketProxyFactory) {

        this.underlyingStorage = underlyingStorage;
        this.bucketFactory = bucketFactory;
        this.bucketProxyFactory = bucketProxyFactory;
    }

    @Override
    public Bucket newBucket(String bucketName, KeyMaker keyMaker) {
        Bucket delegate = bucketFactory.makeBucket(keyMaker);
        bucketInstances.put(bucketName, delegate);

        Bucket proxy = makeProxy(delegate);
        bucketProxies.put(bucketName, proxy);
        return proxy;
    }

    @Override
    public Bucket getBucket(String bucketName) {
        if (bucketProxies.isEmpty())
            load();
        Bucket result = bucketProxies.get(bucketName);
        if (result == null)
            throw new BucketNotFoundException(bucketName);
        return result;
    }

    @Override
    public void save() throws IOException {
        underlyingStorage.persist(bucketInstances);
    }

    @Override
    public void load() throws BucketLoadException {
        try {
            clearInternalReferences();
            bucketInstances = underlyingStorage.restore();
            rebuildProxies();
        } catch (Exception e) {
            throw new BucketLoadException(e);
        }
    }

    @Override
    public void discard() {
        underlyingStorage.dispose();
        clearInternalReferences();
    }

    private void rebuildProxies() {
        for (Entry<String, Bucket> entry : bucketInstances.entrySet())
            bucketProxies.put(entry.getKey(), makeProxy(entry.getValue()));
    }

    private void clearInternalReferences() {
        bucketProxies.clear();
        bucketInstances.clear();
    }

    private Bucket makeProxy(Bucket delegate) {
        return bucketProxyFactory.makeProxy(this, delegate);
    }
}
