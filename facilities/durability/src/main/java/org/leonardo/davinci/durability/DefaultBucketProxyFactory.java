package org.leonardo.davinci.durability;

public class DefaultBucketProxyFactory implements BucketProxyFactory {
    @Override
    public Bucket makeProxy(
            DurableRepository durableRepository,
            Bucket delegate) {
        return new DefaultBucketProxy(durableRepository, delegate);
    }
}
