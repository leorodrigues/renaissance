package org.leonardo.davinci.durability;

import java.io.IOException;
import java.util.Map;

public interface UnderlyingStorage {
    void persist(Map<String, Bucket> bucketInstances) throws IOException;
    Map<String, Bucket> restore() throws IOException, ClassNotFoundException;
    void dispose();
}
