package org.leonardo.davinci.durability;

/**
 * Created by leonardo on 02/10/2016.
 */
public interface KeyMaker {
    Object nextKey();
    Class<?> getKeyClass();
    void commit();
    void rollback();
}
