package org.leonardo.davinci.durability;

import org.leonardo.renaissance.io.TerminateCloseable;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FileSystemUnderlyingStorage implements UnderlyingStorage {

    private final File storageFile;
    private final TerminateCloseable terminateCloseable;

    public FileSystemUnderlyingStorage(
            File storageFile,
            TerminateCloseable terminateCloseable) {
        this.storageFile = storageFile;
        this.terminateCloseable = terminateCloseable;
    }

    @Override
    public void persist(Map<String, Bucket> bucketInstances)
            throws IOException {
        FileOutputStream fileStream;
        ObjectOutputStream objectStream = null;
        try {
            fileStream = new FileOutputStream(storageFile);
            objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(bucketInstances);
        } finally {
            terminateCloseable.invoke(objectStream);
        }
    }

    @Override
    public HashMap<String, Bucket> restore()
            throws IOException, ClassNotFoundException {

        FileInputStream fileStream = null;
        Object content;
        try {
            fileStream = new FileInputStream(storageFile);
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            content = objectStream.readObject();
            return (HashMap<String, Bucket>)content;
        } finally {
            terminateCloseable.invoke(fileStream);
        }
    }

    @Override
    public void dispose() {
        if (storageFile.exists() && storageFile.canWrite()) {
            storageFile.delete();
        }
    }
}
