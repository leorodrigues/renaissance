package org.leonardo.davinci.durability;

public interface RepositoryKit {
    DurableRepository makeRepository(UnderlyingStorage underlyingStorage);
    UnderlyingStorage makeUnderlyingStorage();
}
