package org.leonardo.davinci.durability;

import static java.lang.String.format;

/**
 * Created by Lrteixeira on 08/11/2016.
 */
public class BucketNotFoundException extends RuntimeException {

    public static final String BUCKET_NOT_FOUND_MESSAGE =
        "There is no bucket named '%s'.";

    public BucketNotFoundException(String bucketName) {
        super(format(BUCKET_NOT_FOUND_MESSAGE, bucketName));
    }
}
