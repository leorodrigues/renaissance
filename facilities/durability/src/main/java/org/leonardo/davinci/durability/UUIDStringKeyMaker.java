package org.leonardo.davinci.durability;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by leonardo on 02/10/2016.
 */
public class UUIDStringKeyMaker implements KeyMaker, Serializable {
    public UUIDStringKeyMaker() {
    }

    @Override
    public Object nextKey() {
        return UUID.randomUUID().toString();
    }

    @Override
    public Class<?> getKeyClass() {
        return String.class;
    }

    @Override
    public void commit() {

    }

    @Override
    public void rollback() {

    }
}
