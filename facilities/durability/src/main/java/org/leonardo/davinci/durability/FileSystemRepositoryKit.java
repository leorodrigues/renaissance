package org.leonardo.davinci.durability;

import org.leonardo.renaissance.io.TryClosingOrFail;

import java.io.File;

public class FileSystemRepositoryKit implements RepositoryKit {

    private File repositoryFile;

    public FileSystemRepositoryKit(File repositoryFile) {
        this.repositoryFile = repositoryFile;
    }

    @Override
    public DurableRepository makeRepository(
            UnderlyingStorage underlyingStorage) {
        return new DurableRepositoryImpl(underlyingStorage,
                new DefaultBucketFactory(),
                new DefaultBucketProxyFactory());
    }

    @Override
    public UnderlyingStorage makeUnderlyingStorage() {
        return new FileSystemUnderlyingStorage(
                repositoryFile, TryClosingOrFail.INSTANCE);
    }
}
