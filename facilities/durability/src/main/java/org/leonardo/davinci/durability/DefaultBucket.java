package org.leonardo.davinci.durability;

import org.lenardo.renaissance.collectionworks.Predicate;
import org.leonardo.renaissance.utils.Optional;
import org.leonardo.renaissance.utils.OptionalList;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by leonardo on 02/10/2016.
 */
public class DefaultBucket implements Bucket, Serializable {

    private LinkedList objects = new LinkedList();

    private KeyMaker keyMaker;

    public DefaultBucket(KeyMaker keyMaker) {
        this.keyMaker = keyMaker;
    }

    @Override
    public <T> Optional<T> applyQuery(Predicate<T> query) {
        for (Object o : objects)
            if (query.verifies((T)o))
                return new Optional<>((T)o);
        return new Optional<>(null);
    }

    @Override
    public <T> OptionalList<T> all() {
        return new OptionalList<>((List<T>) objects.clone());
    }

    @Override
    public <T> T make(Class<T> theClass, Object... constructorArguments)
            throws IllegalAccessException, InvocationTargetException,
            InstantiationException {

        try {

            Object[] arguments = new Object[constructorArguments.length + 1];
            Class<?>[] types = new Class<?>[constructorArguments.length + 1];

            arguments[0] = keyMaker.nextKey();
            types[0] = keyMaker.getKeyClass();

            for (int i = 0; i < constructorArguments.length; i++) {
                arguments[i + 1] = constructorArguments[i];
                types[i + 1] = constructorArguments[i].getClass();
            }

            Constructor<T> constructor = findConstructor(theClass, types);
            T result = constructor.newInstance(arguments);
            objects.add(result);
            keyMaker.commit();

            return result;
        } catch (Throwable e) {
            keyMaker.rollback();
            throw e;
        }
    }

    private <T> Constructor<T> findConstructor(
            Class<T> theClass, Class<?>[] types) {

        for (Constructor<?> c : theClass.getConstructors())
            if (isConstructorMatching(c, types))
                return (Constructor<T>) c;

        throw new NoSuitableConstructorFoundException(theClass);
    }

    private boolean isConstructorMatching(
            Constructor<?> constructor, Class<?>[] types) {

        Class<?>[] parameterTypes = constructor.getParameterTypes();

        if (parameterTypes.length != types.length)
            return false;

        for (int i = 0; i < parameterTypes.length; i++)
            if (!parameterTypes[i].isAssignableFrom(types[i]))
                return false;

        return true;
    }

    @Override
    public boolean remove(Object object) {
        return objects.remove(object);
    }
}
