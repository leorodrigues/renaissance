package org.leonardo.davinci.durability;

import org.lenardo.renaissance.collectionworks.Predicate;
import org.leonardo.renaissance.utils.Optional;
import org.leonardo.renaissance.utils.OptionalList;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class DefaultBucketProxy implements Bucket {

    private DurableRepository durableRepository;
    private final Bucket delegate;

    public DefaultBucketProxy(
            DurableRepository durableRepository, Bucket delegate) {

        this.durableRepository = durableRepository;
        this.delegate = delegate;
    }

    @Override
    public <T> Optional<T> applyQuery(Predicate<T> query) {
        return delegate.applyQuery(query);
    }

    @Override
    public <T> OptionalList<T> all() {
        return delegate.all();
    }

    @Override
    public <T> T make(Class<T> theClass, Object... constructorArguments)
            throws IllegalAccessException, InstantiationException,
            InvocationTargetException {
        T result = delegate.make(theClass, constructorArguments);
        firePersistenceEvent();
        return result;
    }

    @Override
    public boolean remove(Object object) {
        return delegate.remove(object);
    }

    private void firePersistenceEvent() {
        try {
            durableRepository.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
