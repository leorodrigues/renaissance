package org.leonardo.davinci.durability;

import java.io.IOException;

public interface DurableRepository {
    Bucket newBucket(String bucketName, KeyMaker keyMaker);

    Bucket getBucket(String bucketName);

    void save() throws IOException;

    void load() throws BucketLoadException;

    void discard();
}
