package org.leonardo.davinci.durability;

import static java.lang.String.format;

public class NoSuitableConstructorFoundException extends RuntimeException {
    public NoSuitableConstructorFoundException(Class<?> subjectClass) {
        super(makeMessage(subjectClass));
    }

    private static String makeMessage(Class<?> subjectClass) {
        return format("Could not find a suitable constructor for class '%s'.",
            subjectClass.getCanonicalName());
    }
}
