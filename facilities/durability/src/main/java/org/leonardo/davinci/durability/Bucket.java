package org.leonardo.davinci.durability;

import org.lenardo.renaissance.collectionworks.Predicate;
import org.leonardo.renaissance.utils.Optional;
import org.leonardo.renaissance.utils.OptionalList;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by leonardo on 02/10/2016.
 */
public interface Bucket {
    <T> Optional<T> applyQuery(Predicate<T> query);

    <T> OptionalList<T> all();

    <T> T make(Class<T> theClass, Object... constructorArguments)
            throws IllegalAccessException, InvocationTargetException,
                InstantiationException;

    boolean remove(Object object);
}
