package org.leonardo.davinci.durability;

import static java.lang.String.format;

/**
 * Created by leonardo on 29/10/2016.
 */
public class BucketLoadException extends RuntimeException {

    private static final String DEFAULT_MESSAGE =
        "Error loading buckets: %s";

    public BucketLoadException(Throwable cause) {
        super(format(DEFAULT_MESSAGE, cause.getMessage()), cause);
    }
}
