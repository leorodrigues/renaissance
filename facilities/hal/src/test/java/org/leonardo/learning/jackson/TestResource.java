package org.leonardo.learning.jackson;

import org.leonardo.davinci.hal.HalResource;

/**
 * Created by Lrteixeira on 10/03/2017.
 */
public class TestResource<T> extends HalResource<T> {

    private String someProperty;

    public TestResource(String someProperty) {
        super();
        this.someProperty = someProperty;
    }

    public String getSomeProperty() {
        return someProperty;
    }
}
