package org.leonardo.learning.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import org.leonardo.davinci.hal.HalError
import org.leonardo.davinci.hal.HalErrors
import org.leonardo.davinci.hal.HalLink
import org.leonardo.davinci.hal.HalResource
import spock.lang.Specification

import static HalErrors.toArrayOfErrors

/**
 * Created by Lrteixeira on 09/03/2017.
 */
class SerializationSpec extends Specification {

    def mapper = new ObjectMapper()

    def "Serializing an error"() {
        given:
        def subject = new HalError("fake error instance created by unit test")

        when:
        def result = mapper.writeValueAsString(subject)

        then:
        result == "{\"message\":\"fake error instance created by unit test\"}"
    }

    def "Serializing list of errors"() {
        given:
        def subject = new HalErrors("unit test", toArrayOfErrors("fake error"))

        when:
        def result = mapper.writeValueAsString(subject)

        then:
        result == "{\"subject\":\"unit test\",\"errors\":[{\"message\":\"fake error\"}]}"
    }

    def "Serializing resource without link"() {
        given:
        def subject = new TestResource("x")

        when:
        def result = mapper.writeValueAsString(subject)

        then:
        result == "{\"someProperty\":\"x\"}"
    }

    def "Serializing resource with 'self' link"() {
        given:
        def subject = new TestResource("x")
        subject.addSelf(new HalLink("http://xyz.com/some/path"))

        when:
        def result = mapper.writeValueAsString(subject)

        then:
        result == "{\"someProperty\":\"x\",\"_links\":{\"self\":{\"href\":\"http://xyz.com/some/path\"}}}"
    }

    def "Serializing resource with 'embedded' resource"() {
        given:
        def subject = new HalResource<TestResource>(new TestResource("y"))

        when:
        def result = mapper.writeValueAsString(subject)

        then:
        result == "{\"_embedded\":{\"someProperty\":\"y\"}}"
    }
}
