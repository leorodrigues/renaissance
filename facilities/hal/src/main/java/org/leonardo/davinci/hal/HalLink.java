package org.leonardo.davinci.hal;

/**
 * Created by Lrteixeira on 10/03/2017.
 */
public class HalLink {

    private String href;

    public HalLink(String href) {
        this.href = href;
    }

    public String getHref() {
        return href;
    }
}
