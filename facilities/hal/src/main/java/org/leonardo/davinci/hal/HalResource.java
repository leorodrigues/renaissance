package org.leonardo.davinci.hal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

/**
 * Created by Lrteixeira on 10/03/2017.
 */
@JsonInclude(NON_EMPTY)
public class HalResource<T> {

    @JsonProperty("_links")
    private final Map<String, HalLink> links;

    @JsonProperty("_embedded")
    private final T embedded;

    public HalResource(T embedded) {
        this.links = new HashMap<>();
        this.embedded = embedded;
    }

    public HalResource() {
        this.links = new HashMap<>();
        this.embedded = null;
    }

    public Map<String, HalLink> getLinks() {
        return links;
    }

    public void addSelf(HalLink link) {
        this.links.put("self", link);
    }
}
