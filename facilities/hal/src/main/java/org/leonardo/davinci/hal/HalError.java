package org.leonardo.davinci.hal;

/**
 * Created by Lrteixeira on 09/03/2017.
 */
public class HalError {

    private String message;

    public HalError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
