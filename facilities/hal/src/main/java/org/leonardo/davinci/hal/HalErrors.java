package org.leonardo.davinci.hal;

/**
 * Created by Lrteixeira on 07/03/2017.
 */
public class HalErrors {

    private String subject;
    private HalError[] errors;

    public HalErrors(String subject, HalError... errors) {
        this.subject = subject;
        this.errors = errors;
    }

    public String getSubject() {
        return subject;
    }

    public HalError[] getErrors() {
        return errors;
    }

    public static HalError[] toArrayOfErrors(String... details) {
        HalError[] result = new HalError[details.length];
        for (int i = 0; i < result.length; i++)
            result[i] = new HalError(details[i]);
        return result;
    }
}
