package org.leonardo.testing.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by leonardo on 29/11/2015.
 */
public class EmulationControllerTests {

    private static final String PROXY_STRING_REPRESENTATION =
            "[\\w$]+(\\.[\\w$]+)*@[a-z0-9]+";

    @Test
    public void testImplementationHasDefaultStringRepresentation() {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        assertTrue(subjectImpl.toString().matches(PROXY_STRING_REPRESENTATION));
    }

    @Test
    public void testImplementationHasHashcode() throws Exception {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        assertNotEquals(0, subjectImpl.hashCode());
    }

    @Test
    public void testImplementationIsEqualToItself() throws Exception {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        assertTrue(subjectImpl.equals(subjectImpl));
    }

    @Test
    public void testImplementationIsDifferentFromNull() throws Exception {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        assertFalse(subjectImpl.equals(null));
    }

    @Test
    public void testImplementationIsDifferentFromSomethingElse() {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        assertFalse(subjectImpl.equals(new Object()));
    }

    @Test
    public void testCountOneInvocation() throws Exception {
        EmulationController emulationController = new EmulationController();

        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);

        subjectImpl.someOperation();

        int count = emulationController.countInvocations(
                subjectImpl, "someOperation");

        assertEquals(1, count);
    }

    @Test
    public void testCountZeroInvocations() throws Exception {
        EmulationController emulationController = new EmulationController();

        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);

        int count = emulationController.countInvocations(
                subjectImpl, "someOperation");

        assertEquals(0, count);
    }

    @Test
    public void testReturnSomeValue() throws Exception {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        emulationController.expect(subjectImpl, "returnSomeInteger", 17);
        assertEquals(17, subjectImpl.returnSomeInteger());
    }

    @Test
    public void testRunAnEmulation() throws Exception {
        EmulationController emulationController = new EmulationController();
        LongSubjectInterface subjectImpl =
                emulationController.emulate(LongSubjectInterface.class);
        emulationController.expect(
                subjectImpl, "returnSomeLong", primeEmulation);
        assertEquals(11235813, subjectImpl.returnSomeLong());
    }

    @Test(expected = RuntimeException.class)
    public void testThrowsAnException() throws Exception {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        emulationController.expect(
                subjectImpl,
                "someOperation",
                new RuntimeException("thrown on purpose by an unity test"));
        subjectImpl.someOperation();
    }

    @Test
    public void testClearEmulations() throws Exception {
        EmulationController emulationController = new EmulationController();

        LongSubjectInterface subjectImpl =
                emulationController.emulate(LongSubjectInterface.class);

        emulationController.expect(
                subjectImpl, "returnSomeLong", primeEmulation);

        assertEquals(11235813, subjectImpl.returnSomeLong());
        emulationController.clearExpectations();
        assertEquals(0, subjectImpl.returnSomeLong());
    }

    @Test
    public void testClearReturns() throws Exception {
        EmulationController emulationController = new EmulationController();

        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);

        emulationController.expect(subjectImpl, "returnSomeInteger", 17);

        assertEquals(17, subjectImpl.returnSomeInteger());
        emulationController.clearExpectations();
        assertEquals(0, subjectImpl.returnSomeInteger());
    }

    @Test
    public void testReturnFalseForBooleanByDefault() throws Exception {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        assertFalse(subjectImpl.returnSomeBoolean());
        assertFalse(subjectImpl.returnSomeWrappedBoolean());
    }

    @Test
    public void testReturnsNullForArbitraryTypeByDefault() {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        assertEquals(null, subjectImpl.returnSomeClass());
    }

    @Test
    public void testReturnZeroForAnyNumberByDefault() throws Exception {
        EmulationController emulationController = new EmulationController();
        ManyTypesSubjectInterface subjectImpl =
                emulationController.emulate(ManyTypesSubjectInterface.class);
        byte b = 0;
        assertEquals(b, subjectImpl.returnSomeByte());
        assertEquals(0, subjectImpl.returnSomeShort());
        assertEquals(0, subjectImpl.returnSomeInteger());
        assertEquals(0L, subjectImpl.returnSomeLong());
        assertEquals(0f, subjectImpl.returnSomeFloat(), 0);
        assertEquals(0D, subjectImpl.returnSomeDouble(), 0);

        assertEquals(new Byte(b), subjectImpl.returnSomeWrappedByte());
        assertEquals(new Short(b), subjectImpl.returnSomeWrappedShort());
        assertEquals(new Integer(0), subjectImpl.returnSomeWrappedInteger());
        assertEquals(new Long(0), subjectImpl.returnSomeWrappedLong());
        assertEquals(new Float(0), subjectImpl.returnSomeWrappedFloat());
        assertEquals(new Double(0), subjectImpl.returnSomeWrappedDouble());
    }

    public MethodEmulation<Long> primeEmulation = new MethodEmulation<Long>() {
        @Override
        public Long emulate(String methodName, Object[] arguments) {
            return 11235813L;
        }
    };

    public interface LongSubjectInterface {
        long returnSomeLong();
    }

    public interface ManyTypesSubjectInterface {
        byte returnSomeByte();
        short returnSomeShort();
        int returnSomeInteger();
        long returnSomeLong();
        float returnSomeFloat();
        double returnSomeDouble();

        Byte returnSomeWrappedByte();
        Short returnSomeWrappedShort();
        Integer returnSomeWrappedInteger();
        Long returnSomeWrappedLong();
        Float returnSomeWrappedFloat();
        Double returnSomeWrappedDouble();

        boolean returnSomeBoolean();
        Boolean returnSomeWrappedBoolean();
        void someOperation();

        MyClass returnSomeClass();
    }

    class MyClass {

    }
}
