package org.leonardo.testing.util;

import java.util.concurrent.Semaphore;

/**
 * Created by lrteixeira on 13/11/2015.
 */
public abstract class TickerRacer extends AbstractRacer {

    private final String TEMPLATE_WAITING_MILLIS =
            "Waiting '%d' milliseconds.";

    private final String MESSAGE_TICKING_CANCELED =
            "Ticking canceled.";

    private final String MESSAGE_TICKING_COMPLETE =
            "Ticking complete.";

    private final int count;

    private final TimeSequencer timeSequencer;

    public TickerRacer(Semaphore startSignal, int count,
                       TimeSequencer timeSequencer) {
        super(startSignal);
        this.count = count;
        this.timeSequencer = timeSequencer;
    }

    @Override
    protected void execute() throws Exception {
        try {
            for (long milliseconds : timeSequencer.newSequence(count)) {
                getLog().fine(reportWaiting(milliseconds));
                Thread.sleep(milliseconds);
            }
            action();
            getLog().info(MESSAGE_TICKING_COMPLETE);
        } catch (InterruptedException e) {
            getLog().warning(MESSAGE_TICKING_CANCELED);
        }
    }

    protected abstract void action() throws Exception;

    private String reportWaiting(long milliseconds) {
        return String.format(TEMPLATE_WAITING_MILLIS, milliseconds);
    }
}
