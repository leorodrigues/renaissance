package org.leonardo.testing.util;

import java.util.concurrent.Semaphore;

/**
 * Created by leonardo on 14/11/2015.
 */
public abstract class ObserverRacer extends AbstractRacer {

    public ObserverRacer(Semaphore startSignal) {
        super(startSignal);
    }

    @Override
    protected void execute() throws Exception {
        try {
            waitForCondition();
            action();
            getLog().fine(reportActionExecuted());
        } catch (InterruptedException e) {
            getLog().warning(reportWaitInterrupted(e));
        }
    }

    protected abstract void waitForCondition() throws InterruptedException;

    protected abstract void action() throws Exception;

    protected String reportActionExecuted() {
        return "Action executed.";
    }

    protected String reportWaitInterrupted(InterruptedException e) {
        return "Waiting interrupted.";
    }

}
