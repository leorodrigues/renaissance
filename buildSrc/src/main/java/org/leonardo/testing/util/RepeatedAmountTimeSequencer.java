package org.leonardo.testing.util;

import java.util.Iterator;

/**
 * Created by leonardo on 07/11/2015.
 */
public class RepeatedAmountTimeSequencer implements TimeSequencer {

    private final long milliseconds;

    public RepeatedAmountTimeSequencer(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    @Override
    public Iterable<Long> newSequence(final int numberOfElements) {
        return new Iterable<Long>() {
            private int position = 0;
            @Override
            public Iterator<Long> iterator() {
                return new Iterator<Long>() {
                    @Override
                    public boolean hasNext() {
                        return position < numberOfElements;
                    }

                    @Override
                    public Long next() {
                        position++;
                        return milliseconds;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }
}
