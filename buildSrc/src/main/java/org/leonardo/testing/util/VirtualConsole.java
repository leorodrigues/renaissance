package org.leonardo.testing.util;

import java.io.InputStream;
import java.io.PrintStream;

/**
 * Created by lrteixeira on 18/11/2015.
 */
public interface VirtualConsole extends Runnable {
    void setStandardOutput(PrintStream displayPrintStream);
    void setStandardError(PrintStream displayPrintStream);
    void setStandardInput(InputStream dataSource);
}
