package org.leonardo.testing.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by leonardo on 14/11/2015.
 */
public interface DataBuffer extends Closeable {
    OutputStream getDataIntake();

    InputStream getDataSource();

    String snapshot();

    String getName();

    @Override
    void close() throws IOException;
}
