package org.leonardo.testing.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by leonardo on 23/09/2015.
 */
public class Memory implements Closeable {
    private int writeCursor;
    private final CharBuffer theBuffer;
    private final int size;
    private final ArrayList<OutputStream> outputs = new ArrayList<>();
    private final ArrayList<InputStream> inputs = new ArrayList<>();

    private final ReentrantReadWriteLock accessLock =
            new ReentrantReadWriteLock();

    public Memory(int size) {
        theBuffer = CharBuffer.allocate(size);
        this.size = size;
    }

    @Override
    public void close() {
        writeCursor = 0;
        theBuffer.clear();
        for (InputStream inputStream : inputs)
            tryClose(inputStream);
        for (OutputStream outputStream : outputs)
            tryClose(outputStream);
        inputs.clear();
        outputs.clear();
    }

    public OutputStream getNewOutput() {
        return new OutputStream() {
            @Override
            public synchronized void write(int b) throws IOException {
                try {
                    accessLock.writeLock().lock();
                    saveChars(b);
                } finally {
                    accessLock.writeLock().unlock();
                }
            }
        };
    }

    public OutputStream getNewOutput(final Memory linkedMemory) {
        return new OutputStream() {
            @Override
            public synchronized void write(int b) throws IOException {
                try {
                    accessLock.writeLock().lock();
                    saveChars(b);
                    linkedMemory.saveChars(b);
                } finally {
                    accessLock.writeLock().unlock();
                }
            }
        };
    }

    public InputStream getNewIndependentInput() {
        return new InputStream() {
            private int myCursor = 0;
            @Override
            public synchronized int read() throws IOException {
                try {
                    accessLock.readLock().lock();
                    if (myCursor + 1 > writeCursor)
                        throw new IOException("End of stream");
                    int result = theBuffer.get(myCursor);
                    myCursor += 1;
                    return result;
                } finally {
                    accessLock.readLock().unlock();
                }
            }
        };
    }

    public String snapshot() {
        try {
            accessLock.readLock().lock();
            char[] snapshot = Arrays.copyOf(theBuffer.array(), writeCursor);
            return new String(snapshot);
        } finally {
            accessLock.readLock().unlock();
        }
    }

    private void saveChars(int input) throws IOException {
        int amount = Character.charCount(input);
        if (writeCursor + amount > size)
            throw new IOException("Out of memory");
        for (char c : Character.toChars(input))
            theBuffer.append(c);
        writeCursor += amount;
    }

    private void tryClose(Closeable inputStream) {
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
