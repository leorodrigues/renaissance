package org.leonardo.testing.util;

/**
 * Created by leonardo on 07/11/2015.
 */
public interface TimeSequencer {
    long ONE_SECOND = 1000;
    long HALF_A_SECOND = 500;

    Iterable<Long> newSequence(int numberOfElements);
}
