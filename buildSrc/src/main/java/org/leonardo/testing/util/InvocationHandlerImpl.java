package org.leonardo.testing.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by leonardo on 20/01/2016.
 */
class InvocationHandlerImpl implements InvocationHandler {

    public Map<String, Integer> invocationCounter =
            new HashMap<>();

    public Map<String, ArrayList<Object>> allExpectedReturnValues =
            new HashMap<>();

    private Map<String, ArrayList<MethodEmulation>> allExpectedEmulations =
            new HashMap<>();

    private static Map<Class, Object> defaultReturnValues =
            new HashMap<>();

    private static byte zeroByte = 0;

    private static short zeroShort = 0;

    static {
        defaultReturnValues.put(byte.class, zeroByte);
        defaultReturnValues.put(short.class, zeroShort);
        defaultReturnValues.put(int.class, 0);
        defaultReturnValues.put(long.class, 0L);
        defaultReturnValues.put(float.class, 0f);
        defaultReturnValues.put(double.class, 0D);
        defaultReturnValues.put(boolean.class, false);
        defaultReturnValues.put(void.class, null);

        defaultReturnValues.put(Byte.class, zeroByte);
        defaultReturnValues.put(Short.class, zeroShort);
        defaultReturnValues.put(Integer.class, 0);
        defaultReturnValues.put(Long.class, 0L);
        defaultReturnValues.put(Float.class, 0f);
        defaultReturnValues.put(Double.class, 0D);
        defaultReturnValues.put(Boolean.class, false);
        defaultReturnValues.put(Void.class, null);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        incrementInvocations(method);
        if (!Object.class.equals(method.getDeclaringClass()))
            return tryToRunExpectedEmulation(method, args);
        String methodName = method.getName();
        if (methodName.equals("equals"))
            return proxy == args[0];
        if (methodName.equals("hashCode"))
            return System.identityHashCode(proxy);
        // We assume you called "toString"
        return makeStringRepresentation(proxy);
    }

    public void addExpectation(String methodName, Object returnValue) {
        ArrayList<Object> expectedValues =
                getExpectedReturnValuesFor(methodName);
        expectedValues.add(returnValue);
    }

    public void addExpectation(String methodName, MethodEmulation action) {
        ArrayList<MethodEmulation> expectedActions =
                getExpectedEmulationsFor(methodName);
        expectedActions.add(action);
    }

    public int countInvocations(String methodName) {
        if (!invocationCounter.containsKey(methodName)) {
            return 0;
        }
        return invocationCounter.get(methodName);
    }

    private Object tryToRunExpectedEmulation(Method method, Object[] args)
            throws Throwable {
        String methodName = method.getName();
        ArrayList<MethodEmulation> expectations =
                getExpectedEmulationsFor(methodName);
        if (expectations.size() > 0)
            return expectations.remove(0).emulate(methodName, args);
        return tryToGetExpectedReturnValue(method);
    }

    private Object tryToGetExpectedReturnValue(Method method) {
        String methodName = method.getName();
        ArrayList<Object> expectations = getExpectedReturnValuesFor(methodName);
        if (expectations.size() > 0)
            return expectations.remove(0);
        return tryToGetDefaultReturnValue(method);
    }

    private Object tryToGetDefaultReturnValue(Method method) {
        Class<?> returnType = method.getReturnType();
        if (defaultReturnValues.containsKey(returnType))
            return defaultReturnValues.get(returnType);
        return null;
    }

    private ArrayList<Object> getExpectedReturnValuesFor(
            String methodName) {
        if (allExpectedReturnValues.containsKey(methodName))
            return allExpectedReturnValues.get(methodName);
        ArrayList<Object> result = new ArrayList<>();
        allExpectedReturnValues.put(methodName, result);
        return result;
    }

    private ArrayList<MethodEmulation> getExpectedEmulationsFor(
            String methodName) {
        if (allExpectedEmulations.containsKey(methodName))
            return allExpectedEmulations.get(methodName);
        ArrayList<MethodEmulation> result = new ArrayList<>();
        allExpectedEmulations.put(methodName, result);
        return result;
    }

    private void incrementInvocations(Method method) {
        if (!invocationCounter.containsKey(method.getName())) {
            invocationCounter.put(method.getName(), 1);
            return;
        }
        int invocations = invocationCounter.get(method.getName());
        invocationCounter.put(method.getName(), invocations + 1);
    }

    private String makeStringRepresentation(Object proxy) {
        return String.format("%s@%x", proxy.getClass().getName(),
                System.identityHashCode(proxy));
    }

    public void clearExpectations() {
        for (ArrayList expectations : allExpectedReturnValues.values())
            expectations.clear();
        for (ArrayList expectations : allExpectedEmulations.values())
            expectations.clear();
        allExpectedReturnValues.clear();
        allExpectedEmulations.clear();
        invocationCounter.clear();
    }
}
