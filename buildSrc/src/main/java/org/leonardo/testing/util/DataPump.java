package org.leonardo.testing.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.util.logging.Logger;

/**
 * Created by lrteixeira on 13/11/2015.
 */
public class DataPump implements Runnable {

    private final Logger log;
    private DataBuffer frontBuffer;
    private DataBuffer[] backBufferArray;

    public DataPump(
            DataBuffer frontBuffer, DataBuffer... backBufferArray) {
        this.frontBuffer = frontBuffer;
        this.backBufferArray = backBufferArray;
        log = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public void run() {
        InputStream input = frontBuffer.getDataSource();
        try {
            int byteRead;
            while ((byteRead = input.read()) > -1)
                for (DataBuffer backBuffer : backBufferArray)
                    tryWritingToOutput(byteRead, backBuffer);
        } catch (InterruptedIOException e) {
            log.severe(makeReportPumpingInterrupted(frontBuffer.getName()));
        } catch (IOException e) {
            log.severe(makeReportFrontBufferError(frontBuffer.getName(), e));
        }
    }

    private String makeReportFrontBufferError(String name, Exception e) {
        return String.format("Cant read from front buffer: %s",
                e.getLocalizedMessage());
    }

    private String makeReportPumpingInterrupted(String name) {
        return String.format("Pumping of front buffer '%s' interrupted.", name);
    }

    private void tryWritingToOutput(int byteRead, DataBuffer backBuffer) {
        try {
            OutputStream output = backBuffer.getDataIntake();
            output.write(byteRead);
            output.flush();
        } catch (IOException e) {
            String message = makeErrorMessage(
                    byteRead, backBuffer.getName(), e.getLocalizedMessage());
            log.warning(message);
        }
    }

    private String makeErrorMessage(
            int byteRead, String bufferName, String cause) {
        return String.format("sending '%c' to buffer '%s': %s",
                byteRead, bufferName, cause);
    }
}
