package org.leonardo.testing.util;

import java.io.*;

/**
 * Created by lrteixeira on 13/11/2015.
 */
public class PipedDataBuffer implements DataBuffer {
    private PipedInputStream pipedInput;
    private PipedOutputStream pipedOutput;
    private StringBuffer snapshot;
    private String name;

    @Override
    public OutputStream getDataIntake() {
        return pipedOutput;
    }

    @Override
    public InputStream getDataSource() {
        return pipedInput;
    }

    @Override
    public String snapshot() {
        return snapshot.toString();
    }

    public PipedDataBuffer(String name) throws IOException {
        this(name, false);
    }

    public PipedDataBuffer(String name, boolean recordSnapshot)
            throws IOException {

        this.name = name;

        snapshot = new StringBuffer();

        if (recordSnapshot)
            makeRecordedOutput();
        else
            makeSimpleOutput();

        pipedInput = new PipedInputStream(pipedOutput);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void close() throws IOException {
        tryToClose(pipedOutput);
        tryToClose(pipedInput);
        snapshot.setLength(0);
    }

    private void tryToClose(Closeable closeable) {
        try { closeable.close(); } catch (Exception ignored) { }
    }

    private void makeSimpleOutput() {
        pipedOutput = new PipedOutputStream();
    }

    private void makeRecordedOutput() {
        pipedOutput = new PipedOutputStream() {
            @Override
            public void write(int b) throws IOException {
                snapshot.append((char) b);
                super.write(b);
            }

            @Override
            public void write(byte[] b, int off, int len)
                    throws IOException {
                for (int i = off; i < len; i++)
                    snapshot.append((char) b[i]);
                super.write(b, off, len);
            }
        };
    }
}
