package org.leonardo.testing.util;

/**
 * Created by leonardo on 06/12/2015.
 */
public interface MethodEmulation<T> {
    T emulate(String methodName, Object[] arguments) throws Throwable;
}
