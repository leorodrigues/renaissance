package org.leonardo.testing.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.*;

/**
 *
 * @author Leonardo Rodrigues Teixeira
 */
public class VirtualConsoleUser implements ThreadFactory {

    public static final int DEFAULT_WAIT_COUNT = 6;

    private VirtualConsole console;
    private PrintStream keyboardPrinter;
    private Scanner displayScanner;

    private DataPump keyboardDataPump;
    private DataBuffer consoleBuffer;
    private DataBuffer keyboardBuffer;
    private DataBuffer displayBuffer;

    private final String promptPattern;

    private final TimeSequencer timeSequencer;

    private final Semaphore startSignal;

    private final Semaphore countdownEndedOrPromptSeenSignal;

    private final Memory displayMemory;

    private ThreadGroup threadGroup;

    private ExecutorService executorService;

    private Future<?> keyboardDataPumpTask;

    private Future<?> consoleTask;

    public VirtualConsoleUser(
            VirtualConsole console,
            String promptPattern,
            TimeSequencer timeSequencer) throws IOException {

        this.promptPattern = promptPattern;
        this.console = console;
        this.timeSequencer = timeSequencer;

        displayMemory = new Memory(4096);
        startSignal = new Semaphore(0);
        countdownEndedOrPromptSeenSignal = new Semaphore(0);

        configureExecutorService();
        configureDataPump();
        configureBridgeCommunication();
        configureConsole();
    }

    @Override
    public Thread newThread(Runnable target) {
        return new Thread(threadGroup, target);
    }

    public void reset() throws IOException {
        displayMemory.close();
        tryToCancelTask(keyboardDataPumpTask);
        tryToCancelTask(consoleTask);
        tryToClose(keyboardPrinter);
        tryToClose(keyboardBuffer);
        tryToClose(consoleBuffer);
        tryToClose(displayBuffer);
        tryToClose(displayScanner);
        configureDataPump();
        configureBridgeCommunication();
        configureConsole();
    }

    private void tryToClose(Closeable closeable) {
        try { closeable.close(); } catch (Exception ignored) { }
    }

    public void terminate() throws IOException {
        reset();
        executorService.shutdownNow();
    }

    public boolean hasNextLineOnConsole() {
        return displayScanner.hasNextLine();
    }

    public String nextLineFromConsole() {
        return displayScanner.nextLine();
    }

    public void printlnToConsole(String line) {
        keyboardPrinter.println(line);
    }

    public String getDisplaySnapshotAfterPromptOrGiveUp()
            throws InterruptedException {
        waitForPromptOrGiveUp(DEFAULT_WAIT_COUNT);
        return displayBuffer.snapshot().replace("\r", "");
    }

    public void typeAtPromptOrGiveUp(String text)
            throws InterruptedException {
        waitForPromptOrGiveUp(DEFAULT_WAIT_COUNT);
        printlnToConsole(text);
    }

    public void typeAtPromptOrGiveUp(String text, int waitCount)
            throws InterruptedException {
        waitForPromptOrGiveUp(waitCount);
        printlnToConsole(text);
    }

    public void waitForPromptOrGiveUp() throws InterruptedException {
        waitForTextOrGiveUp(promptPattern, DEFAULT_WAIT_COUNT);
    }

    public void waitForPromptOrGiveUp(int count) throws InterruptedException {
        waitForTextOrGiveUp(promptPattern, count);
    }

    public void waitForTextOrGiveUp(String text, int count)
            throws InterruptedException {
        Future<?> observer = executorService.submit(
                new TextAwareRacer(text));
        Future<?> ticker = executorService.submit(
                new TimeoutTickerRacer(count));
        Thread.sleep(5);
        startSignal.release(2);
        countdownEndedOrPromptSeenSignal.acquire();
        tryToCancelTask(observer);
        tryToCancelTask(ticker);
    }

    private void tryToCancelTask(Future<?> task) {
        if (task.isCancelled())
            return;
        if (task.isDone())
            return;
        task.cancel(true);
    }

    private void forwardToNextTextAndConsume(String text) {
        if (displayScanner.hasNext(text))
            displayScanner.next(text);
    }

    private void configureExecutorService() {
        threadGroup = new ThreadGroup(UUID.randomUUID().toString());
        threadGroup.setDaemon(false);
        executorService = Executors.newCachedThreadPool(this);
    }

    private void configureDataPump() throws IOException {
        displayBuffer = new MemoryDataBuffer(displayMemory, "displayBuffer");
        keyboardBuffer = new PipedDataBuffer("keyboardBuffer");
        consoleBuffer = new PipedDataBuffer("consoleBuffer");
        keyboardDataPump = new DataPump(
                keyboardBuffer, displayBuffer, consoleBuffer);
        keyboardDataPumpTask = executorService.submit(keyboardDataPump);
    }

    private void configureConsole() {
        PrintStream displayPrintStream =
                new PrintStream(displayBuffer.getDataIntake(), true);
        console.setStandardOutput(displayPrintStream);
        console.setStandardError(displayPrintStream);
        console.setStandardInput(consoleBuffer.getDataSource());
        consoleTask = executorService.submit(console);
    }

    private void configureBridgeCommunication() {
        keyboardPrinter = new PrintStream(keyboardBuffer.getDataIntake(), true);
        displayScanner = new Scanner(displayBuffer.getDataSource());
    }

    private class TimeoutTickerRacer extends TickerRacer {
        public TimeoutTickerRacer(int count) {
            super(startSignal, count, timeSequencer);
        }

        @Override
        public void action() {
            countdownEndedOrPromptSeenSignal.release();
        }
    }

    private class TextAwareRacer extends ObserverRacer {
        private final String text;

        public TextAwareRacer(String text) {
            super(VirtualConsoleUser.this.startSignal);
            this.text = text;
        }

        @Override
        protected void waitForCondition() throws InterruptedException {
            forwardToNextTextAndConsume(text);
            getLog().warning("Found text: " + text);
        }

        @Override
        protected void action() throws Exception {
            countdownEndedOrPromptSeenSignal.release();
        }
    }
}
