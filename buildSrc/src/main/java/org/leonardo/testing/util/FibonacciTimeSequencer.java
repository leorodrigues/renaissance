package org.leonardo.testing.util;

import java.util.Iterator;

/**
 * Created by leonardo on 07/11/2015.
 */
public class FibonacciTimeSequencer implements TimeSequencer {

    private static final int[] sequence = {
            1, 1, 2, 3, 5, 8, 13, 21, 34
    };

    private long scaleInMilliseconds;

    private long length;

    public FibonacciTimeSequencer(long scaleInMilliseconds, long length) {
        this.scaleInMilliseconds = scaleInMilliseconds;
        this.length = length <= sequence.length ? length : sequence.length;
    }

    @Override
    public Iterable<Long> newSequence(int numberOfElements) {
        final long end = numberOfElements <= length ? numberOfElements : length;
        return new Iterable<Long>() {
            private int position = 0;
            @Override
            public Iterator<Long> iterator() {
                return new Iterator<Long>() {
                    @Override
                    public boolean hasNext() {
                        return position < end;
                    }

                    @Override
                    public Long next() {
                        return sequence[position++] * scaleInMilliseconds;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }
}
