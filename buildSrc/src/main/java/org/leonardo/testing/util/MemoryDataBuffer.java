package org.leonardo.testing.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by leonardo on 14/11/2015.
 */
public class MemoryDataBuffer implements DataBuffer {

    private Memory delegate;
    private String name;

    public MemoryDataBuffer(Memory delegate, String name) {
        this.delegate = delegate;
        this.name = name;
    }

    @Override
    public OutputStream getDataIntake() {
        return delegate.getNewOutput();
    }

    @Override
    public InputStream getDataSource() {
        return delegate.getNewIndependentInput();
    }

    @Override
    public String snapshot() {
        return delegate.snapshot();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }
}
