package org.leonardo.testing.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by leonardo on 14/11/2015.
 */
@Retention(RUNTIME)
@Target(ElementType.FIELD)
public @interface FixtureResource {
}
