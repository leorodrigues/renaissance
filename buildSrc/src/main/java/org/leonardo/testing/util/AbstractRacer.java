package org.leonardo.testing.util;

import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

/**
 * Created by leonardo on 14/11/2015.
 */
public abstract class AbstractRacer implements Runnable {
    private final Logger log;
    private Semaphore startSignal;

    protected Logger getLog() {
        return log;
    }

    public AbstractRacer(Semaphore startSignal) {
        log = Logger.getLogger(this.getClass().getName());
        this.startSignal = startSignal;
    }

    @Override
    public void run() {
        try {
            startSignal.acquire();
            execute();
        } catch (InterruptedException e) {
            log.warning(reportRemovedFromRace(e));
        } catch (Exception e) {
            log.severe(reportUnexpectedError(e));
        }
    }

    protected abstract void execute() throws Exception;

    protected String reportRemovedFromRace(InterruptedException e) {
        return "Removed from race";
    }

    private String reportUnexpectedError(Exception e) {
        return String.format("Unexpected error: %s", e.getLocalizedMessage());
    }
}
