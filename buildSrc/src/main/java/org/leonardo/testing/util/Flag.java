package org.leonardo.testing.util;

/**
 * Created by Lrteixeira set 14/10/2015.
 */
public class Flag {
    private boolean set;
    private boolean initial;

    public Flag() {

    }

    public Flag(boolean set) {
        this.set = set;
        this.initial = set;
    }

    public void reset() {
        set = initial;
    }

    public boolean signal() {
        return set = !initial;
    }

    public boolean isSet() {
        return set;
    }
}
