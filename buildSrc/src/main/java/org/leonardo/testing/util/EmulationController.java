package org.leonardo.testing.util;

import java.lang.reflect.Proxy;
import java.util.*;

/**
 * Created by leonardo on 29/11/2015.
 */
public class EmulationController {

    private Map<Object, InvocationHandlerImpl> invocationHandlers =
            new HashMap<>();

    @SuppressWarnings("unchecked")
    public <T> T emulate(Class<T> targetClass) {
        InvocationHandlerImpl handler = new InvocationHandlerImpl();
        ClassLoader classLoader = targetClass.getClassLoader();
        Class[] classes = {targetClass};
        T instance = (T)Proxy.newProxyInstance(classLoader, classes, handler);
        invocationHandlers.put(instance, handler);
        return instance;
    }

    public void expect(Object mock, String methodName, Object returnValue) {
        invocationHandlers.get(mock).addExpectation(methodName, returnValue);
    }

    public void expect(Object mock, String methodName, MethodEmulation action) {
        invocationHandlers.get(mock).addExpectation(methodName, action);
    }

    public void expect(Object mock, String methodName, final Throwable error) {
        MethodEmulation invocation = new MethodEmulation() {
            @Override
            public Object emulate(String methodName, Object[] arguments)
                    throws Throwable {
                throw error;
            }
        };

        invocationHandlers.get(mock).addExpectation(methodName, invocation);
    }

    public int countInvocations(Object mock, String methodName) {
        return invocationHandlers.get(mock).countInvocations(methodName);
    }

    public void clearExpectations() {
        for (InvocationHandlerImpl handler : invocationHandlers.values())
            handler.clearExpectations();
    }
}
