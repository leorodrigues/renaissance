package org.leonardo.testing.util;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by leonardo on 30/08/2015.
 */
public class FixtureResourcesHelper {

    private static String XPATH_TEMPLATE_TEXT_RESOURCE =
            "/resources/class[@name='%s']/resource[@id='%s']/text/text()";

    private static final int DATA_BUFFER_SIZE = 4096;

    private final Logger log;

    private final Class<?> referenceClass;

    public FixtureResourcesHelper(Class<?> someClass) {
        referenceClass = someClass;
        log = LogManager.getLogger(referenceClass.getSimpleName());
    }

    public static void loadResourcesForClass(Class<?> someClass)
            throws IllegalAccessException, XPathExpressionException,
            ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder =
                documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(
                someClass.getResourceAsStream("/fixtureResources.xml"));

        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();

        Field[] fields = someClass.getDeclaredFields();

        String className = someClass.getCanonicalName();
        for (Field f : fields) {
            if (!f.isAnnotationPresent(FixtureResource.class))
                continue;
            String fieldName = f.getName();
            String content = xPath.evaluate(
                    makeExpression(className,fieldName), document);
            if (!f.isAccessible())
                f.setAccessible(true);
            f.set(someClass, content);
        }
    }

    private static String makeExpression(String className, String fieldName) {
        return String.format(
                XPATH_TEMPLATE_TEXT_RESOURCE, className, fieldName);
    }

    public String deployTemporaryResource(String resourcePath)
        throws IOException {

        URL resourceUrl = getResourceUrl(resourcePath);
        String outputPath = computeTemporaryPath(resourcePath);
        dumpResourceToFile(resourceUrl, outputPath);
        log.info(String.format(
                "Deployed '%s' to '%s'", resourcePath, outputPath));
        return outputPath;
    }

    private URL getResourceUrl(String resourcePath) {
        return referenceClass.getResource(resourcePath);
    }

    private static void dumpResourceToFile(URL originUrl, String outputPath)
        throws IOException {

        InputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            File outputFile = safeCreateFile(outputPath);
            inputStream = originUrl.openStream();
            outputStream = new FileOutputStream(outputFile);
            copyData(inputStream, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            tryClosing(inputStream);
            tryClosing(outputStream);
        }
    }

    private static File safeCreateFile(String path) throws IOException {
        File outputFile = new File(path);
        if (outputFile.getParentFile().exists()) {
            return outputFile;
        }
        if (outputFile.getParentFile().mkdirs()) {
            return outputFile;
        }
        throw new IOException("Unable to create directory for path: " + path);
    }

    private static void copyData(InputStream input, FileOutputStream output)
        throws IOException {
        int bytesRead;
        byte[] buffer = new byte[DATA_BUFFER_SIZE];
        while ((bytesRead = input.read(buffer)) > 0) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private static void tryClosing(Closeable closeable) {
        if (closeable == null)
            return;
        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String computeTemporaryPath(String resourcePath) {
        String destinationStart = System.getProperty("java.io.tmpdir");
        String separator = escape(File.separator);
        List<String> pathParts = asList(destinationStart.split(separator));
        pathParts.add("fixture-resources");
        List<String> moreParts = asList(resourcePath.split(escape("/")));
        pathParts.addAll(moreParts);
        return joinPathParts(pathParts);
    }

    private static String joinPathParts(List<String> parts) {
        List<String> myParts = new LinkedList<>(parts);
        StringBuilder buffer = new StringBuilder();
        buffer.append(myParts.remove(0));
        for (String part : myParts) {
            buffer.append(escape(File.separator));
            buffer.append(part);
        }
        String result = buffer.toString();
        buffer.setLength(0);
        return result;
    }

    private static String escape(String someString) {
        if (someString == null)
            return "";
        return StringEscapeUtils.escapeJava(someString);
    }

    private static List<String> asList(String[] array) {
        return new LinkedList<>(java.util.Arrays.asList(array));
    }
}
