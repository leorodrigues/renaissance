#!/usr/bin/env bash
SUSPEND=n
if [ "$1" == "wait" ]; then SUSPEND=y; fi
export APP_HOME="$(pwd)/shell/build/install/shell"
export JAVA_OPTS="$JAVA_OPTS -agentlib:jdwp=transport=dt_socket,server=y,suspend=$SUSPEND,address=5005"
./shell/build/install/shell/bin/shell