package org.leonardo.environment.shell.commands;

import org.leonardo.environment.ControlPoint;
import org.leonardo.renaissance.application.lifecycle.ControlPointLoadingProcedure;
import org.leonardo.environment.shell.ShellSession;
import org.leonardo.environment.shell.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import java.util.Arrays;

import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.join;
import static org.leonardo.environment.shell.Constants.END_OF_COMMAND;

/**
 * Created by leonardo on 17/02/2016.
 */
@Component
@Order(1)
@SuppressWarnings("SameReturnValue")
public class ConfigurationCommands implements CommandMarker {

    @Autowired
    private ShellSession session;

    @Autowired
    private FeedbackService feedBackService;

    @Autowired
    @Qualifier("setupProcedure")
    private ControlPointLoadingProcedure configurationProcedure;

    @CliCommand(
            value = "setup",
            help = "Execute first time setup.")
    public void setup() {
        ControlPoint controlPoint = configurationProcedure.execute();
        session.disengageEmptyContextMode(controlPoint);
    }

    @CliCommand(
            value = "state-save",
            help = "Save all current state.")
    public void saveState() {
        ControlPoint controlPoint = session.getControlPoint();
        controlPoint.saveState();
    }

    @CliCommand(
            value = "state-load",
            help = "Loads a previously saved state." +
                    " WARNING: Once called ALL unit states" +
                    " will be discarded and replaced.")
    public void loadState() {
        ControlPoint controlPoint = session.getControlPoint();
        controlPoint.loadState();
    }

    @CliCommand(
            value = "session-set",
            help = "Set session parameters")
    public void configSet(
            @CliOption(key = {"auto-save-units", "a"},
                    help = "Activates/Deactivates auto-save unit states on shutdown",
                    specifiedDefaultValue = "true")
                    Boolean autoSave) {

        if (autoSave != null)
            session.setAutoSave(autoSave);

        feedBackService.printCommandOutput(END_OF_COMMAND);
    }

    @CliCommand(
            value = "token-list",
            help = "General system configuration command")
    public void listTokens() {
        ControlPoint controlPoint = session.getControlPoint();
        String tokens[] = controlPoint.getRegisteredTokens();
        String output = join(Arrays.asList(tokens), ", ");
        feedBackService.printCommandOutput(output);
    }
}
