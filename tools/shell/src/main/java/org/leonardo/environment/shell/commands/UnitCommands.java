package org.leonardo.environment.shell.commands;

import org.lenardo.renaissance.collectionworks.Mapping;
import org.leonardo.environment.*;
import org.leonardo.environment.config.*;
import org.leonardo.environment.shell.FeedbackService;
import org.leonardo.environment.shell.ShellSession;
import org.leonardo.renaissance.command.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import static java.util.Arrays.asList;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.join;
import static org.lenardo.renaissance.collectionworks.MapperReducer.map;
import static org.leonardo.environment.CommonUnitName.newName;
import static org.leonardo.environment.shell.Constants.END_OF_COMMAND;
import static org.leonardo.renaissance.command.Feedback.makeError;

/**
 * Created by leonardo on 07/03/2016.
 */

@Component
@Order(1)
@SuppressWarnings("SameReturnValue")
public class UnitCommands implements CommandMarker {

    private static final String UNIT_NOT_DECLARED_TEMPLATE =
            "Unit '%s' was not declared.";

    @Autowired
    private ShellSession session;

    @Autowired
    private FeedbackService feedBackService;

    @CliCommand(value = "run",
            help = "Ask the current unit to run a resource.")
    public String run(
            @CliOption(key = "", mandatory = true,
                help = "A resource path within the unit.",
                unspecifiedDefaultValue = "",
                specifiedDefaultValue = "")
                String resourcePath) {

        Resource resource = new DefaultResource(resourcePath);
        ControlPoint controlPoint = session.getControlPoint();

        try {
            UnitName currentUnitName = session.getCurrentUnitName();
            Unit unit = controlPoint.getUnitByName(currentUnitName);
            unit.execute(resource);
        } catch (ResourceExecutionException e) {
            makeError(e.getMessage()).describe(feedBackService);
        }

        return END_OF_COMMAND;
    }


    @CliCommand(
            value = "unit-list",
            help = "Show unit names.")
    public void list() {
        ConfigurationIndex configurationIndex = session.getControlPoint();
        String output = joinNames(configurationIndex.getNames());
        feedBackService.printCommandOutput(output);
    }

    @CliCommand(
            value = "unit-env-set",
            help = "Set a variable.")
    public void setEnv(
            @CliOption(key = {""},
                    help = "Name and value separated by space.")
                    String variableAssignmentExpression,
            @CliOption(key = {"unit", "U"},
                    help = "Specify the complete name of a unit configuration.")
                    String givenUnitName) {

        UnitName unitName = session.givenNameOrCurrent(givenUnitName);

        ControlPoint controlPoint = session.getControlPoint();
        AssignmentExpression assignmentExpression =
                AssignmentExpression.parse(variableAssignmentExpression);

        try {
            Configuration configuration =
                    controlPoint.getConfigurationForName(unitName);

            configuration.set(
                    assignmentExpression.getVariableName(),
                    assignmentExpression.getAssignedValue());

            feedBackService.reportValueAssigned(assignmentExpression);
        } catch (NameNotFoundException | ConfigLockedException e) {
            feedBackService.reportVariableAssignmentFailed(
                    assignmentExpression, e);
        }
    }

    @CliCommand(
            value = "unit-env-unset",
            help = "Unset a variable.")
    public void unsetEnv(
            @CliOption(key = {""},
                    help = "Name and value separated by space.")
                    String variableName,
            @CliOption(key = {"unit", "U"},
                    help = "Specify the complete name of a unit configuration.")
                    String givenUnitName) {

        UnitName unitName = session.givenNameOrCurrent(givenUnitName);
        ControlPoint controlPoint = session.getControlPoint();

        try {
            Configuration configuration =
                    controlPoint.getConfigurationForName(unitName);
            configuration.unset(variableName);
            feedBackService.reportVariableRemoved(variableName);
        } catch (NameNotFoundException | ConfigLockedException e) {
            feedBackService.reportVariableRemovalFailed(variableName, e);
        }
    }

    @CliCommand(
            value = "unit-env-list",
            help = "Unset a variable.")
    public void listEnv(
            @CliOption(key = {"unit", "U"},
                    help = "Specify the complete name of a unit configuration.")
                    String givenUnitName) {

        UnitName unitName = session.givenNameOrCurrent(givenUnitName);

        try {
            ControlPoint controlPoint = session.getControlPoint();
            Configuration configuration =
                    controlPoint.getConfigurationForName(unitName);
            feedBackService.describeConfiguration(configuration);
        } catch (NameNotFoundException e) {
            feedBackService.reportConfigurationDescriptionFailed(e);
        }
    }

    @CliCommand(
            value = "unit-declare",
            help = "Declare a new unit.")
    public String declare(
            @CliOption(key = {""},
                    mandatory = true,
                    help = "A unit name expression.")
                    String identification) {

        UnitName name = newName(identification);

        ControlPoint controlPoint = session.getControlPoint();

        try {
            String fabricationToken = name.getFabricationToken();
            if (!controlPoint.hasUnitFactoryForToken(fabricationToken))
                feedBackService.reportWillUseDefaultFactory(fabricationToken);
            controlPoint.declareName(name);
            feedBackService.reportNameDeclarationSucceeded(name);
        } catch (NameAlreadyDeclaredException e) {
            feedBackService.reportNameDeclarationFailed(e);
        }

        return END_OF_COMMAND;
    }

    @CliCommand(
            value = "unit-dispose",
            help = "Dispose of an existing unit.")
    public String dispose(
            @CliOption(key = {""},
                    mandatory = true,
                    help = "A unit name expression.")
                    String identification) {

        UnitName name = newName(identification);

        ControlPoint controlPoint = session.getControlPoint();

        try {
            controlPoint.disposeOfName(name);
            if (session.getCurrentUnitName().equals(name))
                session.setCurrentUnitName(BasicUnitName.INSTANCE);
            feedBackService.reportDisposingOfNameSucceeded(name);
        } catch (NameNotFoundException e) {
            feedBackService.reportDisposingOfNameFailed(e);
        }

        return END_OF_COMMAND;
    }

    @CliCommand(
            value = "unit-select",
            help = "Change the current unit.")
    public void select(
            @CliOption(key = {""},
                    mandatory = true,
                    help = "A unit name expression.")
                    String nameExpression) {

        ControlPoint controlPoint = session.getControlPoint();
        UnitName nextUnitName = newName(nameExpression);
        if (controlPoint.isNameDeclared(nextUnitName)) {
            session.setCurrentUnitName(nextUnitName);
            return;
        }
        Feedback feedback = makeErrorUnitNotDeclared(nextUnitName);
        feedBackService.reportStep(feedback);
    }

    private String joinNames(UnitName[] names) {
        return join(map(asList(names), nameToStringMapping), ", ");
    }

    private Feedback makeErrorUnitNotDeclared(UnitName name) {
        return makeError(UNIT_NOT_DECLARED_TEMPLATE, name.getDisplayString());
    }

    private static final Mapping<String, UnitName> nameToStringMapping =
        new Mapping<String, UnitName>() {
            @Override
            public String apply(UnitName nextElement) {
                return nextElement.getDisplayString();
            }
        };

}
