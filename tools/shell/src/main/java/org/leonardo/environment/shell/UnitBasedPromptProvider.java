package org.leonardo.environment.shell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultPromptProvider;
import org.springframework.stereotype.Component;

import java.util.Calendar;

import static java.lang.String.format;
import static org.leonardo.environment.shell.Constants.PROVIDER_NAME;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

/**
 * Created by leonardo on 30/01/2016.
 */
@Component
@Order(HIGHEST_PRECEDENCE)
public class UnitBasedPromptProvider extends DefaultPromptProvider {

    @Autowired
    private ShellSession session;

    @Override
    public String getPrompt() {
        if (session.isEmptyContextMode())
            return format("%s\ninput: ", getTime());
        String name = session.getCurrentUnitName().getDisplayString();
        return format("%s @%s\ninput: ", getTime(), name);
    }

    @Override
    public String getProviderName() {
        return PROVIDER_NAME;
    }

    private static String getTime() {
        return format("%1$tF %1$tT", Calendar.getInstance());
    }
}
