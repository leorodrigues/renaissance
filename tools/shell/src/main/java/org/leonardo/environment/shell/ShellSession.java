package org.leonardo.environment.shell;

import org.leonardo.environment.BasicUnitName;
import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.application.lifecycle.ControlPointLoadingProcedure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static org.leonardo.environment.CommonUnitName.newName;

/**
 * Created by leonardo on 31/01/2016.
 */
@Component
public class ShellSession {

    private static final String EMPTY_CONTEXT_NOTICE =
        "Control functions not available in empty context mode." +
        " Please run command 'setup'.";

    @Autowired
    private FeedbackService feedBackService;

    @Autowired
    @Qualifier("startUpProcedure")
    private ControlPointLoadingProcedure configurationProcedure;

    private boolean emptyContextMode = false;

    private UnitName currentUnitName;

    private ControlPoint controlPoint;

    private boolean autoSave = true;

    @PostConstruct
    public void executeControlPointStartUp() {
        try {
            controlPoint = configurationProcedure.execute();
            disengageEmptyContextMode(controlPoint);
        } catch (Exception e) {
            engageEmptyContextMode();
        }
    }

    public boolean isEmptyContextMode() {
        return emptyContextMode;
    }

    public void engageEmptyContextMode() {
        this.emptyContextMode = true;
    }

    public void disengageEmptyContextMode(ControlPoint controlPoint) {
        if (!controlPoint.isNameDeclared(BasicUnitName.INSTANCE))
            return;
        emptyContextMode = false;
        this.controlPoint = controlPoint;
        setCurrentUnitName(BasicUnitName.INSTANCE);
    }

    public UnitName getCurrentUnitName() {
        return currentUnitName;
    }

    public void setCurrentUnitName(UnitName currentUnitName) {
        this.currentUnitName = currentUnitName;
    }

    public ControlPoint getControlPoint() {
        if (isEmptyContextMode())
            throw new RuntimeException(EMPTY_CONTEXT_NOTICE);
        return controlPoint;
    }

    public UnitName givenNameOrCurrent(String givenName) {
        if (givenName == null || givenName.isEmpty())
            return getCurrentUnitName();
        return newName(givenName);
    }

    public boolean isAutoSave() {
        return autoSave;
    }

    public void setAutoSave(boolean autoSave) {
        this.autoSave = autoSave;
    }
}
