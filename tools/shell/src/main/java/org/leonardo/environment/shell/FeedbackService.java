package org.leonardo.environment.shell;

import org.leonardo.environment.*;
import org.leonardo.environment.config.Configuration;
import org.leonardo.environment.config.NameAlreadyDeclaredException;
import org.leonardo.environment.config.NameNotFoundException;
import org.leonardo.environment.shell.commands.AssignmentExpression;
import org.leonardo.renaissance.command.Feedback;
import org.leonardo.renaissance.command.FeedbackOutput;

/**
 * Created by leonardo on 28/03/2016.
 */
public interface FeedbackService extends FeedbackOutput {
    void reportNameDeclarationSucceeded(UnitName name);

    void reportNameDeclarationFailed(NameAlreadyDeclaredException e);

    void describeConfiguration(Configuration configuration);

    void printCommandOutput(String output);

    void reportConfigurationDescriptionFailed(NameNotFoundException e);

    void reportUnitShutdownFailed(Exception e);

    void reportStep(String message);

    void reportStep(Feedback feedback);

    void reportVariableRemovalFailed(String variableName, Exception e);

    void reportVariableAssignmentFailed(
            AssignmentExpression assignmentExpression, Exception e);

    void reportVariableRemoved(String variableName);

    void reportValueAssigned(AssignmentExpression assignmentExpression);

    void reportWillUseDefaultFactory(String fabricationToken);

    void reportDisposingOfNameSucceeded(UnitName name);

    void reportDisposingOfNameFailed(NameNotFoundException exception);
}
