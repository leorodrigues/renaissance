package org.leonardo.environment.shell;

/**
 * Created by leonardo on 31/01/2016.
 */
public final class Constants {
    public static final String PROVIDER_NAME =
            "Environment control shell";

    public static final String END_OF_COMMAND =
            "Shell command execution complete.";

    public static final String COMP_VARIABLES = "variables";

    public static final String COMP_TOKENS = "tokens";

}
