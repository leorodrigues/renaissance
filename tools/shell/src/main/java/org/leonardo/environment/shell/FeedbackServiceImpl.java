package org.leonardo.environment.shell;

import org.leonardo.environment.*;
import org.leonardo.environment.config.Configuration;
import org.leonardo.environment.config.ConfigurationDescriptionBuilder;
import org.leonardo.environment.config.NameAlreadyDeclaredException;
import org.leonardo.environment.config.NameNotFoundException;
import org.leonardo.environment.shell.commands.AssignmentExpression;
import org.leonardo.renaissance.command.Feedback;
import org.leonardo.renaissance.command.FeedbackKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.logging.Logger;

import static java.lang.String.format;
import static org.leonardo.renaissance.command.Feedback.makeError;
import static org.leonardo.renaissance.command.Feedback.makeInfo;

/**
 * Created by leonardo on 18/02/2016.
 */
@Component("feedBackService")
public class FeedbackServiceImpl implements FeedbackService {

    private static final String DISPOSE_OF_NAME_SUCCESS_TEMPLATE =
        "Unit '%s' was successfully disposed of.";

    private static final String DISPOSE_OF_NAME_FAIL_TEMPLATE =
        "Failed to dispose of unit: %s";

    private final String DEFAULT_FACTORY_WILL_BE_USED_MSG_TEMPLATE =
        "Token '%s' has no factory associated with it. Upon unit " +
        "instantiation, a default factory will be used instead if available.";

    @Autowired
    @Qualifier("feedBackServiceLogger")
    private Logger logger;

    @Override
    public void reportNameDeclarationSucceeded(UnitName name) {
        logger.info(format(
                "Unit '%s' declared successfully.", name.getDisplayString()));
    }

    @Override
    public void reportNameDeclarationFailed(NameAlreadyDeclaredException e) {
        logger.severe(e.getMessage());
    }

    @Override
    public void describeConfiguration(Configuration configuration) {
        final StringBuilder stringBuilder = new StringBuilder();
        configuration.describe(new ConfigurationDescriptionBuilder() {
            @Override
            public void append(
                    String name, String value, String expandedValue) {
                stringBuilder.append(format(
                        "%s\t%s\t%s\n", name, value, expandedValue));
            }

            @Override
            public void append(String name, String value) {
                stringBuilder.append(format(
                        "%s\t%s\n", name, value));
            }
        });
        logger.info(stringBuilder.toString());
    }

    @Override
    public void printCommandOutput(String output) {
        logger.info(output + "\n");
    }

    @Override
    public void reportConfigurationDescriptionFailed(NameNotFoundException e) {
        logger.severe(e.getMessage());
    }

    @Override
    public void reportUnitShutdownFailed(Exception e) {
        logger.severe(e.getMessage());
    }

    @Override
    public void reportStep(String message) {
        logger.info(message);
    }

    @Override
    public void reportStep(Feedback feedback) {
        switch (feedback.getKind()) {
            case WARNING: logger.warning(feedback.getMessage()); break;
            case ERROR: logger.severe(feedback.getMessage()); break;
            default: logger.info(feedback.getMessage());
        }
    }

    @Override
    public void reportVariableRemovalFailed(String variableName, Exception e) {
        logger.severe(format(
                "Error trying to remove variable '%s': %s",
                variableName,
                e.getMessage()));
    }

    @Override
    public void reportVariableAssignmentFailed(
            AssignmentExpression assignmentExpression, Exception e) {

        logger.severe(format(
                "Failure: %s <- `%s`; Cause: %s",
                assignmentExpression.getVariableName(),
                assignmentExpression.getAssignedValue(),
                e.getMessage()));
    }

    @Override
    public void reportValueAssigned(AssignmentExpression assignmentExpression) {
        logger.info(format(
            "Success: %s <- `%s`",
            assignmentExpression.getVariableName(),
            assignmentExpression.getAssignedValue()));
    }

    @Override
    public void reportVariableRemoved(String variableName) {
        logger.info(format("Success: %s cleared.", variableName));
    }

    @Override
    public void reportWillUseDefaultFactory(String fabricationToken) {
        logger.warning(format(
                DEFAULT_FACTORY_WILL_BE_USED_MSG_TEMPLATE, fabricationToken));
    }

    @Override
    public void reportDisposingOfNameSucceeded(UnitName name) {
        String displayString = name.getDisplayString();
        makeInfo(DISPOSE_OF_NAME_SUCCESS_TEMPLATE, displayString)
            .describe(this);
    }

    @Override
    public void reportDisposingOfNameFailed(NameNotFoundException exception) {
        makeError(DISPOSE_OF_NAME_FAIL_TEMPLATE, exception.getMessage())
            .describe(this);
    }

    @Override
    public void write(
            long milliSecondsSinceEpoch, FeedbackKind kind, String message) {
        Calendar eventTime = getEventTime(milliSecondsSinceEpoch);

        String completeMessage =
                format("@ %1$tF %1$tT - %2$s", eventTime, message);
        switch (kind) {
            case WARNING:
                logger.warning(completeMessage);
                break;
            case ERROR:
                logger.severe(completeMessage);
                break;
            case INFO:
                logger.info(completeMessage);
                break;
            default:
                logger.info(message);
        }
    }

    private Calendar getEventTime(long milliSecondsSinceEpoch) {
        Calendar eventTime = Calendar.getInstance();
        eventTime.setTimeInMillis(milliSecondsSinceEpoch);
        return eventTime;
    }
}
