package org.leonardo.environment.shell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultBannerProvider;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Scanner;

import static org.leonardo.environment.shell.Constants.PROVIDER_NAME;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

/**
 * Created by leonardo on 30/01/2016.
 */
@Component
@Order(HIGHEST_PRECEDENCE)
public class LocalBannerProvider extends DefaultBannerProvider {

    private static final String FAIL_SAFE_BANNER_PATH =
            "/org/leonardo/environment/shell/empty-context-banner.txt";

    private static final String BANNER_RESOURCE_PATH =
            "/org/leonardo/environment/shell/banner.txt";

    @Autowired
    private ShellSession session;

    @Override
    public String getBanner() {
        StringBuilder banner = new StringBuilder();
        Scanner scanner = new Scanner(openTextFile());
        loadLines(banner, scanner);
        scanner.close();
        return banner.toString();
    }

    @Override
    public String getProviderName() {
        return PROVIDER_NAME;
    }

    private void loadLines(StringBuilder banner, Scanner scanner) {
        if (!scanner.hasNextLine())
            return;
        banner.append(scanner.nextLine());
        while (scanner.hasNextLine()) {
            banner.append("\n");
            banner.append(scanner.nextLine());
        }
        banner.append("\n");
    }

    private InputStream openTextFile() {
        if (session.isEmptyContextMode())
            return getClass().getResourceAsStream(FAIL_SAFE_BANNER_PATH);
        return getClass().getResourceAsStream(BANNER_RESOURCE_PATH);
    }
}
