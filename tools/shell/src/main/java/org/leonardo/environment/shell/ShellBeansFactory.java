package org.leonardo.environment.shell;

import org.leonardo.renaissance.application.lifecycle.ControlPointShutdownProcedure;
import org.leonardo.renaissance.application.FSControlPointKit;
import org.leonardo.renaissance.application.lifecycle.ControlPointLoadingProcedure;
import org.leonardo.renaissance.application.lifecycle.LoadingProcedureAssembler;
import org.leonardo.renaissance.command.FeedbackListener;
import org.leonardo.renaissance.command.SerialCommandRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

import static java.lang.System.getProperty;
import static java.lang.System.setProperty;
import static org.springframework.shell.support.logging.HandlerUtils.getLogger;

/**
 * Created by leonardo on 18/07/2016.
 */
@Component("shellBeansFactory")
public class ShellBeansFactory {
    private static final String HOME_DIRECTORY_NAME =
            ".renaissance";

    private static final String STATE_FILE_NAME =
            "control-point-state.ser";

    private static final String LOG_FORMAT_PROP =
            "java.util.logging.SimpleFormatter.format";

    private static final String LOG_TEMPLATE =
            "%1$tF %1$tT %4$s %5$s%6$s%n";

    private static final FSControlPointKit fsApplicationKit =
        new FSControlPointKit(
            getProperty("user.home"), STATE_FILE_NAME, HOME_DIRECTORY_NAME);

    private static final FeedbackListener feedbackListener =
        fsApplicationKit.makeFeedbackListener(
            getLogger(FeedbackListener.class));

    private static final SerialCommandRunner serialCommandRunner =
        fsApplicationKit.makeSerialCommandRunner(feedbackListener);


    static {
        setProperty(LOG_FORMAT_PROP, LOG_TEMPLATE);
    }

    @Bean(name = "feedBackServiceLogger")
    public Logger createFeedBackServiceLogger() {
        return getLogger(FeedbackServiceImpl.class);
    }

    @Bean
    public ControlPointShutdownProcedure createShutdownProcedure() {
        return fsApplicationKit.makeShutdownProcedure(
            feedbackListener, serialCommandRunner);
    }

    @Bean(name = "setupProcedure")
    public ControlPointLoadingProcedure createSetupProcedure() {
        LoadingProcedureAssembler assembler =
            fsApplicationKit.makeSetupAssembler(
                feedbackListener, serialCommandRunner);

        return assembler.assemble();
    }

    @Bean(name = "startUpProcedure")
    public ControlPointLoadingProcedure createStartUpProcedure() {
        LoadingProcedureAssembler assembler =
            fsApplicationKit.makeStartupAssembler(
                feedbackListener, serialCommandRunner);

        return assembler.assemble();
    }
}
