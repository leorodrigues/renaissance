package org.leonardo.environment.shell;

import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultHistoryFileNameProvider;
import org.springframework.stereotype.Component;

import static org.leonardo.environment.shell.Constants.PROVIDER_NAME;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

/**
 * Created by leonardo on 30/01/2016.
 */
@Component
@Order(HIGHEST_PRECEDENCE)
public class LocalHistoryFileProvider extends DefaultHistoryFileNameProvider {
    @Override
    public String getHistoryFileName() {
        return "history.log";
    }

    @Override
    public String getProviderName() {
        return PROVIDER_NAME;
    }
}
