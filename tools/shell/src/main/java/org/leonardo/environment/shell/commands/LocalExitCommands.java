package org.leonardo.environment.shell.commands;

import org.leonardo.environment.shell.ShellSession;
import org.leonardo.environment.shell.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.ExitShellRequest;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.stereotype.Component;

import static org.leonardo.environment.shell.Constants.END_OF_COMMAND;

/**
 * Created by leonardo on 20/03/2016.
 */

@Component
@SuppressWarnings("SameReturnValue")
public class LocalExitCommands implements CommandMarker {

    @Autowired
    private FeedbackService feedBackService;

    @Autowired
    private UnitLifeCycleControlCommands unitLifeCycleControlCommands;

    @Autowired
    private ShellSession session;

    @CliCommand(
            value = "exit",
            help = "Shut's down all units and exit's the shell.")
    public ExitShellRequest quit() {
        if (!session.isEmptyContextMode())
            unitLifeCycleControlCommands.shutdownAllUnits();
        feedBackService.reportStep(END_OF_COMMAND);
        return ExitShellRequest.NORMAL_EXIT;
    }
}
