package org.leonardo.environment.shell.commands;

import org.leonardo.environment.BasicUnitName;
import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.UnitName;
import org.leonardo.environment.shell.FeedbackService;
import org.leonardo.environment.shell.ShellSession;
import org.leonardo.renaissance.application.lifecycle.ControlPointShutdownProcedure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import static org.leonardo.environment.shell.Constants.END_OF_COMMAND;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

/**
 * Created by leonardo on 30/01/2016.
 */
@Component
@Order(HIGHEST_PRECEDENCE)
@SuppressWarnings("SameReturnValue")
public class UnitLifeCycleControlCommands implements CommandMarker {

    @Autowired
    private FeedbackService feedBackService;

    @Autowired
    private ShellSession session;

    @Autowired
    private ControlPointShutdownProcedure shutdownProcedure;

    @CliCommand(value = "unit-boot",
            help = "Brings one unit up.")
    public String bootUnit(
            @CliOption(key = {""}, mandatory = true,
                help = "A string identification.",
                unspecifiedDefaultValue = "",
                specifiedDefaultValue = "")
                String givenUnitName) {

        ControlPoint controlPoint = session.getControlPoint();

        UnitName unitName = session.givenNameOrCurrent(givenUnitName);

        feedBackService.reportStep("Bringing up unit '" +
                unitName.getDisplayString() + "' ...");

        try {
            controlPoint.bootUnit(unitName);
            session.setCurrentUnitName(unitName);
        } catch (Exception e) {
            feedBackService.reportUnitShutdownFailed(e);
        }

        return END_OF_COMMAND;
    }

    @CliCommand(value = "unit-shutdown",
        help = "Shutdown one unit")
    public String shutdownUnit(
            @CliOption(key = {""}, mandatory = true,
                    help = "A string identification.",
                    unspecifiedDefaultValue = "",
                    specifiedDefaultValue = "")
                    String givenUnitName) {

        ControlPoint controlPoint = session.getControlPoint();

        UnitName unitName = session.givenNameOrCurrent(givenUnitName);

        feedBackService.reportStep("Shutting down unit '" +
                unitName.getDisplayString() + "' ...");

        try {
            fallBackToBasicUnit();
            controlPoint.shutdownUnit(unitName);
        } catch (Exception e) {
            feedBackService.reportUnitShutdownFailed(e);
        }

        return END_OF_COMMAND;
    }

    @CliCommand(value = "system-shutdown",
            help = "Shutdown all units.")
    public void shutdownAllUnits() {
        ControlPoint controlPoint = session.getControlPoint();
        fallBackToBasicUnit();
        shutdownProcedure.execute(controlPoint, session.isAutoSave());
    }

    private void fallBackToBasicUnit() {
        UnitName name = session.getCurrentUnitName();
        if (name.equals(BasicUnitName.INSTANCE))
            return;
        session.setCurrentUnitName(BasicUnitName.INSTANCE);
    }
}
