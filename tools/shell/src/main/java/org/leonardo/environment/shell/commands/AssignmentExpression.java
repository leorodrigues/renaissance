package org.leonardo.environment.shell.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

public class AssignmentExpression {

    private static final transient Pattern assignmentPattern =
        Pattern.compile("(\\w+)\\s+(.*)");

    private static final String MALFORMED_ASSIGNMENT_MSG_TEMPLATE =
            "Malformed assignment expression: '%s'";

    private String variableName;
    private String assignedValue;

    private AssignmentExpression(
            String variableName, String assignedValue) {
        this.variableName = variableName;
        this.assignedValue = assignedValue;
    }

    public String getVariableName() {
        return variableName;
    }

    public String getAssignedValue() {
        return assignedValue;
    }

    public static AssignmentExpression parse(String expressionText) {
        Matcher m = assignmentPattern.matcher(expressionText);
        if (!m.matches())
            throw new IllegalArgumentException(format(
                MALFORMED_ASSIGNMENT_MSG_TEMPLATE, expressionText));
        return new AssignmentExpression(m.group(1), m.group(2));
    }
}
