package org.leonardo.testing.integration.util

import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.springframework.shell.core.JLineShellComponent

/**
 * Created by Lrteixeira on 20/07/2016.
 */
public class IntegrationSupport {

    public static DrainLogger log

    public static JLineShellComponent shell

    @Before
    public void setup(spyLogger) {
        log.delegate = spyLogger
    }

    @BeforeClass
    public static void startUp() {
        (log, shell) = IntegrationTestLifeCycle.startUp()
    }

    @AfterClass
    public static void shutdown() {
        IntegrationTestLifeCycle.shutdown()
    }
}
