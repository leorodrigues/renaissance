package org.leonardo.testing.integration.util

import org.leonardo.environment.shell.ShellBeansFactory
import org.springframework.context.annotation.Bean

import java.util.logging.Logger
/**
 * Created by Lrteixeira on 19/07/2016.
 */
public class TestShellBeansFactory extends ShellBeansFactory {

    @Override
    @Bean(name = "feedBackServiceLogger")
    public Logger createFeedBackServiceLogger() {
        return DrainLogger.instance
    }
}
