package org.leonardo.testing.integration.shell

import spock.lang.Specification
import spock.lang.Unroll

import static org.leonardo.environment.shell.commands.AssignmentExpression.parse

class AssignmentExpressionSpec extends Specification {

    @Unroll
    def "parse; success; '#expression' should yield name '#name' and value '#value'"() {
        given:
        def subject = parse((String)expression)

        expect:
        subject.variableName == name
        subject.assignedValue == value

        where:
        expression | name | value
        'x y'      | 'x'  | 'y'
        'z a b c'  | 'z'  | 'a b c'
    }

    def "parse; error; malformed assignment expression text"() {
        when:
        parse("")

        then:
        thrown(IllegalArgumentException)
    }
}
