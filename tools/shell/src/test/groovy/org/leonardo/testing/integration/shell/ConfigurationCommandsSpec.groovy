package org.leonardo.testing.integration.shell

import org.leonardo.testing.integration.util.IntegrationSupport
import spock.lang.Specification

import java.util.logging.Logger

/**
 * Created by Lrteixeira on 20/07/2016.
 */
class ConfigurationCommandsSpec extends Specification {

    def "describe unit configuration; sucess"() {
        when:
        shell.executeCommand("unit-describe basic")

        then:
        1 * log.delegate.info("NAME\tsurrogate:basic:0")
    }

    def "list units; success; no unit declared so far"() {
        when:
        shell.executeCommand("unit-list")

        then:
        1 * log.delegate.info("surrogate-basic#0")
    }

    def "list units; success; a unit has been declared"() {
        given:
        shell.executeCommand("unit-declare secret")

        when:
        shell.executeCommand("unit-list")

        then:
        1 * log.delegate.info("surrogate-basic#0, surrogate-secret#0")
    }

    def "declare new unit; success; just the instance name"() {
        when:
        shell.executeCommand("unit-declare first")

        then:
        1 * log.delegate.info("Unit 'surrogate-first#0' declared successfully.")
    }

    def "declare new unit; success; using instance number"() {
        when:
        shell.executeCommand("unit-declare second --i 11235")

        then:
        1 * log.delegate.info("Unit 'surrogate-second#11235' declared successfully.")
    }

    def "declare new unit; success; with warning message"() {
        when:
        shell.executeCommand("unit-declare third --ft ${factoryToken}")

        then:
        1 * log.delegate.warning("Token '${factoryToken}' has no factory associated with it. Upon unit instantiation, a default factory will be used instead if available.")
        1 * log.delegate.info("Unit '${factoryToken}-third#0' declared successfully.")
    }

    def "declare new unit; failure; duplicate name"() {
        when:
        shell.executeCommand("unit-declare fourth")
        shell.executeCommand("unit-declare fourth")

        then:
        1 * log.delegate.info("Unit 'surrogate-fourth#0' declared successfully.")
        1 * log.delegate.severe("The name 'surrogate-fourth#0' is already declared.")
    }

    def "list fabrication tokens; ok"() {
        when:
        shell.executeCommand("config --list fabrication-token")

        then:
        1 * log.delegate.info("rhino-basic, surrogate")
    }

    static factoryToken = "my-factory"
    static shell
    static log

    void setup() {
        support.setup(Spy(Logger, constructorArgs: [null, null]))
    }

    void setupSpec() {
        support.startUp()
        shell = support.shell
        log = support.log
    }

    void cleanupSpec() {
        support.shutdown()
    }

    static support = new IntegrationSupport()
}
