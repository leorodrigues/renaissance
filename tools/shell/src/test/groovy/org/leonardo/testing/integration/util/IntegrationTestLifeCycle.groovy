package org.leonardo.testing.integration.util

import org.springframework.shell.Bootstrap
import org.springframework.shell.core.JLineShellComponent
/**
 * Created by Nardo on 25/03/2016.
 */
class IntegrationTestLifeCycle {

    private static JLineShellComponent shell

    public static startUp() throws InterruptedException {
        String[] contextPath = [
                "classpath*:/META-INF/spring/spring-shell-plugin.xml",
                "classpath*:/META-INF/spring/test-spring-shell-plugin.xml"
        ]
        Bootstrap bootstrap = new Bootstrap(new String[0], contextPath)
        shell = bootstrap.JLineShellComponent

        return [DrainLogger.instance, shell]
    }

    public static void shutdown() {
        shell.stop()
    }
}
