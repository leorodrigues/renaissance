package org.leonardo.testing.integration.util

import java.util.logging.Logger

/**
 * Created by Lrteixeira on 19/07/2016.
 */
class DrainLogger extends Logger {

    private static DrainLogger theInstance = new DrainLogger(null, null)

    public Logger delegate = null

    public static DrainLogger getInstance() {
        return theInstance
    }

    protected DrainLogger(String name, String resourceBundleName) {
        super(name, resourceBundleName)
    }

    @Override
    void info(String msg) {
        if (delegate)
            delegate.info(msg)
        else
            super.info(msg)
    }

    @Override
    void severe(String msg) {
        if (delegate)
            delegate.severe(msg)
        else
            super.severe(msg)
    }

    @Override
    void warning(String msg) {
        if (delegate)
            delegate.warning(msg)
        else
            super.warning(msg)
    }
}
