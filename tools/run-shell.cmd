@ECHO OFF

SET SUSPEND=n
IF ".%1"== ".wait" SET SUSPEND=y

SET JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=%SUSPEND%,address=5005
SET APP_HOME=%CD%\shell\build\install\shell
CALL shell\build\install\shell\bin\shell.bat
