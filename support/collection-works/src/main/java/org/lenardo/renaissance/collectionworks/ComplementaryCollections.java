package org.lenardo.renaissance.collectionworks;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.lenardo.renaissance.collectionworks.MapperReducer.reduce;

/**
 * Created by Lrteixeira on 24/11/2015.
 */
public final class ComplementaryCollections {

    public static <T> T head(Collection<T> someCollection, T defaultValue) {
        if (someCollection == null)
            return defaultValue;
        if (someCollection.isEmpty())
            return defaultValue;

        return someCollection.iterator().next();
    }

    public static <T> Iterable<T> tail(final Collection<T> someCollection) {
        Iterable<T> defaultValue = new Iterable<T>() {
            @Override
            public Iterator<T> iterator() {
                return new Iterator<T>() {
                    @Override
                    public boolean hasNext() { return false; }

                    @Override
                    public T next() { return null; }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };

        if (someCollection == null)
            return defaultValue;
        if (someCollection.isEmpty())
            return defaultValue;

        final Iterator<T> targetIterator = someCollection.iterator();
        targetIterator.next();
        return new Iterable<T>() {
            @Override
            public Iterator<T> iterator() {
                return targetIterator;
            }
        };
    }

    public static String join(
            Collection<String> elements,
            String separator) {

        StringBuilder buffer = new StringBuilder();

        buffer.append(head(elements, ""));

        for (String s : tail(elements)) {
            buffer.append(separator);
            buffer.append(s);
        }

        return buffer.toString();
    }

    public static <T> void join(
            Collection<T> someCollection,
            String separator,
            PrintStream output) {

        if (someCollection == null)
            return;

        if (someCollection.isEmpty())
            return;

        output.print(head(someCollection, null));
        for (T element : tail(someCollection)) {
            output.print(separator);
            output.print(element);
        }
    }

    public static boolean all(Collection<Boolean> elements) {
        for (Boolean b : elements) if (!b) return false;
        return true;
    }

    public static boolean none(Collection<Boolean> elements) {
        for (Boolean b : elements) if (b) return false;
        return true;
    }

    public static <T> List<T> complement(
            Collection<T> a, final Collection<T> b) {
        return reduce(new LinkedList<T>(), a, new Reduction<List<T>, T>() {
            @Override
            public List<T> apply(List<T> accumulated, T nextElement) {
                if (!b.contains(nextElement))
                    accumulated.add(nextElement);
                return accumulated;
            }
        });
    }

    public static <T> List<T> intersection(
            Collection<T> a, final Collection<T> b) {
        return reduce(new LinkedList<T>(), a, new Reduction<List<T>, T>() {
            @Override
            public List<T> apply(List<T> accumulated, T nextElement) {
                if (b.contains(nextElement))
                    accumulated.add(nextElement);
                return accumulated;
            }
        });
    }
}
