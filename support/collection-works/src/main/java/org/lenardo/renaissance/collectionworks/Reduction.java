package org.lenardo.renaissance.collectionworks;

public interface Reduction<T, U> {
    T apply(T accumulated, U nextElement);
}
