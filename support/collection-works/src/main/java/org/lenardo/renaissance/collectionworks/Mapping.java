package org.lenardo.renaissance.collectionworks;

public interface Mapping<U, R> {
    U apply(R nextElement);
}
