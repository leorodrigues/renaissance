package org.lenardo.renaissance.collectionworks;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public final class MapperReducer {

    public static <T, U> List<T> map(
            Collection<U> elements, Mapping<T, U> mapping) {

        LinkedList<T> result = new LinkedList<>();
        for (U element : elements)
            result.add(mapping.apply(element));
        return result;
    }

    public static <T, U> T reduce(
            T initial, Collection<U> elements, Reduction<T, U> reduction) {

        T result = initial;
        for (U nextElement : elements)
            result = reduction.apply(result, nextElement);
        return result;
    }
}
