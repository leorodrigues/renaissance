package org.lenardo.renaissance.collectionworks;

/**
 * Created by leonardo on 29/12/2016.
 */
public interface Predicate<T> {
    boolean verifies(T argument);
}
