package org.leonardo.renaissance.utils;

/**
 * Created by leonardo on 29/12/2016.
 */
public class Optional<T> {
    private T data;

    public Optional() {
    }

    public Optional(T data) {
        this.data = data;
    }

    public boolean isPresent() {
        return data != null;
    }

    public T get() {
        if (data == null)
            throw new RuntimeException("No data available");
        return data;
    }
}
