package org.leonardo.renaissance.utils;

import java.util.List;

public class OptionalList<T> extends Optional<List<T>> {

    public OptionalList(List<T> data) {
        super(data);
    }

    @Override
    public boolean isPresent() {
        return super.isPresent() && super.get().size() > 0;
    }
}
