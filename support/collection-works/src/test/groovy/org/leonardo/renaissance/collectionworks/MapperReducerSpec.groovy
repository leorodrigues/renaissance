package org.leonardo.renaissance.collectionworks

import org.lenardo.renaissance.collectionworks.MapperReducer
import org.lenardo.renaissance.collectionworks.Mapping
import org.lenardo.renaissance.collectionworks.Reduction
import spock.lang.Specification

class MapperReducerSpec extends Specification {
    def "reduce; ok; reduce list with elements"() {
        expect:
        MapperReducer.reduce('', sequence, reduction) == '11235813'
    }

    def "reduce; ok; reduce empty list"() {
        expect:
        MapperReducer.reduce('', [], reduction) == ''
    }

    def "map; ok; mapping list with elements"() {
        expect:
        MapperReducer.map(sequence, mapping) == ['1', '1', '2', '3', '5', '8', '13']
    }

    def "map; ok; mapping empty list"() {
        expect:
        MapperReducer.map([], mapping) == []
    }

    def sequence = [1, 1, 2, 3, 5, 8, 13]

    def reduction = new ReductionGuineaPig()

    def mapping = new MappingGuineaPig()

    static class ReductionGuineaPig implements Reduction<String, Integer> {
        @Override
        String apply(String accumulated, Integer nextElement) {
            return "${accumulated}${nextElement}"
        }
    }

    static class MappingGuineaPig implements Mapping<String, Integer> {
        @Override
        String apply(Integer nextElement) {
            return nextElement.toString()
        }
    }
}
