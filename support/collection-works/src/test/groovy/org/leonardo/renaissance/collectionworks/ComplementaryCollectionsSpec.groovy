package org.leonardo.renaissance.collectionworks

import org.leonardo.testing.util.Memory
import spock.lang.Specification

import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.*

/**
 * Created by leonardo on 11/03/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class ComplementaryCollectionsSpec extends Specification {
    def "head; ok; gives out the head of a two item collection"() {
        expect:
        head(['a', 'b'], null) == 'a'
    }

    def "head; ok; gives out default value if collection is empty"() {
        expect:
        head([], 'x') == 'x'
    }

    def "head; ok; gives out default value if collection is null"() {
        expect:
        head(null, 'y') == 'y'
    }

    def "tail; ok; gives out the tail of a three item collection"() {
        expect:
        tail(['r', 's', 't']).iterator().collect { it } == ['s', 't']
    }

    def "tail; ok; gives out empty collection if input is empty"() {
        expect:
        tail([]).iterator().collect { it } == []
    }

    def "tail; ok; gives out empty collection if input is null"() {
        expect:
        tail(null).iterator().collect { it } == []
    }

    def "tail; error; iterator does not support remove if input is empty"() {
        when:
        tail([]).iterator().remove()

        then:
        thrown(UnsupportedOperationException)
    }

    def "tail; error; iterator does not support remove if input is null"() {
        when:
        tail(null).iterator().remove()

        then:
        thrown(UnsupportedOperationException)
    }

    def "tail; ok; iterator returns null if input is empty"() {
        expect:
        tail([]).iterator().next() == null
    }

    def "tail; ok; iterator returns null if input is null"() {
        expect:
        tail(null).iterator().next() == null
    }

    def "join; ok; prints the concatenated items using the given separator"() {
        given:
        def printer = new PrintStream(memory.getNewOutput());

        when:
        join(['k', 'l'], ';', printer)

        then:
        "k;l" == memory.snapshot()
    }

    def "join; ok; prints empty if collection is empty"() {
        given:
        def printer = new PrintStream(memory.getNewOutput());

        when:
        join([], ';', printer)

        then:
        '' == memory.snapshot()
    }

    def "join; ok; prints empty if collection is null"() {
        given:
        def printer = new PrintStream(memory.getNewOutput());

        when:
        join(null, ';', printer)

        then:
        '' == memory.snapshot()
    }

    def "join; ok; empty collection of strings"() {
        given:
        def elements = ['a', 'b', 'c'] as List<String>

        expect:
        join(elements, "/") == 'a/b/c'
    }

    def "complement; ok; elements of A not present in B"() {
        given:
        def a = [1, 2, 3]

        and:
        def b = [1, 3]

        expect:
        complement(a, b) == [2]
        complement(b, a) == []
    }

    def "intersection; ok; elements present in both collections"() {
        given:
        def a = [1, 2, 3]

        and:
        def b = [1, 3]

        expect:
        intersection(a, b) == [1, 3]
        intersection(b, a) == [1, 3]
    }

    private Memory memory = new Memory(40);

    void cleanup() { memory.close(); }
}
