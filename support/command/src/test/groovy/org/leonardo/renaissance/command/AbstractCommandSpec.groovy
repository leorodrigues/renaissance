package org.leonardo.renaissance.command

import spock.lang.Specification

import static org.leonardo.renaissance.command.FeedbackKind.INFO
import static org.leonardo.renaissance.command.FeedbackKind.WARNING

class AbstractCommandSpec extends Specification {

    def "reportInfo; ok"() {
        given:
        def feedbackListener = Mock(FeedbackListener)

        and:
        subject.setFeedbackListener(feedbackListener)

        when:
        subject.sayHello()

        then:
        1 * feedbackListener.register({ f -> f.kind == INFO })
    }

    def "reportWarning; ok"() {
        given:
        def feedbackListener = Mock(FeedbackListener)

        and:
        subject.setFeedbackListener(feedbackListener)

        when:
        subject.screamForHelp()

        then:
        1 * feedbackListener.register({ f -> f.kind == WARNING })
    }

    def "getRecoveryStrategy; ok; it has no recovery strategy"() {
        expect:
        !subject.getRecoveryStrategy(new Exception()).present
    }

    def "set/get next; ok; property 'next' works properly"() {
        given:
        def command = Mock(Command)

        when:
        subject.next = command

        then:
        subject.next.get().is(command)
    }

    def "getSuccessMessage; ok; provides default success message"() {
        expect:
        subject.successMessage == "GuineaPig - terminated - success"
    }

    def "getErrorMessage; ok; provides default error message"() {
        expect:
        subject.getErrorMessage(new Exception('y')) == "GuineaPig - terminated - failure - y"
    }

    def subject

    void setup() {
        subject = new GuineaPig()
    }

    static class GuineaPig extends AbstractCommand {
        @Override
        void execute() {}

        void sayHello() {
            reportInfo("hello!")
        }

        void screamForHelp() {
            reportWarning("HEEEELP!")
        }
    }
}
