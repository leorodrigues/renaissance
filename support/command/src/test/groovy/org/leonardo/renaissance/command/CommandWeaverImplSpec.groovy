package org.leonardo.renaissance.command

import spock.lang.Specification

class CommandWeaverImplSpec extends Specification {
    def "weave; ok; no commands"() {
        when:
        def result = subject.weave()

        then:
        !result.isPresent()
    }

    def "weave; ok; one command only"() {
        when:
        def result = subject.weave([firstCommand])

        then:
        result.present
        result.get().is(firstCommand)
    }

    def "weave; ok; more than one command"() {
        when:
        def result = subject.weave(firstCommand, secondCommand)

        then:
        result.present
        result.get().is(firstCommand)
        1 * firstCommand.setNext(secondCommand)
    }

    def subject
    def firstCommand
    def secondCommand

    void setup() {
        subject = new CommandWeaverImpl()
        firstCommand = Mock(AbstractCommand)
        secondCommand = Mock(AbstractCommand)
    }
}
