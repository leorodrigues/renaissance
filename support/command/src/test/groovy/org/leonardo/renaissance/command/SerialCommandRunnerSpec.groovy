package org.leonardo.renaissance.command

import spock.lang.Specification

import static org.leonardo.renaissance.command.CompletionLevel.FULL

class SerialCommandRunnerSpec extends Specification {
    def "run; success; a chain runner is given"() {
        given:
        def chainRunner = Mock(CommandChainRunner)

        and:
        subject.setCommandChainRunner(chainRunner)

        and:
        def oneCommand = Mock(Command)

        when:
        subject.run([oneCommand])

        then:
        1 * chainRunner.run(oneCommand) >> FULL
    }

    def "run; fail; no chain runner is set"() {
        given:
        subject.commandChainRunner = null

        when:
        subject.run()

        then:
        thrown(IllegalStateException)
    }

    def subject

    void setup() {
        subject = new SerialCommandRunner(Mock(CommandChainRunner))
    }
}
