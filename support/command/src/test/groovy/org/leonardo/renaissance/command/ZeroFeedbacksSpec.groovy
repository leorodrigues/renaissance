package org.leonardo.renaissance.command

import org.leonardo.renaissance.command.ZeroFeedbacks
import spock.lang.Specification

/**
 * Created by leonardo on 07/02/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class ZeroFeedbacksSpec extends Specification {
    def "includeAll, error, this operation is not supported"() {
        when:
        ZeroFeedbacks.INSTANCE.includeAll(null)

        then:
        thrown(UnsupportedOperationException)
    }

    def "isEmpty, true, it is always true"() {
        expect:
        ZeroFeedbacks.INSTANCE.empty
    }
}
