package org.leonardo.renaissance.command

import spock.lang.Specification
import org.leonardo.renaissance.utils.Optional

import static org.leonardo.renaissance.command.FeedbackKind.ERROR
import static org.leonardo.renaissance.command.FeedbackKind.INFO

class RunWithRecoverySpec extends Specification {
    def "run; ok; command is successful"() {
        given:
        def theCommand = Mock(Command)

        when:
        def success = subject.run(theCommand)

        then:
        success
        1 * theCommand.execute()
        1 * theCommand.getSuccessMessage() >> 'done'
        1 * feedbackListener.register({ f -> f.kind == INFO })
    }

    def "run; ok; command throws exception and will not try to recover"() {
        given:
        def theCommand = Mock(Command)

        and:
        def fakeError = new RuntimeException('thrown on purpose')

        when:
        def success = subject.run(theCommand)

        then:
        !success
        1 * theCommand.execute() >> { throw fakeError }
        1 * theCommand.getErrorMessage(fakeError) >> 'error'
        1 * feedbackListener.register({ f -> f.kind == ERROR })
        1 * theCommand.getRecoveryStrategy(fakeError) >> new Optional<>()
    }

    def "run; ok; command throws exception and will try to recover"() {
        given:
        def theCommand = Mock(Command)

        and:
        def fakeError = new RuntimeException('thrown on purpose')

        and:
        def strategy = Mock(RecoveryStrategy)

        when:
        def success = subject.run(theCommand)

        then:
        !success
        1 * theCommand.execute() >> { throw fakeError }
        1 * theCommand.getErrorMessage(fakeError) >> 'failed'
        1 * feedbackListener.register({ f -> f.kind == ERROR })
        1 * theCommand.getRecoveryStrategy(fakeError) >> new Optional<>(strategy)
        1 * strategy.invoke(fakeError)
        1 * strategy.getSuccessMessage(fakeError) >> 'succeeded'
        1 * feedbackListener.register({ f -> f.kind == INFO })
    }

    def "run; ok; command throws exception and recovery fails as well"() {
        given:
        def theCommand = Mock(Command)

        and:
        def firstError = new RuntimeException('thrown on purpose')

        and:
        def secondError = new RuntimeException('thrown on purpose')

        and:
        def strategy = Mock(RecoveryStrategy)

        when:
        def success = subject.run(theCommand)

        then:
        !success
        1 * theCommand.execute() >> { throw firstError }
        1 * theCommand.getErrorMessage(firstError) >> 'failed once'
        1 * feedbackListener.register({ f -> f.kind == ERROR })
        1 * theCommand.getRecoveryStrategy(firstError) >> new Optional<>(strategy)
        1 * strategy.invoke(firstError) >> { throw secondError }
        1 * strategy.getErrorMessage(secondError, firstError) >> 'failed'
        1 * feedbackListener.register({ f -> f.kind == ERROR })
    }

    def subject
    def feedbackListener

    void setup() {
        feedbackListener = Mock(FeedbackListener)
        subject = new RunWithRecovery(feedbackListener)
    }
}
