package org.leonardo.renaissance.command

import spock.lang.Specification

import static Feedback.makeError
import static org.leonardo.renaissance.command.Feedback.makeInfo
import static org.leonardo.renaissance.command.Feedback.makeWarning
import static org.leonardo.renaissance.command.FeedbackKind.WARNING
import static org.leonardo.renaissance.command.Feedbacks.makeErrors
import static org.leonardo.renaissance.command.Feedbacks.makeInstance

/**
 * Created by leonardo on 19/03/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class FeedbacksSpec extends Specification {
    def "describe; ok"() {
        given:
        Feedbacks feedbacks = makeInstance(new Feedback[0])

        and:
        def aFeedback = makeWarning('%s-', 'test')

        and:
        feedbacks.add(aFeedback)

        and:
        def output = Mock(FeedbackOutput)

        when:
        feedbacks.describe(output)

        then:
        1 * output.write(
            { m -> m instanceof Long },
            { k -> k == WARNING },
            { m -> m == 'test-' })
    }

    def "includeAll; ok; add ZeroFeedbacks"() {
        given:
        Feedbacks feedBacks = makeInstance(new Feedback[0])

        when:
        feedBacks.includeAll(ZeroFeedbacks.INSTANCE)

        then:
        feedBacks.empty
    }

    def "includeAll; ok; add one to another"() {
        given:
        Feedbacks feedBacks = makeInstance(new Feedback[0])
        Feedbacks moreErrors = makeErrors("some error")

        when:
        feedBacks.includeAll(moreErrors)

        then:
        !feedBacks.empty
    }

    def "makeInstance; ok; from empty FeedBack array"() {
        given:
        Feedback[] arrayOfBacks = []

        when:
        Feedbacks feedBacks = makeInstance(arrayOfBacks)

        then:
        feedBacks
        feedBacks.empty
    }

    def "makeInstance; ok; from one feedback instance"() {
        when:
        Feedbacks feedBacks = makeInstance(makeInfo('%s-',"msg"))

        then:
        feedBacks
        !feedBacks.empty
    }

    def "makeInstance; ok; from null and an error"() {
        given:
        def anError = makeError("some error")

        when:
        Feedbacks feedBacks = makeInstance(null, anError)

        then:
        feedBacks
        !feedBacks.empty
    }

    def "makeErrors; ok; from empty string array"() {
        given:
        String[] messages = []

        when:
        Feedbacks feedBacks = makeErrors(messages)

        then:
        feedBacks
        feedBacks.empty
    }

    def "makeErrors; ok; from one message"() {
        when:
        Feedbacks feedBacks = makeErrors("some message")

        then:
        feedBacks
        !feedBacks.empty
    }

    def "makeErrors; ok; from one message and null"() {
        when:
        Feedbacks feedBacks = makeErrors(null, "some message")

        then:
        feedBacks
        !feedBacks.empty
    }
}
