package org.leonardo.renaissance.command

import spock.lang.Specification

import static Feedback.*
import static org.leonardo.renaissance.command.FeedbackKind.*

/**
 * Created by leonardo on 07/02/2016.
 */
@SuppressWarnings("GroovyAssignabilityCheck")
class FeedbackSpec extends Specification {

    def "describe; ok; invoke output"() {
        given:
        def output = Mock(FeedbackOutput)

        and:
        def subject = makeInfo("test")

        and:
        Thread.sleep(1000)

        when:
        subject.describe(output)

        then:
        1 * output.write({ m -> m < Calendar.instance.timeInMillis }, INFO, 'test')
    }

    def "getDisplayString; ok"() {
        expect:
        makeError("test").getDisplayString() == "ERROR: test"
    }

    def "makeWarning; ok"() {
        given:
        long thisSecond = Calendar.instance.getTimeInMillis() / 1000

        when:
        Feedback subject = makeWarning("test")

        then:
        subject.kind == WARNING

        and:
        subject.message == "test"

        and:
        subject.milliSecondsSinceEpoch >= thisSecond
    }

    def "makeError; ok"() {
        given:
        long thisSecond = Calendar.instance.getTimeInMillis() / 1000

        when:
        Feedback subject = makeError("%s", 'test')

        then:
        subject.kind == ERROR

        and:
        subject.message == "test"

        and:
        subject.milliSecondsSinceEpoch >= thisSecond
    }
}
