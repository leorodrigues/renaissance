package org.leonardo.renaissance.command

import org.leonardo.renaissance.utils.Optional
import spock.lang.Specification

class ResilientChainRunnerSpec extends Specification {
    def "run; ok; single command succeeds"() {
        given:
        def onlyCommand = Mock(Command)

        when:
        subject.run(onlyCommand)

        then:
        1 * singleCommandRunner.run(onlyCommand) >> true
        1 * onlyCommand.getNext() >> new Optional<>()
    }

    def "run; ok; single command fails"() {
        given:
        def onlyCommand = Mock(Command)

        when:
        subject.run(onlyCommand)

        then:
        1 * singleCommandRunner.run(onlyCommand) >> false
        1 * onlyCommand.getNext() >> new Optional<>()
    }

    def "run; ok; two commands and both are successful"() {
        given:
        def firstCommand = Mock(Command)

        and:
        def secondCommand = Mock(Command)

        when:
        subject.run(firstCommand)

        then:
        1 * singleCommandRunner.run(firstCommand) >> true
        1 * firstCommand.getNext() >> new Optional<>(secondCommand)
        1 * singleCommandRunner.run(secondCommand) >> true
        1 * secondCommand.getNext() >> new Optional<>()
    }

    def "run; ok; the first fails and the second goes on"() {
        given:
        def firstCommand = Mock(Command)

        and:
        def secondCommand = Mock(Command)

        when:
        subject.run(firstCommand)

        then:
        1 * singleCommandRunner.run(firstCommand) >> false
        1 * firstCommand.getNext() >> new Optional<>(secondCommand)
        1 * singleCommandRunner.run(secondCommand) >> true
        1 * secondCommand.getNext() >> new Optional<>()
    }

    def subject
    def singleCommandRunner

    void setup() {
        singleCommandRunner = Mock(SingleCommandRunner)
        subject = new ResilientChainRunner(singleCommandRunner)
    }
}
