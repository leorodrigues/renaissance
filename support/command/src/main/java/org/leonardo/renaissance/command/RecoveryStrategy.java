package org.leonardo.renaissance.command;

public interface RecoveryStrategy {
    void invoke(Throwable cause);
    String getSuccessMessage(Throwable cause);
    String getErrorMessage(Throwable cause, Throwable originalCause);
}
