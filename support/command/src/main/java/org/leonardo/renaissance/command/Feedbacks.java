package org.leonardo.renaissance.command;

import java.util.LinkedList;

import static org.leonardo.renaissance.command.Feedback.makeError;

/**
 * Created by leonardo on 07/02/2016.
 */
public class Feedbacks {

    private LinkedList<Feedback> feedbackList = new LinkedList<>();

    public void includeAll(Feedbacks feedbacks) {
        this.feedbackList.addAll(feedbacks.feedbackList);
    }

    public void add(Feedback feedback) {
        this.feedbackList.add(feedback);
    }

    public boolean isEmpty() {
        return feedbackList.isEmpty();
    }

    public static Feedbacks makeErrors(String... messages) {
        Feedbacks result = new Feedbacks();
        for (String message : messages)
            if (message != null)
                result.feedbackList.add(makeError(message));
        return result;
    }

    public static Feedbacks makeInstance(Feedback... feedbacks) {
        Feedbacks result = new Feedbacks();
        for (Feedback feedback : feedbacks)
            if (feedback != null)
                result.feedbackList.add(feedback);
        return result;
    }

    public void describe(FeedbackOutput feedbackOutput) {
        for (Feedback f : this.feedbackList)
            f.describe(feedbackOutput);
    }
}
