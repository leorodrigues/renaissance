package org.leonardo.renaissance.command;

import org.leonardo.renaissance.utils.Optional;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.head;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.tail;

public class CommandWeaverImpl implements CommandWeaver {

    private FeedbackListener feedbackListener;

    public CommandWeaverImpl(FeedbackListener feedbackListener) {
        this.feedbackListener = feedbackListener;
    }

    @Override
    public Optional<AbstractCommand> weave(AbstractCommand... commands) {
        return weave(asList(commands));
    }

    @Override
    public Optional<AbstractCommand> weave(
            Collection<AbstractCommand> commands) {

        AbstractCommand head = head(commands, null);
        AbstractCommand current = head;
        for (AbstractCommand c : tail(commands)) {
            current.setFeedbackListener(feedbackListener);
            current.setNext(c);
            current = c;
        }
        return new Optional<>(head);
    }
}
