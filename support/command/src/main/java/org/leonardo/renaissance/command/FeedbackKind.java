package org.leonardo.renaissance.command;

/**
 * Created by leonardo on 07/02/2016.
 */
public enum FeedbackKind {
    INFO, WARNING, ERROR
}
