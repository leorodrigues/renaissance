package org.leonardo.renaissance.command;

import org.leonardo.renaissance.utils.Optional;

import java.util.Collection;

public interface CommandWeaver {
    Optional<AbstractCommand> weave(Collection<AbstractCommand> commands);
    Optional<AbstractCommand> weave(AbstractCommand ...commands);
}
