package org.leonardo.renaissance.command;

/**
 * Created by leonardo on 07/02/2016.
 */
public final class ZeroFeedbacks extends Feedbacks {

    public static final Feedbacks INSTANCE = new ZeroFeedbacks();

    private ZeroFeedbacks() {
        super();
    }

    @Override
    public void includeAll(Feedbacks feedbacks) {
        throw new UnsupportedOperationException(
                "This implementation does not allow changes.");
    }

    @Override
    public boolean isEmpty() {
        return true;
    }
}
