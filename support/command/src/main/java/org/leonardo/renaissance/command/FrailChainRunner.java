package org.leonardo.renaissance.command;

import org.leonardo.renaissance.utils.Optional;

import java.util.ArrayList;

import static org.leonardo.renaissance.command.CompletionLevel.chooseBasedOnBoolean;

public class FrailChainRunner implements CommandChainRunner {

    private SingleCommandRunner singleRunner;

    public FrailChainRunner(SingleCommandRunner singleRunner) {
        this.singleRunner = singleRunner;
    }

    @Override
    public CompletionLevel run(Command command) {
        ArrayList<Boolean> results = new ArrayList<>();
        Optional<Command> current = new Optional<>(command);
        while (current.isPresent()) {
            Command c = current.get();
            boolean b = singleRunner.run(c);
            results.add(b);
            if (!b) break;
            current = c.getNext();
        }
        return chooseBasedOnBoolean(results);
    }
}
