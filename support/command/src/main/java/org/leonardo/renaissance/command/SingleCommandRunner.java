package org.leonardo.renaissance.command;

public interface SingleCommandRunner {
    boolean run(Command command);
}
