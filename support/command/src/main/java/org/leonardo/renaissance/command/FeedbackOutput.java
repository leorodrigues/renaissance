package org.leonardo.renaissance.command;

public interface FeedbackOutput {
    void write(long milliSecondsSinceEpoch, FeedbackKind kind, String message);
}
