package org.leonardo.renaissance.command;

import org.leonardo.renaissance.utils.Optional;

import java.util.ArrayList;

import static org.leonardo.renaissance.command.CompletionLevel.chooseBasedOnBoolean;

public class ResilientChainRunner implements CommandChainRunner {

    private SingleCommandRunner singleRunner;

    public ResilientChainRunner(SingleCommandRunner singleRunner) {
        this.singleRunner = singleRunner;
    }

    @Override
    public CompletionLevel run(Command command) {
        ArrayList<Boolean> results = new ArrayList<>();
        Optional<Command> current = new Optional<>(command);
        while (current.isPresent())
            current = run(current, results);
        return chooseBasedOnBoolean(results);
    }

    private Optional<Command> run(
            Optional<Command> current, ArrayList<Boolean> results) {
        Command command = current.get();
        results.add(singleRunner.run(command));
        return command.getNext();
    }
}
