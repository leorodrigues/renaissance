package org.leonardo.renaissance.command;

import java.util.Calendar;

import static java.lang.String.format;
import static org.leonardo.renaissance.command.FeedbackKind.ERROR;
import static org.leonardo.renaissance.command.FeedbackKind.INFO;
import static org.leonardo.renaissance.command.FeedbackKind.WARNING;

/**
 * Created by leonardo on 07/02/2016.
 */
public class Feedback {

    private final String DISPLAY_PATTERN = "%s: %s";

    private final long milliSecondsSinceEpoch;
    private final FeedbackKind kind;
    private final String message;

    public long getMilliSecondsSinceEpoch() {
        return milliSecondsSinceEpoch;
    }

    public FeedbackKind getKind() {
        return kind;
    }

    public String getMessage() {
        return message;
    }

    private Feedback(
            long milliSecondsSinceEpoch, FeedbackKind kind, String message) {
        this.milliSecondsSinceEpoch = milliSecondsSinceEpoch;
        this.kind = kind;
        this.message = message;
    }

    public static Feedback makeInfo(String message) {
        long now = Calendar.getInstance().getTimeInMillis();
        return new Feedback(now, INFO, message);
    }

    public static Feedback makeInfo(String template, Object ...arguments) {
        long now = Calendar.getInstance().getTimeInMillis();
        return new Feedback(now, INFO, format(template, arguments));
    }

    public static Feedback makeWarning(String message) {
        long now = Calendar.getInstance().getTimeInMillis();
        return new Feedback(now, WARNING, message);
    }

    public static Feedback makeWarning(String template, Object ...arguments) {
        long now = Calendar.getInstance().getTimeInMillis();
        return new Feedback(now, WARNING, format(template, arguments));
    }

    public static Feedback makeError(String message) {
        long now = Calendar.getInstance().getTimeInMillis();
        return new Feedback(now, ERROR, message);
    }

    public static Feedback makeError(String template, Object ...arguments) {
        long now = Calendar.getInstance().getTimeInMillis();
        return new Feedback(now, ERROR, format(template, arguments));
    }

    public String getDisplayString() {
        return format(DISPLAY_PATTERN, kind, message);
    }

    public void describe(FeedbackOutput feedbackOutput) {
        feedbackOutput.write(
            getMilliSecondsSinceEpoch(), getKind(), getMessage());
    }
}
