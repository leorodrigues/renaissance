package org.leonardo.renaissance.command;

import org.leonardo.renaissance.utils.Optional;

import static org.leonardo.renaissance.command.Feedback.makeError;
import static org.leonardo.renaissance.command.Feedback.makeInfo;

public class RunWithRecovery implements SingleCommandRunner {

    private FeedbackListener feedbackListener;

    public RunWithRecovery() {
        this(NullFeedbackListener.INSTANCE);
    }

    public RunWithRecovery(FeedbackListener feedbackListener) {
        this.feedbackListener = feedbackListener;
    }

    @Override
    public boolean run(Command command) {
        try {
            command.execute();
            reportSuccess(command);
            return true;
        } catch (Throwable t) {
            reportFailure(command, t);
            tryRecovering(command, t);
            return false;
        }
    }

    private void tryRecovering(Command command, Throwable firstCause) {
        Optional<RecoveryStrategy> result =
                command.getRecoveryStrategy(firstCause);

        if (!result.isPresent()) return;

        RecoveryStrategy recoveryStrategy = result.get();

        try {
            recoveryStrategy.invoke(firstCause);
            reportSuccess(recoveryStrategy, firstCause);
        } catch (Throwable cause) {
            reportFailure(recoveryStrategy, cause, firstCause);
        }
    }

    private void reportSuccess(Command command) {
        String message = command.getSuccessMessage();
        feedbackListener.register(makeInfo(message));
    }

    private void reportSuccess(RecoveryStrategy command, Throwable cause) {
        String message = command.getSuccessMessage(cause);
        feedbackListener.register(makeInfo(message));
    }

    private void reportFailure(Command command, Throwable cause) {
        String message = command.getErrorMessage(cause);
        feedbackListener.register(makeError(message));
    }

    private void reportFailure(
            RecoveryStrategy command, Throwable cause, Throwable firstCause) {
        String message = command.getErrorMessage(cause, firstCause);
        feedbackListener.register(makeError(message));
    }
}
