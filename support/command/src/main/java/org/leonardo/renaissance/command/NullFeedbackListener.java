package org.leonardo.renaissance.command;

/**
 * Created by leonardo on 17/03/2016.
 */
public final class NullFeedbackListener implements FeedbackListener {

    public static final FeedbackListener INSTANCE =
            new NullFeedbackListener();

    @Override
    public void register(Feedback feedback) {

    }
}
