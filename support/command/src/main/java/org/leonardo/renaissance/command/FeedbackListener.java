package org.leonardo.renaissance.command;

/**
 * Created by leonardo on 17/03/2016.
 */
public interface FeedbackListener {
    void register(Feedback feedback);
}
