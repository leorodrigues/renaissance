package org.leonardo.renaissance.command;

import org.leonardo.renaissance.utils.Optional;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.join;
import static org.leonardo.renaissance.command.Feedback.makeInfo;
import static org.leonardo.renaissance.command.Feedback.makeWarning;

public abstract class AbstractCommand implements Command {

    private static final String ERROR_TEMPLATE =
        "%s - terminated - failure - %s";

    private static final String SUCCESS_TEMPLATE =
        "%s - terminated - success";

    private static final String NEUTRAL_TEMPLATE =
        "%s - %s";

    private Command next;

    private Optional<FeedbackListener> feedbackListener = new Optional<>();

    @Override
    public Optional<RecoveryStrategy> getRecoveryStrategy(Throwable cause) {
        return new Optional<>();
    }

    @Override
    public Optional<Command> getNext() {
        return new Optional<>(next);
    }

    public AbstractCommand setNext(Command next) {
        this.next = next;
        return this;
    }

    @Override
    public String getSuccessMessage() {
        return format(SUCCESS_TEMPLATE, getClass().getSimpleName());
    }

    @Override
    public String getErrorMessage(Throwable error) {
        return format(ERROR_TEMPLATE, getClass().getSimpleName(),
                error.getMessage());
    }

    public void setFeedbackListener(FeedbackListener feedbackListener) {
        this.feedbackListener = new Optional<>(feedbackListener);
    }

    protected void reportInfo(String ...messageItems) {
        if (feedbackListener.isPresent())
            feedbackListener.get().register(makeInfo(
                    applyTemplate(messageItems)));
    }

    protected void reportWarning(String ...messageItems) {
        if (feedbackListener.isPresent())
            feedbackListener.get().register(makeWarning(
                    applyTemplate(messageItems)));
    }

    private String applyTemplate(String[] messageItems) {
        return format(NEUTRAL_TEMPLATE, getClass().getSimpleName(),
            join(asList(messageItems), " - "));
    }
}
