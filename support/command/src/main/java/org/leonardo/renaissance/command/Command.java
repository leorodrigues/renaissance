package org.leonardo.renaissance.command;

import org.leonardo.renaissance.utils.Optional;

public interface Command {
    void execute();
    Optional<Command> getNext();
    String getSuccessMessage();
    String getErrorMessage(Throwable error);
    Optional<RecoveryStrategy> getRecoveryStrategy(Throwable cause);
}
