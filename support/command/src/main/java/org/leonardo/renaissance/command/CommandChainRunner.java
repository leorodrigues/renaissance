package org.leonardo.renaissance.command;

public interface CommandChainRunner {
    CompletionLevel run(Command command);
}
