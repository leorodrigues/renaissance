package org.leonardo.renaissance.command;

import java.util.ArrayList;
import java.util.Collection;

import static java.util.Arrays.asList;
import static org.leonardo.renaissance.command.CompletionLevel.chooseBasedOnLevel;

public class SerialCommandRunner {

    private CommandChainRunner commandChainRunner;

    public SerialCommandRunner(CommandChainRunner commandChainRunner) {
        this.commandChainRunner = commandChainRunner;
    }

    public void setCommandChainRunner(CommandChainRunner commandChainRunner) {
        this.commandChainRunner = commandChainRunner;
    }

    public CompletionLevel run(Command ...commands) {
        return run(asList(commands));
    }

    public CompletionLevel run(Collection<Command> commands) {
        ArrayList<CompletionLevel> results = new ArrayList<>();

        if (commandChainRunner == null)
            throw new IllegalStateException("Command chain runner is missing.");

        for (Command c : commands)
            results.add(commandChainRunner.run(c));

        return chooseBasedOnLevel(results);
    }
}
