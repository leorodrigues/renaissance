package org.leonardo.renaissance.command;

import java.util.Collection;

import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.all;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.none;

public enum CompletionLevel {
    NONE, PARTIAL, FULL;

    public static CompletionLevel chooseBasedOnBoolean(
            Collection<Boolean> elements) {
        if (none(elements))
            return NONE;
        if (all(elements))
            return FULL;
        return PARTIAL;
    }

    public static CompletionLevel chooseBasedOnLevel(
            Collection<CompletionLevel> elements) {
        int full = 0;
        int none = 0;
        for (CompletionLevel c : elements) {
            switch (c) {
                case FULL: full++; break;
                case NONE: none++; break;
            }
        }
        if (full == elements.size())
            return FULL;
        if (none == elements.size())
            return NONE;
        return PARTIAL;
    }
}
