package org.leonardo.renaissance.io

import spock.lang.Specification

class TryClosingOrFailSpec extends Specification {
    def "invoke; ok; closeable is null"() {
        expect:
        subject.invoke(null)
    }

    def "invoke; ok; closeable closes without problem"() {
        when:
        subject.invoke(closeable)

        then:
        1 * closeable.close()
    }

    def "invoke; error; closeable throws exception"() {
        when:
        subject.invoke(closeable)

        then:
        thrown(CloseAttemptFailedException)
        1 * closeable.close() >> { throw new IOException('thrown on purpose') }
    }

    def subject
    def closeable

    void setup() {
        closeable = Mock(Closeable)
        subject = TryClosingOrFail.INSTANCE
    }
}
