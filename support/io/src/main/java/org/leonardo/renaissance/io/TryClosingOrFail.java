package org.leonardo.renaissance.io;

import java.io.Closeable;

import static java.lang.String.format;

public class TryClosingOrFail implements TerminateCloseable {

    public static final TryClosingOrFail INSTANCE = new TryClosingOrFail();

    private TryClosingOrFail() {
    }

    @Override
    public void invoke(Closeable closeable) {
        if (closeable == null)
            return;
        try {
            closeable.close();
        } catch (Throwable e) {
            throw new CloseAttemptFailedException(makeMessage(e), e);
        }
    }

    private String makeMessage(Throwable e) {
        return format("Unable to close object: %s", e.getMessage());
    }
}
