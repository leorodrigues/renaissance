package org.leonardo.renaissance.io;

public class CloseAttemptFailedException extends RuntimeException {
    public CloseAttemptFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
