package org.leonardo.renaissance.io;

import java.io.Closeable;

public interface TerminateCloseable {
    void invoke(Closeable closeable);
}
