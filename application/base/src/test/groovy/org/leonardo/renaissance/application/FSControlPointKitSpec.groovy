package org.leonardo.renaissance.application

import org.leonardo.renaissance.application.lifecycle.ControlPointShutdownProcedure
import org.leonardo.renaissance.application.lifecycle.SetupAssembler
import org.leonardo.renaissance.application.lifecycle.StartupAssembler
import org.leonardo.renaissance.command.SerialCommandRunner
import spock.lang.Specification

import java.util.logging.Logger

class FSControlPointKitSpec extends Specification {
    def "single test; make everything"() {
        when:
        def f = subject.makeFeedbackListener(Logger.getAnonymousLogger())
        def r = subject.makeSerialCommandRunner(f)

        then:
        f instanceof LoggerFeedbackListener
        r instanceof SerialCommandRunner
        subject.makeSetupAssembler(f, r) instanceof SetupAssembler
        subject.makeStartupAssembler(f, r) instanceof StartupAssembler
        subject.makeShutdownProcedure(f, r) instanceof ControlPointShutdownProcedure
    }

    def subject

    void setup() {
        subject = new FSControlPointKit("base", "file", "home")
    }
}
