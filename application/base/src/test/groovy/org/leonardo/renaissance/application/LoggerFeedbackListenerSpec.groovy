package org.leonardo.renaissance.application

import org.leonardo.renaissance.command.Feedback
import spock.lang.Specification

import java.util.logging.Logger

import static org.leonardo.renaissance.command.Feedback.makeError
import static org.leonardo.renaissance.command.Feedback.makeInfo
import static org.leonardo.renaissance.command.Feedback.makeWarning

class LoggerFeedbackListenerSpec extends Specification {

    def "register; ok; info"() {
        when:
        subject.register(makeInfo('x'))

        then:
        logger.info('x')
    }

    def "register; ok; warning"() {
        when:
        subject.register(makeWarning('y'))

        then:
        logger.warning('y')
    }

    def "register; ok; error"() {
        when:
        subject.register(makeError('z'))

        then:
        logger.severe('z')
    }

    def subject
    def logger

    void setup() {
        logger = Mock(Logger)
        subject = new LoggerFeedbackListener(logger)
    }
}
