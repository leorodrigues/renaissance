package org.leonardo.renaissance.application.operations

import org.leonardo.environment.BasicUnitName
import org.leonardo.environment.ControlPoint
import org.leonardo.environment.config.NameAlreadyDeclaredException
import org.leonardo.environment.UnitName
import org.leonardo.renaissance.application.lifecycle.ControlPointLifeCycleException
import org.leonardo.renaissance.utils.Optional
import spock.lang.Specification

class InitializeControlPointStateSpec extends Specification {
    def "execute; ok"() {
        when:
        subject.execute()

        then:
        1 * fabricationContext.getControlPoint() >> new Optional<>(controlPoint)
        1 * controlPoint.declareName(BasicUnitName.INSTANCE)
        1 * controlPoint.saveState()
    }

    def "execute; error; control point is missing"() {
        when:
        subject.execute()

        then:
        thrown(MissingControlPointException)
        1 * fabricationContext.getControlPoint() >> new Optional<>()
        0 * controlPoint.declareName(_)
        0 * controlPoint.saveState()
    }

    def "execute; error; basic unit is already declared"() {
        given:
        def e = new NameAlreadyDeclaredException(Mock(UnitName))

        when:
        subject.execute()

        then:
        thrown(ControlPointInitException)
        1 * fabricationContext.getControlPoint() >> new Optional<>(controlPoint)
        1 * controlPoint.declareName(BasicUnitName.INSTANCE) >> { throw e }
        0 * controlPoint.saveState()
    }

    def subject
    def controlPoint
    def fabricationContext

    void setup() {
        fabricationContext = Spy(FabricationContext)
        controlPoint = Mock(ControlPoint)
        subject = new InitializeControlPointState(fabricationContext)
    }
}
