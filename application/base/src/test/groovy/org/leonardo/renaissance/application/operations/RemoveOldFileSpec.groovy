package org.leonardo.renaissance.application.operations

import org.leonardo.renaissance.application.FileSystemLayer
import org.leonardo.renaissance.application.operations.RemoveOldFile
import org.leonardo.renaissance.application.operations.UnsuitablePathException
import spock.lang.Specification

import static java.nio.file.Files.createTempFile

class RemoveOldFileSpec extends Specification {
    def "execute; ok; file is not there"() {
        when:
        subject.execute()

        then:
        1 * fileSystemLayer.fileExists(_) >> false
        0 * fileSystemLayer.deleteFile(_) >> false
    }

    def "execute; ok; file is there but is deleted"() {
        when:
        subject.execute()

        then:
        1 * fileSystemLayer.fileExists(_) >> true
        1 * fileSystemLayer.deleteFile(_) >> true
    }

    def "execute; error; file not removed"() {
        when:
        subject.execute()

        then:
        thrown(UnsuitablePathException)
        1 * fileSystemLayer.fileExists(_) >> true
        1 * fileSystemLayer.deleteFile(_) >> false
    }

    def fileSystemLayer
    def subject

    void setup() {
        def prefix = "test-run-"
        def suffix = ".tmp"
        def path = createTempFile(prefix, suffix).toFile().absolutePath
        fileSystemLayer = Mock(FileSystemLayer)
        subject = new RemoveOldFile(path, fileSystemLayer)
    }
}
