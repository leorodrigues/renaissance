package org.leonardo.renaissance.application.operations

import org.leonardo.renaissance.application.FileSystemLayer
import org.leonardo.renaissance.application.operations.ConfirmHomePathExistence
import org.leonardo.renaissance.application.operations.UnsuitablePathException
import spock.lang.Specification

import static java.nio.file.Files.createTempDirectory

class ConfirmHomePathExistenceSpec extends Specification {
    def "execute; ok; home path exists"() {
        when:
        subject.execute()

        then:
        1 * fileSystemLayer.directoryExists(_) >> true
    }

    def "execute; fail; home path does not exist"() {
        when:
        subject.execute()

        then:
        thrown(UnsuitablePathException)
        1 * fileSystemLayer.directoryExists(_) >> false
    }

    def fileSystemLayer
    def homePath
    def subject

    void setup() {
        def pathPrefix = "test-run-"
        homePath = createTempDirectory(pathPrefix).toFile().absolutePath
        fileSystemLayer = Mock(FileSystemLayer)
        subject = new ConfirmHomePathExistence(homePath, fileSystemLayer)
    }
}
