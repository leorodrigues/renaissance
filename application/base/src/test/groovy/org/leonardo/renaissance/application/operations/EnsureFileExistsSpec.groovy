package org.leonardo.renaissance.application.operations

import org.leonardo.renaissance.application.FileSystemLayer
import org.leonardo.renaissance.application.operations.EnsureFileExists
import org.leonardo.renaissance.application.operations.UnsuitablePathException
import spock.lang.Specification

import java.nio.file.Files

class EnsureFileExistsSpec extends Specification {
    def "execute; ok; file exists"() {
        when:
        subject.execute()

        then:
        1 * fileSystemLayer.fileExists(_) >> true
    }

    def "execute; error; file is not there"() {
        when:
        subject.execute()

        then:
        thrown(UnsuitablePathException)
        1 * fileSystemLayer.fileExists(_) >> false
    }

    def subject
    def fileSystemLayer

    void setup() {
        def pathPrefix = "test-run-"
        def pathSuffix = ".tmp"
        String path = Files.createTempFile(
                pathPrefix, pathSuffix).toFile().absolutePath

        fileSystemLayer = Mock(FileSystemLayer)
        subject = new EnsureFileExists(path, fileSystemLayer)
    }
}
