package org.leonardo.renaissance.application.lifecycle

import org.leonardo.environment.ControlPoint
import org.leonardo.renaissance.application.operations.FabricationContext
import org.leonardo.renaissance.command.SerialCommandRunner
import org.leonardo.renaissance.utils.Optional
import spock.lang.Specification
import spock.lang.Unroll

import static org.leonardo.renaissance.command.CompletionLevel.FULL
import static org.leonardo.renaissance.command.CompletionLevel.NONE
import static org.leonardo.renaissance.command.CompletionLevel.PARTIAL

class ControlPointLoadingProcedureSpec extends Specification {
    def "execute; ok"() {
        when:
        def result = subject.execute()

        then:
        result.is(controlPoint)
        1 * serialCommandSequence.run(_) >> FULL
        1 * fabricationContext.getControlPoint() >> new Optional<>(controlPoint)
    }

    def "execute; error; if control point is missing"() {
        when:
        subject.execute()

        then:
        thrown(ControlPointLifeCycleException)
        1 * serialCommandSequence.run(_) >> FULL
        1 * fabricationContext.getControlPoint() >> new Optional<>()
    }

    @Unroll
    def "execute; error; if completion level is #level"() {
        when:
        subject.execute()

        then:
        thrown(ControlPointLifeCycleException)
        1 * serialCommandSequence.run(_) >> level
        0 * fabricationContext.getControlPoint() >> new Optional<>()

        where:
        level   | x
        PARTIAL | ''
        NONE    | ''
    }

    def subject
    def controlPoint
    def fabricationContext
    def serialCommandSequence

    void setup() {
        serialCommandSequence = Spy(SerialCommandRunner)
        fabricationContext = Mock(FabricationContext)
        controlPoint = Mock(ControlPoint)
        subject = new ControlPointLoadingProcedure(
                fabricationContext, serialCommandSequence)
    }
}
