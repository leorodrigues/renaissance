package org.leonardo.renaissance.application

import org.leonardo.environment.ControlPoint
import org.leonardo.environment.UnitName
import org.leonardo.renaissance.application.lifecycle.ControlPointLifeCycleException
import org.leonardo.renaissance.application.lifecycle.ControlPointShutdownProcedure
import org.leonardo.renaissance.command.AbstractCommand
import org.leonardo.renaissance.command.CommandChainRunner
import org.leonardo.renaissance.command.CommandWeaver
import org.leonardo.renaissance.command.SerialCommandRunner
import org.leonardo.renaissance.utils.Optional
import spock.lang.Specification

import static org.leonardo.renaissance.command.CompletionLevel.*

class ControlPointShutdownProcedureSpec extends Specification {

    def "execute; ok; no units running at the moment"() {
        when:
        subject.execute(controlPoint, false)

        then:
        1 * controlPoint.getRunningUnitsNames() >> []
        1 * commandWeaver.weave(_) >> new Optional<>()
    }

    def "execute; ok; no units running and the saveState is on"() {
        given:
        def someOtherCommand = Mock(AbstractCommand)

        when:
        subject.execute(controlPoint, true)

        then:
        1 * controlPoint.getRunningUnitsNames() >> []
        1 * commandWeaver.weave(_) >> new Optional<>(someOtherCommand)
        1 * serialCommandRunner.run(someOtherCommand) >> FULL
    }

    def "execute; ok; one unit running and the saveState is off"() {
        given:
        def someOtherCommand = Mock(AbstractCommand)

        when:
        subject.execute(controlPoint, false)

        then:
        1 * controlPoint.getRunningUnitsNames() >> [Mock(UnitName)]
        1 * commandWeaver.weave(_) >> new Optional<>(someOtherCommand)
        1 * serialCommandRunner.run(someOtherCommand) >> FULL
    }

    def "execute; ok; one unit running and the saveState is on"() {
        given:
        def someOtherCommand = Mock(AbstractCommand)

        when:
        subject.execute(controlPoint, true)

        then:
        1 * controlPoint.getRunningUnitsNames() >> [Mock(UnitName)]
        1 * commandWeaver.weave(_) >> new Optional<>(someOtherCommand)
        1 * serialCommandRunner.run(someOtherCommand) >> FULL
    }

    def "execute; error; one unit running and the outcome if PARTIAL"() {
        given:
        def someOtherCommand = Mock(AbstractCommand)

        when:
        subject.execute(controlPoint, false)

        then:
        thrown(ControlPointLifeCycleException)
        1 * controlPoint.getRunningUnitsNames() >> [Mock(UnitName)]
        1 * commandWeaver.weave(_) >> new Optional<>(someOtherCommand)
        1 * serialCommandRunner.run(someOtherCommand) >> PARTIAL
    }

    def "execute; error; one unit running and the outcome if NONE"() {
        given:
        def someOtherCommand = Mock(AbstractCommand)

        when:
        subject.execute(controlPoint, false)

        then:
        thrown(ControlPointLifeCycleException)
        1 * controlPoint.getRunningUnitsNames() >> [Mock(UnitName)]
        1 * commandWeaver.weave(_) >> new Optional<>(someOtherCommand)
        1 * serialCommandRunner.run(someOtherCommand) >> NONE
    }

    def subject
    def controlPoint
    def commandWeaver
    def commandChainRunner
    def serialCommandRunner

    void setup() {
        controlPoint = Mock(ControlPoint)
        commandWeaver = Mock(CommandWeaver)
        commandChainRunner = Mock(CommandChainRunner)
        serialCommandRunner = Spy(SerialCommandRunner)
        subject = new ControlPointShutdownProcedure(
            serialCommandRunner, commandWeaver)
    }
}
