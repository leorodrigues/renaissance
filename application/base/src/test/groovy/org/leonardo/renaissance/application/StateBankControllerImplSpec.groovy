package org.leonardo.renaissance.application

import org.leonardo.environment.ControlPointStateBank
import org.leonardo.environment.config.ConfigurationIndex
import org.leonardo.environment.config.Configurations
import org.leonardo.environment.config.ConfigIndexSync
import org.leonardo.environment.config.StateBankControllerImpl
import org.leonardo.environment.config.ConfigBankSync
import spock.lang.Specification

class StateBankControllerImplSpec extends Specification {

    def "save; ok; orchestrates call"() {
        given:
        def configurations = Spy(Configurations)

        when:
        subject.load()

        then:
        1 * controlPointStateBank.allConfigurationsByName() >> configurations
        1 * configIndexSync.invoke(configurationIndex, configurations)
    }

    def "load; ok; orchestrates call"() {
        when:
        subject.save()

        then:
        1 * configBankSync.invoke(controlPointStateBank, configurationIndex)
    }

    def controlPointStateBank
    def configurationIndex
    def configIndexSync
    def configBankSync
    def subject

    void setup() {
        configBankSync = Mock(ConfigBankSync)
        configIndexSync = Mock(ConfigIndexSync)
        configurationIndex = Mock(ConfigurationIndex)
        controlPointStateBank = Mock(ControlPointStateBank)

        subject = new StateBankControllerImpl(
                controlPointStateBank,
                configurationIndex,
                configBankSync,
                configIndexSync)
    }
}
