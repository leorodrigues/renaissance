package org.leonardo.renaissance.application.operations

import org.leonardo.environment.ControlPoint
import spock.lang.Specification

class SaveControlPointStateSpec extends Specification {
    def "execute; ok"() {
        when:
        subject.execute()

        then:
        1 * controlPoint.saveState()
    }

    def subject
    def controlPoint

    void setup() {
        controlPoint = Mock(ControlPoint)
        subject = new SaveControlPointState(controlPoint)
    }
}
