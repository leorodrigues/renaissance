package org.leonardo.renaissance.application.lifecycle

import org.leonardo.environment.ControlPointFactory
import org.leonardo.renaissance.application.FileSystemLayer
import org.leonardo.renaissance.application.operations.PathComputation
import org.leonardo.renaissance.command.AbstractCommand
import org.leonardo.renaissance.command.CommandWeaver
import org.leonardo.renaissance.command.SerialCommandRunner
import org.leonardo.renaissance.utils.Optional
import spock.lang.Specification

class SetupAssemblerSpec extends Specification {

    def "assemble; ok; does everything"() {
        when:
        subject.assemble()

        then:
        1 * pathComputation.invoke('x', 'z') >> 'k'
        1 * pathComputation.invoke('k', 'y') >> 'm'
        1 * subject.addPathSetup(*_)
        1 * subject.addUnderlyingRepositorySetup(*_)
        1 * subject.addPostAssemblySetup(*_)
        1 * commandWeaver.weave(_) >> new Optional<>(universalCommand)
    }

    def subject
    def commandWeaver
    def pathComputation
    def fileSystemLayer
    def universalCommand
    def controlPointFactory
    def serialCommandRunner

    void setup() {
        controlPointFactory = Mock(ControlPointFactory)
        serialCommandRunner = Mock(SerialCommandRunner)
        universalCommand = Mock(AbstractCommand)
        fileSystemLayer = Mock(FileSystemLayer)
        pathComputation = Spy(PathComputation)
        commandWeaver = Mock(CommandWeaver)

        subject = Spy(GuineaPig, constructorArgs: [
            'x', 'y', 'z', commandWeaver, fileSystemLayer, pathComputation,
                controlPointFactory, serialCommandRunner])
    }

    static class GuineaPig extends SetupAssembler {

        GuineaPig(String basePath,
                  String stateFileName,
                  String homeDirectoryName,
                  CommandWeaver commandWeaver,
                  FileSystemLayer fileSystemLayer,
                  PathComputation pathComputation,
                  ControlPointFactory controlPointFactory,
                  SerialCommandRunner serialCommandRunner) {

            super(basePath, stateFileName, homeDirectoryName, commandWeaver,
                pathComputation, fileSystemLayer, controlPointFactory,
                    serialCommandRunner)
        }
    }
}