package org.leonardo.renaissance.application.operations

import org.leonardo.environment.ControlPoint
import org.leonardo.renaissance.utils.Optional
import spock.lang.Specification

class LoadControlPointStateSpec extends Specification {
    def "execute; ok"() {
        when:
        subject.execute()

        then:
        1 * fabricationContext.getControlPoint() >> new Optional<>(controlPoint)
        1 * controlPoint.loadState()
    }

    def "execute; error; control point is missing"() {
        when:
        subject.execute()

        then:
        thrown(MissingControlPointException)
        1 * fabricationContext.getControlPoint() >> new Optional<>()
        0 * controlPoint.loadState()
    }

    def subject
    def controlPoint
    def fabricationContext

    void setup() {
        fabricationContext = Spy(FabricationContext)
        controlPoint = Mock(ControlPoint)
        subject = new LoadControlPointState(fabricationContext)
    }
}
