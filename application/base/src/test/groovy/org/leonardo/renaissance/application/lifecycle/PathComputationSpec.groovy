package org.leonardo.renaissance.application.lifecycle

import org.leonardo.renaissance.application.operations.PathComputation
import spock.lang.Specification

class PathComputationSpec extends Specification {
    def "invoke"() {
        expect:
        new PathComputation().invoke("a", "b") =~ "a.b"
    }
}
