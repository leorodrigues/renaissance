package org.leonardo.renaissance.application

import org.leonardo.davinci.durability.Bucket
import org.leonardo.davinci.durability.DurableRepository
import org.leonardo.environment.UnitName
import org.leonardo.environment.config.ConfigEntry
import org.leonardo.environment.config.Configuration
import org.leonardo.renaissance.application.operations.StateBankInsertException
import org.leonardo.renaissance.application.operations.UnderlyingBankException
import org.leonardo.renaissance.utils.*
import spock.lang.Specification

import static org.leonardo.renaissance.application.ControlPointStateBankImpl.*

class ControlPointStateBankImplSpec extends Specification {

    def "allConfigurationsByName; ok; empty config if but is absent"() {
        when:
        def result = subject.allConfigurationsByName()

        then:
        result.size() == 0
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.all() >> new OptionalList<Record>()
    }

    def "allConfigurationsByName; ok; some config if bucket is there"() {
        given:
        def someConfig = Spy(Configuration)

        and:
        def record = new Record('x', Mock(UnitName), someConfig)

        when:
        def result = subject.allConfigurationsByName()

        then:
        result.values()[0].is(someConfig)
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.all() >> new OptionalList<Record>([record])
    }

    def "allUnitNames; ok; empty if bucket is not there"() {
        when:
        def result = subject.allUnitNames()

        then:
        result.size() == 0
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.all() >> new OptionalList<Record>()
    }

    def "allUnitNames; ok; some config exists in the bucket"() {
        given:
        def someConfig = Spy(Configuration)

        and:
        def name = Mock(UnitName)

        and:
        def record = new Record('x', name, someConfig)

        when:
        def result = subject.allUnitNames()

        then:
        result[0].is(name)
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.all() >> new OptionalList<Record>([record])
    }

    def "insert; ok"() {
        given:
        def name = Mock(UnitName)
        def conf = Mock(Configuration)

        when:
        subject.insert(name, conf)

        then:
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.make(Record.class, name, { c ->
            c instanceof Configuration && !c.is(conf)
        }) >> { }
        1 * conf.getEntries() >> [new ConfigEntry('x', 'y')]
    }

    def "insert; error"() {
        given:
        def name = Mock(UnitName)
        def conf = Mock(Configuration)

        when:
        subject.insert(name, conf)

        then:
        thrown(StateBankInsertException)
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.make(Record.class, name, { c ->
            c instanceof Configuration && !c.is(conf)
        }) >> {
            throw new RuntimeException('thrown on purpose')
        }
        1 * conf.getEntries() >> [new ConfigEntry('x', 'y')]
    }

    def "delete; ok"() {
        given:
        def name = Mock(UnitName)

        when:
        subject.delete(name)

        then:
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.applyQuery(_) >> new Optional<Record>()
    }

    def "delete; error"() {
        given:
        def name = Mock(UnitName)
        def conf = Mock(Configuration)
        def record = new Record('x', name, conf)

        when:
        subject.delete(name)

        then:
        1 * durableRepository.getBucket(BUCKET_NAME) >> bucket
        1 * bucket.applyQuery({ p ->
            p.verifies(record)
        }) >> new Optional<Record>(record)
        1 * bucket.remove(record)
    }

    def "commit; ok"() {
        when:
        subject.commit()

        then:
        1 * durableRepository.save()
    }

    def "commit; error"() {
        when:
        subject.commit()

        then:
        thrown(UnderlyingBankException)
        1 * durableRepository.save() >> {
            throw new IOException('thrown on purpose')
        }
    }

    def subject
    def durableRepository
    def bucket

    void setup() {
        bucket = Mock(Bucket)
        durableRepository = Mock(DurableRepository)
        subject = new ControlPointStateBankImpl(durableRepository)
    }
}
