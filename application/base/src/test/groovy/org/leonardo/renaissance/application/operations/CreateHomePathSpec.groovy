package org.leonardo.renaissance.application.operations

import org.leonardo.renaissance.application.FileSystemLayer
import org.leonardo.renaissance.application.operations.CreateHomePath
import org.leonardo.renaissance.application.operations.UnsuitablePathException
import spock.lang.Specification

import static java.nio.file.Files.createTempDirectory

class CreateHomePathSpec extends Specification {
    def "execute; ok; directory is already there"() {
        when:
        subject.execute()

        then:
        1 * fileSystemLayer.directoryExists(_) >> true
        0 * fileSystemLayer.makeDirectories(_) >> false
    }

    def "execute; ok; directory not there but is created"() {
        when:
        subject.execute()

        then:
        1 * fileSystemLayer.directoryExists(_) >> false
        1 * fileSystemLayer.makeDirectories(_) >> true
    }

    def "execute; error; directory not there and cannot be created"() {
        when:
        subject.execute()

        then:
        thrown(UnsuitablePathException)
        1 * fileSystemLayer.directoryExists(_) >> false
        1 * fileSystemLayer.makeDirectories(_) >> false
    }

    def homePath
    def subject
    def fileSystemLayer

    void setup() {
        def pathPrefix = "test-run-"
        homePath = createTempDirectory(pathPrefix).toFile().absolutePath
        fileSystemLayer = Mock(FileSystemLayer)
        subject = new CreateHomePath(homePath, fileSystemLayer)
    }
}
