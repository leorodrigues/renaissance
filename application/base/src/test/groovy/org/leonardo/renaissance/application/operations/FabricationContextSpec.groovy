package org.leonardo.renaissance.application.operations

import org.leonardo.davinci.durability.DurableRepository
import org.leonardo.environment.ControlPoint
import org.leonardo.environment.ControlPointStateBank
import org.leonardo.renaissance.application.operations.FabricationContext
import spock.lang.Specification

class FabricationContextSpec extends Specification {
    def "get/set values"() {
        given:
        def subject = new FabricationContext()

        and:
        subject.controlPoint = Mock(ControlPoint)
        subject.durableRepository = Mock(DurableRepository)
        subject.controlPointStateBank = Mock(ControlPointStateBank)

        expect:
        subject.controlPoint.present
        subject.controlPointStateBank != null
        subject.durableRepository != null
    }
}
