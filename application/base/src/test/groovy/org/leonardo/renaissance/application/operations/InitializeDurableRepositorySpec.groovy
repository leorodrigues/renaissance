package org.leonardo.renaissance.application.operations

import org.leonardo.davinci.durability.DurableRepository
import org.leonardo.renaissance.application.operations.FabricationContext
import org.leonardo.renaissance.application.operations.InitializeDurableRepository
import org.leonardo.renaissance.application.operations.RepositoryInitializationException
import spock.lang.Specification

class InitializeDurableRepositorySpec extends Specification {

    def "execute; ok; everything runs according to plan"() {
        when:
        subject.execute()

        then:
        1 * fabricationContext.getDurableRepository() >> durableRepository
        1 * durableRepository.newBucket(
            { n -> n != null }, { k -> k != null })
        1 * durableRepository.save()
    }

    def "execute; error; repository throws on save"() {
        given:
        def error = new IOException('thrown on purpose')

        when:
        subject.execute()

        then:
        thrown(RepositoryInitializationException)
        1 * fabricationContext.getDurableRepository() >> durableRepository
        1 * durableRepository.newBucket(
                { n -> n != null }, { k -> k != null })
        1 * durableRepository.save() >> { throw error }
    }

    def subject
    def fabricationContext
    def durableRepository

    void setup() {
        durableRepository = Mock(DurableRepository)
        fabricationContext = Spy(FabricationContext)
        subject = new InitializeDurableRepository(fabricationContext)
    }
}
