package org.leonardo.renaissance.application.operations

import org.leonardo.environment.ControlPoint
import org.leonardo.environment.UnitName
import spock.lang.Specification

class ShutdownUnitSpec extends Specification {

    def "execute; ok; ask the control point to shutdown a single unit"() {
        when:
        subject.execute()

        then:
        1 * controlPoint.shutdownUnit(unitName)
    }

    def "getSuccessMessage; ok"() {
        when:
        def msg = subject.getSuccessMessage()

        then:
        1 * unitName.getDisplayString() >> 'x'
        msg == 'ShutdownUnit - terminated - success - x'
    }

    def "getErrorMessage; ok"() {
        given:
        def e = new RuntimeException('thrown on purpose')

        when:
        def msg = subject.getErrorMessage(e)

        then:
        1 * unitName.getDisplayString() >> 'x'
        msg == 'ShutdownUnit - terminated - failure - thrown on purpose - x'
    }

    def subject
    def unitName
    def controlPoint

    void setup() {
        controlPoint = Mock(ControlPoint)
        unitName = Mock(UnitName)

        subject = new ShutdownUnit(controlPoint, unitName)
    }
}
