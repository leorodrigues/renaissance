package org.leonardo.renaissance.application.operations

import org.leonardo.renaissance.application.operations.FabricationContext
import org.leonardo.renaissance.application.operations.MakeDurableRepository
import spock.lang.Specification

import static java.nio.file.Files.createTempFile

class MakeDurableRepositorySpec extends Specification {
    def "execute; ok"() {
        when:
        subject.execute()

        then:
        1 * fabricationContext.setDurableRepository(_) >> {}
    }

    def subject
    def fabricationContext

    void setup() {
        def prefix = "test-run-"
        def suffix = ".tmp"
        def path = createTempFile(prefix, suffix).toFile().absolutePath
        fabricationContext = Spy(FabricationContext)
        subject = new MakeDurableRepository(fabricationContext, path)
    }
}
