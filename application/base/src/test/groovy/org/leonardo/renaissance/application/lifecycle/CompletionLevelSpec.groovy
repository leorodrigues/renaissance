package org.leonardo.renaissance.application.lifecycle

import spock.lang.Specification

import static org.leonardo.renaissance.command.CompletionLevel.FULL
import static org.leonardo.renaissance.command.CompletionLevel.NONE
import static org.leonardo.renaissance.command.CompletionLevel.PARTIAL
import static org.leonardo.renaissance.command.CompletionLevel.chooseBasedOnBoolean
import static org.leonardo.renaissance.command.CompletionLevel.chooseBasedOnLevel

class CompletionLevelSpec extends Specification {
    def "chooseBasedOnBoolean; ok"() {
        expect:
        chooseBasedOnBoolean([true, true]) == FULL
        chooseBasedOnBoolean([false, true]) == PARTIAL
        chooseBasedOnBoolean([false, false]) == NONE
    }

    def "chooseBasedOnLevel; ok"() {
        expect:
        chooseBasedOnLevel([FULL, FULL]) == FULL
        chooseBasedOnLevel([FULL, PARTIAL]) == PARTIAL
        chooseBasedOnLevel([FULL, NONE]) == PARTIAL
        chooseBasedOnLevel([PARTIAL, PARTIAL]) == PARTIAL
        chooseBasedOnLevel([PARTIAL, NONE]) == PARTIAL
        chooseBasedOnLevel([FULL, PARTIAL, NONE]) == PARTIAL
        chooseBasedOnLevel([NONE, NONE]) == NONE
    }
}
