package org.leonardo.renaissance.application.operations

import org.leonardo.environment.ControlPoint
import org.leonardo.environment.ControlPointFactory
import org.leonardo.environment.ControlPointStateBank
import org.leonardo.renaissance.application.operations.FabricationContext
import org.leonardo.renaissance.application.operations.MakeControlPoint
import org.leonardo.renaissance.command.SerialCommandRunner
import spock.lang.Specification

class MakeControlPointSpec extends Specification {

    def "execute; ok; only calls the factory"() {
        given:
        def repository = Mock(ControlPointStateBank)

        and:
        def controlPoint = Mock(ControlPoint)

        when:
        subject.execute()

        then:
        1 * fabricationContext.getControlPointStateBank() >> repository
        1 * fabricationContext.setControlPoint(controlPoint)
        1 * controlPointFactory.newControlPointInstance(
                repository, serialCommandRunner) >> controlPoint
    }

    def subject
    def fabricationContext
    def serialCommandRunner
    def controlPointFactory

    void setup() {
        controlPointFactory = Mock(ControlPointFactory)
        serialCommandRunner = Mock(SerialCommandRunner)
        fabricationContext = Spy(FabricationContext)

        subject = new MakeControlPoint(
            fabricationContext, controlPointFactory, serialCommandRunner)
    }
}
