package org.leonardo.renaissance.application.operations

import org.leonardo.davinci.durability.DurableRepository
import org.leonardo.renaissance.application.operations.FabricationContext
import org.leonardo.renaissance.application.operations.MakeCPStateBank
import spock.lang.Specification

class MakeCPStateBankSpec extends Specification {

    def "execute; ok"() {
        given:
        def durableRepository = Mock(DurableRepository)

        when:
        subject.execute()

        then:
        1 * fabricationContext.getDurableRepository() >> durableRepository
        1 * fabricationContext.setControlPointStateBank(_)
    }

    def subject
    def fabricationContext

    void setup() {
        fabricationContext = Mock(FabricationContext)
        subject = new MakeCPStateBank(fabricationContext)
    }
}
