package org.leonardo.renaissance.application;

import org.lenardo.renaissance.collectionworks.Predicate;
import org.leonardo.davinci.durability.Bucket;
import org.leonardo.davinci.durability.DurableRepository;
import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.environment.UnitName;
import org.leonardo.environment.config.Configuration;
import org.leonardo.environment.config.Configurations;
import org.leonardo.environment.config.DefaultConfiguration;
import org.leonardo.renaissance.application.operations.StateBankInsertException;
import org.leonardo.renaissance.application.operations.UnderlyingBankException;
import org.leonardo.renaissance.utils.Optional;
import org.leonardo.renaissance.utils.OptionalList;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

public class ControlPointStateBankImpl implements ControlPointStateBank {

    public static final String BUCKET_NAME = "configurations";

    private final ReentrantLock lock = new ReentrantLock();

    private Bucket bucketInstance;

    private DurableRepository durableRepository;

    public ControlPointStateBankImpl(DurableRepository durableRepository) {
        this.durableRepository = durableRepository;
    }

    @Override
    public Configurations allConfigurationsByName() {
        Bucket bucket = getBucket();

        OptionalList<Record> allRecords = bucket.all();

        if (!allRecords.isPresent())
            return new Configurations();

        Configurations result = new Configurations();
        for (Record r : allRecords.get())
            result.put(r.name, r.configuration);

        return result;
    }

    @Override
    public Set<UnitName> allUnitNames() {
        Bucket bucket = getBucket();

        OptionalList<Record> allRecords = bucket.all();

        if (!allRecords.isPresent())
            return new HashSet<>();

        HashSet<UnitName> result = new HashSet<>();
        for (Record r : allRecords.get())
            result.add(r.name);

        return result;
    }

    @Override
    public void insert(UnitName unitName, Configuration c) {
        try {
            Bucket bucket = getBucket();
            bucket.make(Record.class, unitName, new DefaultConfiguration(c));
        } catch (Throwable e) {
            throw new StateBankInsertException(unitName, e);
        }
    }

    @Override
    public void delete(final UnitName unitName) {
        Bucket bucket = getBucket();

        Optional<Record> record = bucket.applyQuery(new Predicate<Record>() {
            @Override
            public boolean verifies(Record argument) {
                return argument.name.equals(unitName);
            }
        });

        if (!record.isPresent())
            return;

        bucket.remove(record.get());
    }

    @Override
    public void commit() {
        try {
            durableRepository.save();
        } catch (IOException e) {
            throw new UnderlyingBankException(e);
        }
    }

    private Bucket getBucket() {
        try {
            lock.lock();
            if (bucketInstance == null)
                bucketInstance = durableRepository.getBucket(BUCKET_NAME);
            return bucketInstance;
        } finally {
            lock.unlock();
        }
    }

    public static class Record implements Serializable {
        public String id;
        public UnitName name;
        public Configuration configuration;
        public Record(String id, UnitName name, Configuration configuration) {
            this.id = id;
            this.name = name;
            this.configuration = configuration;
        }
    }
}
