package org.leonardo.renaissance.application.operations;

import org.leonardo.environment.UnitName;

import static java.lang.String.format;

public class StateBankInsertException extends RuntimeException {

    private static final String TEMPLATE = "Error inserting '%s': %s";

    public StateBankInsertException(UnitName unitName, Throwable e) {
        super(format(TEMPLATE, unitName.getDisplayString(), e.getMessage()));
    }
}
