package org.leonardo.renaissance.application.lifecycle;

import org.leonardo.environment.ControlPointFactory;
import org.leonardo.renaissance.application.FileSystemLayer;
import org.leonardo.renaissance.application.operations.FabricationContext;
import org.leonardo.renaissance.application.operations.MakeCPStateBank;
import org.leonardo.renaissance.application.operations.MakeControlPoint;
import org.leonardo.renaissance.application.operations.PathComputation;
import org.leonardo.renaissance.command.*;

import java.util.LinkedList;
import java.util.List;

public abstract class LoadingProcedureAssembler {
    private final SerialCommandRunner serialCommandRunner;
    private final ControlPointFactory controlPointFactory;
    private final FileSystemLayer fileSystemLayer;
    private final PathComputation pathComputation;
    private final CommandWeaver commandWeaver;
    private final String homeDirectoryName;
    private final String stateFileName;
    private final String basePath;

    public LoadingProcedureAssembler(
            String basePath,
            String stateFileName,
            String homeDirectoryName,
            CommandWeaver commandWeaver,
            FileSystemLayer fileSystemLayer,
            PathComputation pathComputation,
            ControlPointFactory controlPointFactory,
            SerialCommandRunner serialCommandRunner) {

        this.basePath = basePath;
        this.stateFileName = stateFileName;
        this.commandWeaver = commandWeaver;
        this.fileSystemLayer = fileSystemLayer;
        this.pathComputation = pathComputation;
        this.homeDirectoryName = homeDirectoryName;
        this.controlPointFactory = controlPointFactory;
        this.serialCommandRunner = serialCommandRunner;
    }

    public final ControlPointLoadingProcedure assemble() {
        FabricationContext fabricationContext = new FabricationContext();

        String homePath =
            pathComputation.invoke(basePath, homeDirectoryName);

        String repositoryFilePath =
            pathComputation.invoke(homePath, stateFileName);

        LinkedList<AbstractCommand> commands = new LinkedList<>();

        addPathSetup(
                commands, homePath, repositoryFilePath);

        addUnderlyingRepositorySetup(
                commands, fabricationContext, repositoryFilePath);

        addControlPointStateBankSetup(
                commands, fabricationContext);

        addControlPointInstantiation(
                commands, fabricationContext);

        addPostAssemblySetup(
                commands, fabricationContext);

        AbstractCommand command = commandWeaver.weave(commands).get();

        return new ControlPointLoadingProcedure(
            fabricationContext, serialCommandRunner, command);
    }

    protected ControlPointFactory getControlPointFactory() {
        return controlPointFactory;
    }

    protected FileSystemLayer getFileSystemLayer() {
        return fileSystemLayer;
    }

    protected void addControlPointStateBankSetup(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext) {

        commands.add(
            new MakeCPStateBank(fabricationContext));
    }

    protected void addControlPointInstantiation(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext) {

        commands.add(new MakeControlPoint(
                fabricationContext,
                getControlPointFactory(),
                serialCommandRunner));
    }

    protected abstract void addPathSetup(
            List<AbstractCommand> commands,
            String homeDirectoryPath,
            String repositoryFilePath);

    protected abstract void addUnderlyingRepositorySetup(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext,
            String repositoryFilePath);

    protected abstract void addPostAssemblySetup(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext);
}
