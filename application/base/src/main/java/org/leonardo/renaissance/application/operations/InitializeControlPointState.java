package org.leonardo.renaissance.application.operations;

import org.leonardo.environment.BasicUnitName;
import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.config.NameAlreadyDeclaredException;
import org.leonardo.renaissance.command.AbstractCommand;
import org.leonardo.renaissance.utils.Optional;

import static java.lang.String.format;

public class InitializeControlPointState extends AbstractCommand {

    private static final String ERROR_TEMPLATE =
            "Failed to initialize control point: %s";

    private FabricationContext fabricationContext;

    public InitializeControlPointState(
            FabricationContext fabricationContext) {
        this.fabricationContext = fabricationContext;
    }

    @Override
    public void execute() {
        Optional<ControlPoint> controlPoint =
                fabricationContext.getControlPoint();

        if (!controlPoint.isPresent())
            throw new MissingControlPointException();

        declareBasicUnit(controlPoint.get());
    }

    private void declareBasicUnit(ControlPoint controlPoint) {
        try {
            controlPoint.declareName(BasicUnitName.INSTANCE);
            controlPoint.saveState();
        } catch (NameAlreadyDeclaredException e) {
            String message = format(ERROR_TEMPLATE, e.getMessage());
            throw new ControlPointInitException(message);
        }
    }
}
