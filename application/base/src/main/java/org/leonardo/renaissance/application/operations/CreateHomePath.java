package org.leonardo.renaissance.application.operations;

import org.leonardo.renaissance.application.FileSystemLayer;
import org.leonardo.renaissance.command.AbstractCommand;

import java.io.File;

public class CreateHomePath extends AbstractCommand {

    private String homePath;

    private FileSystemLayer fileSystemLayer;

    public CreateHomePath(
            String homePath,
            FileSystemLayer fileSystemLayer) {

        this.homePath = homePath;
        this.fileSystemLayer = fileSystemLayer;
    }

    @Override
    public void execute() {
        File homePathFile = new File(homePath);

        if (fileSystemLayer.directoryExists(homePathFile)) {
            reportWarning("Home path already exists. Nothing to be done.");
            return;
        }

        if (fileSystemLayer.makeDirectories(homePathFile)) {
            reportInfo("Home path created");
            return;
        }

        throw new UnsuitablePathException(homePathFile);
    }


}
