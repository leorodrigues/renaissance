package org.leonardo.renaissance.application.operations;

import static java.lang.String.format;

public class UnderlyingBankException extends RuntimeException {

    private static final String TEMPLATE =
            "An underlying bank exception has occurred: %s";

    public UnderlyingBankException(Throwable cause) {
        super(format(TEMPLATE, cause.getMessage()));
    }
}
