package org.leonardo.renaissance.application.operations;

import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.command.AbstractCommand;

import static java.lang.String.format;

public class ShutdownUnit extends AbstractCommand {

    private ControlPoint controlPoint;
    private UnitName name;

    public ShutdownUnit(ControlPoint controlPoint, UnitName name) {
        this.controlPoint = controlPoint;
        this.name = name;
    }

    @Override
    public void execute() {
        controlPoint.shutdownUnit(name);
    }

    @Override
    public String getSuccessMessage() {
        return format("%s - %s", super.getSuccessMessage(),
            name.getDisplayString());
    }

    @Override
    public String getErrorMessage(Throwable error) {
        return format("%s - %s", super.getErrorMessage(error),
            name.getDisplayString());
    }
}
