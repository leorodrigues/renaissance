package org.leonardo.renaissance.application;

import org.leonardo.renaissance.application.lifecycle.ControlPointShutdownProcedure;
import org.leonardo.renaissance.application.lifecycle.LoadingProcedureAssembler;
import org.leonardo.renaissance.command.FeedbackListener;
import org.leonardo.renaissance.command.SerialCommandRunner;

import java.util.logging.Logger;

public interface ControlPointKit {

    FeedbackListener makeFeedbackListener(Logger logger);

    SerialCommandRunner makeSerialCommandRunner(
        FeedbackListener feedbackListener);

    LoadingProcedureAssembler makeSetupAssembler(
        FeedbackListener feedbackListener,
        SerialCommandRunner serialCommandRunner);

    LoadingProcedureAssembler makeStartupAssembler(
        FeedbackListener feedbackListener,
        SerialCommandRunner serialCommandRunner);

    ControlPointShutdownProcedure makeShutdownProcedure(
            FeedbackListener feedbackListener,
            SerialCommandRunner serialCommandRunner);
}
