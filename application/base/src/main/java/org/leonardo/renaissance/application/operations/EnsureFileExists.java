package org.leonardo.renaissance.application.operations;

import org.leonardo.renaissance.application.FileSystemLayer;
import org.leonardo.renaissance.command.AbstractCommand;

import java.io.File;

public class EnsureFileExists extends AbstractCommand {

    private String repositoryFilePath;

    private FileSystemLayer fileSystemLayer;

    public EnsureFileExists(
            String repositoryFilePath, FileSystemLayer fileSystemLayer) {
        this.repositoryFilePath = repositoryFilePath;
        this.fileSystemLayer = fileSystemLayer;
    }

    @Override
    public void execute() {
        File file = new File(repositoryFilePath);
        if (fileSystemLayer.fileExists(file)) {
            reportInfo("State file found.", repositoryFilePath);
            return;
        }
        throw new UnsuitablePathException(file);
    }
}
