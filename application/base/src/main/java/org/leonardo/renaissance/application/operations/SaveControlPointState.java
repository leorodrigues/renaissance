package org.leonardo.renaissance.application.operations;

import org.leonardo.environment.ControlPoint;
import org.leonardo.renaissance.command.AbstractCommand;

public class SaveControlPointState extends AbstractCommand {

    private ControlPoint controlPoint;

    public SaveControlPointState(ControlPoint controlPoint) {
        this.controlPoint = controlPoint;
    }

    @Override
    public void execute() {
        controlPoint.saveState();
    }
}
