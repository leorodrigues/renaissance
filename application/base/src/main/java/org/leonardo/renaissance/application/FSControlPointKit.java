package org.leonardo.renaissance.application;

import org.leonardo.environment.ControlPointFactoryImpl;
import org.leonardo.environment.rhino.RhinoControlPointFactory;
import org.leonardo.renaissance.application.lifecycle.ControlPointShutdownProcedure;
import org.leonardo.renaissance.application.lifecycle.LoadingProcedureAssembler;
import org.leonardo.renaissance.application.operations.PathComputation;
import org.leonardo.renaissance.application.lifecycle.SetupAssembler;
import org.leonardo.renaissance.application.lifecycle.StartupAssembler;
import org.leonardo.renaissance.command.*;

import java.util.logging.Logger;

public class FSControlPointKit implements ControlPointKit {

    private static final FileSystemLayerImpl fileSystemLayer =
            new FileSystemLayerImpl();

    private static final RhinoControlPointFactory controlPointFactory =
            new RhinoControlPointFactory(new ControlPointFactoryImpl());

    private static final PathComputation pathComputation =
            new PathComputation();

    private final String basePath;
    private final String stateFileName;
    private final String homeDirectoryName;

    public FSControlPointKit(
        String basePath,
        String stateFileName,
        String homeDirectoryName) {

        this.basePath = basePath;
        this.stateFileName = stateFileName;
        this.homeDirectoryName = homeDirectoryName;
    }

    @Override
    public FeedbackListener makeFeedbackListener(Logger logger) {
        return new LoggerFeedbackListener(logger);
    }

    @Override
    public SerialCommandRunner makeSerialCommandRunner(
        FeedbackListener feedbackListener) {

        return new SerialCommandRunner(
            new FrailChainRunner(
                new RunWithRecovery(feedbackListener)));
    }

    @Override
    public LoadingProcedureAssembler makeSetupAssembler(
        FeedbackListener feedbackListener,
        SerialCommandRunner serialCommandRunner) {

        return new SetupAssembler(
            basePath,
            stateFileName,
            homeDirectoryName,
            new CommandWeaverImpl(feedbackListener),
            pathComputation,
            fileSystemLayer,
            controlPointFactory,
            serialCommandRunner);
    }

    @Override
    public LoadingProcedureAssembler makeStartupAssembler(
        FeedbackListener feedbackListener,
        SerialCommandRunner serialCommandRunner) {

        return new StartupAssembler(
            basePath,
            stateFileName,
            homeDirectoryName,
            new CommandWeaverImpl(feedbackListener),
            pathComputation,
            fileSystemLayer,
            controlPointFactory,
            serialCommandRunner);
    }

    @Override
    public ControlPointShutdownProcedure makeShutdownProcedure(
        FeedbackListener feedbackListener,
        SerialCommandRunner serialCommandRunner) {

        return new ControlPointShutdownProcedure(
            serialCommandRunner, new CommandWeaverImpl(feedbackListener));
    }
}
