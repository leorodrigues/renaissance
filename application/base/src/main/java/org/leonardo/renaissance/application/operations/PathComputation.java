package org.leonardo.renaissance.application.operations;

import java.io.File;

import static java.util.Arrays.asList;
import static org.lenardo.renaissance.collectionworks.ComplementaryCollections.join;

public class PathComputation {
    public String invoke(String baseDirectory, String baseName) {
        return join(asList(baseDirectory, baseName), File.separator);
    }
}
