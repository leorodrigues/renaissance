package org.leonardo.renaissance.application.lifecycle;

import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.UnitName;
import org.leonardo.renaissance.application.operations.SaveControlPointState;
import org.leonardo.renaissance.application.operations.ShutdownUnit;
import org.leonardo.renaissance.command.AbstractCommand;
import org.leonardo.renaissance.command.CommandWeaver;
import org.leonardo.renaissance.command.CompletionLevel;
import org.leonardo.renaissance.command.SerialCommandRunner;
import org.leonardo.renaissance.utils.Optional;

import java.util.LinkedList;

import static java.lang.String.format;
import static org.leonardo.renaissance.command.CompletionLevel.FULL;

public class ControlPointShutdownProcedure {

    private static final String COMPLETION_ERROR_TEMPLATE =
        "Command sequence completion level was '%s'.";

    private final SerialCommandRunner commandRunner;
    private final CommandWeaver commandWeaver;

    public ControlPointShutdownProcedure(
            SerialCommandRunner commandRunner,
            CommandWeaver commandWeaver) {
        this.commandRunner = commandRunner;
        this.commandWeaver = commandWeaver;
    }

    public void execute(ControlPoint controlPoint, boolean saveState) {

        UnitName[] names = controlPoint.getRunningUnitsNames();

        Optional<AbstractCommand> abstractCommand =
                weaveShutdownCommands(controlPoint, names, saveState);

        if (!abstractCommand.isPresent())
            return;

        CompletionLevel completionLevel =
                commandRunner.run(abstractCommand.get());

        if (completionLevel != FULL)
            throw new ControlPointLifeCycleException(
                makeCompletionLevelErrorMessage(completionLevel));
    }

    private Optional<AbstractCommand> weaveShutdownCommands(
            ControlPoint controlPoint, UnitName[] names, boolean saveState) {
        LinkedList<AbstractCommand> commands = new LinkedList<>();
        for (UnitName n : names)
            commands.add(new ShutdownUnit(controlPoint, n));
        if (saveState)
            commands.add(new SaveControlPointState(controlPoint));
        return commandWeaver.weave(commands);
    }

    private String makeCompletionLevelErrorMessage(
            CompletionLevel completionLevel) {
        return format(COMPLETION_ERROR_TEMPLATE, completionLevel);
    }
}
