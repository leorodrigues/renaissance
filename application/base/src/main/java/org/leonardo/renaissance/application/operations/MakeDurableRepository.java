package org.leonardo.renaissance.application.operations;


import org.leonardo.davinci.durability.DurableRepository;
import org.leonardo.davinci.durability.FileSystemRepositoryKit;
import org.leonardo.davinci.durability.UnderlyingStorage;
import org.leonardo.renaissance.command.AbstractCommand;

import java.io.File;

public class MakeDurableRepository extends AbstractCommand {

    private FabricationContext fabricationContext;

    private String repositoryFilePath;

    public MakeDurableRepository(
            FabricationContext fabricationContext,
            String repositoryFilePath) {
        this.fabricationContext = fabricationContext;
        this.repositoryFilePath = repositoryFilePath;
    }

    @Override
    public void execute() {
        FileSystemRepositoryKit fileSystemRepositoryKit =
            new FileSystemRepositoryKit(new File(repositoryFilePath));

        UnderlyingStorage underlyingStorage =
            fileSystemRepositoryKit.makeUnderlyingStorage();

        DurableRepository durableRepository =
            fileSystemRepositoryKit.makeRepository(underlyingStorage);

        fabricationContext.setDurableRepository(durableRepository);
    }
}
