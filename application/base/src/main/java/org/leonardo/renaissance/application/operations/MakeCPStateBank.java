package org.leonardo.renaissance.application.operations;

import org.leonardo.davinci.durability.DurableRepository;
import org.leonardo.renaissance.application.ControlPointStateBankImpl;
import org.leonardo.renaissance.command.AbstractCommand;

public class MakeCPStateBank extends AbstractCommand {

    private FabricationContext fabricationContext;

    public MakeCPStateBank(
            FabricationContext fabricationContext) {
        this.fabricationContext = fabricationContext;
    }

    @Override
    public void execute() {
        DurableRepository durableRepository =
            fabricationContext.getDurableRepository();

        fabricationContext.setControlPointStateBank(
            new ControlPointStateBankImpl(durableRepository));
    }
}
