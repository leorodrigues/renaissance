package org.leonardo.renaissance.application.lifecycle;

import org.leonardo.environment.ControlPointFactory;
import org.leonardo.renaissance.application.FileSystemLayer;
import org.leonardo.renaissance.application.operations.*;
import org.leonardo.renaissance.command.*;

import java.util.List;

public class StartupAssembler extends LoadingProcedureAssembler {

    public StartupAssembler(
            String basePath,
            String stateFileName,
            String homeDirectoryName,
            CommandWeaver commandWeaver,
            PathComputation pathComputation,
            FileSystemLayer fileSystemLayer,
            ControlPointFactory controlPointFactory,
            SerialCommandRunner serialCommandRunner) {

        super(basePath, stateFileName, homeDirectoryName, commandWeaver,
            fileSystemLayer, pathComputation, controlPointFactory,
                serialCommandRunner);
    }

    @Override
    protected void addPathSetup(
            List<AbstractCommand> commands,
            String homeDirectoryPath,
            String repositoryFilePath) {

        ConfirmHomePathExistence confirmHomePathExistence =
            new ConfirmHomePathExistence(
                homeDirectoryPath, getFileSystemLayer());
        commands.add(confirmHomePathExistence);

        AbstractCommand stateFileSetup = new EnsureFileExists(
                repositoryFilePath, getFileSystemLayer());
        commands.add(stateFileSetup);
    }

    @Override
    protected void addUnderlyingRepositorySetup(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext,
            String repositoryFilePath) {

        commands.add(new MakeDurableRepository(
            fabricationContext, repositoryFilePath));
    }

    @Override
    protected void addPostAssemblySetup(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext) {

        commands.add(
            new LoadControlPointState(fabricationContext));
    }
}
