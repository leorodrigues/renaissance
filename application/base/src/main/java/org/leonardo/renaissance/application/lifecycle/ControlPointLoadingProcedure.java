package org.leonardo.renaissance.application.lifecycle;

import org.leonardo.environment.ControlPoint;
import org.leonardo.renaissance.application.operations.FabricationContext;
import org.leonardo.renaissance.command.Command;
import org.leonardo.renaissance.command.CompletionLevel;
import org.leonardo.renaissance.command.SerialCommandRunner;
import org.leonardo.renaissance.utils.Optional;

import java.util.List;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.leonardo.renaissance.command.CompletionLevel.FULL;

public class ControlPointLoadingProcedure {

    private static final String COMPLETION_ERROR_TEMPLATE =
        "Command sequence completion level was '%s'.";

    private static final String CONTROL_POINT_MISSING_MESSAGE =
        "Control point is missing after initialization sequence.";

    private final FabricationContext fabricationContext;
    private final SerialCommandRunner commandRunner;
    private List<Command> commands;

    public ControlPointLoadingProcedure(
            FabricationContext fabricationContext,
            SerialCommandRunner commandRunner,
            Command ...commands) {
        this.fabricationContext = fabricationContext;
        this.commandRunner = commandRunner;
        this.commands = asList(commands);
    }

    public ControlPoint execute() {
        CompletionLevel completionLevel = commandRunner.run(commands);

        if (completionLevel != FULL)
            throw new ControlPointLifeCycleException(
                makeCompletionLevelErrorMessage(completionLevel));

        Optional<ControlPoint> controlPoint =
            fabricationContext.getControlPoint();

        if (!controlPoint.isPresent())
            throw new ControlPointLifeCycleException(CONTROL_POINT_MISSING_MESSAGE);

        return controlPoint.get();
    }

    private String makeCompletionLevelErrorMessage(
            CompletionLevel completionLevel) {
        return format(COMPLETION_ERROR_TEMPLATE, completionLevel);
    }
}
