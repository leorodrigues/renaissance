package org.leonardo.renaissance.application.operations;

import org.leonardo.davinci.durability.DurableRepository;
import org.leonardo.davinci.durability.UUIDStringKeyMaker;
import org.leonardo.renaissance.command.AbstractCommand;

import java.io.IOException;

import static org.leonardo.renaissance.application.ControlPointStateBankImpl.BUCKET_NAME;

public class InitializeDurableRepository extends AbstractCommand {

    private FabricationContext fabricationContext;

    public InitializeDurableRepository(
            FabricationContext fabricationContext) {
        this.fabricationContext = fabricationContext;
    }

    @Override
    public void execute() {
        try {
            DurableRepository durableRepository =
                fabricationContext.getDurableRepository();

            durableRepository.newBucket(BUCKET_NAME, new UUIDStringKeyMaker());
            durableRepository.save();

        } catch (IOException e) {
            throw new RepositoryInitializationException(e);
        }
    }
}
