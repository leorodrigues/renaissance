package org.leonardo.renaissance.application;

import org.leonardo.renaissance.command.Feedback;
import org.leonardo.renaissance.command.FeedbackListener;

import java.util.logging.Logger;

public class LoggerFeedbackListener implements FeedbackListener {
    private Logger logger;

    LoggerFeedbackListener(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void register(Feedback feedBack) {
        switch (feedBack.getKind()) {
            case WARNING: logger.warning(feedBack.getMessage()); break;
            case ERROR: logger.severe(feedBack.getMessage()); break;
            default: logger.info(feedBack.getMessage());
        }
    }
}
