package org.leonardo.renaissance.application;

import java.io.File;

public interface FileSystemLayer {
    boolean fileExists(File filePath);
    boolean deleteFile(File filePath);
    boolean directoryExists(File directoryPath);
    boolean makeDirectories(File directoryPath);
}
