package org.leonardo.renaissance.application.lifecycle;

import org.leonardo.environment.ControlPointFactory;
import org.leonardo.renaissance.application.FileSystemLayer;
import org.leonardo.renaissance.application.operations.*;
import org.leonardo.renaissance.command.AbstractCommand;
import org.leonardo.renaissance.command.CommandWeaver;
import org.leonardo.renaissance.command.SerialCommandRunner;

import java.util.List;

public class SetupAssembler extends LoadingProcedureAssembler {

    public SetupAssembler(
            String basePath,
            String stateFileName,
            String homeDirectoryName,
            CommandWeaver commandWeaver,
            PathComputation pathComputation,
            FileSystemLayer fileSystemLayer,
            ControlPointFactory controlPointFactory,
            SerialCommandRunner serialCommandRunner) {

        super(basePath, stateFileName, homeDirectoryName, commandWeaver,
            fileSystemLayer, pathComputation, controlPointFactory,
                serialCommandRunner);
    }

    @Override
    protected void addPathSetup(
            List<AbstractCommand> commands,
            String homeDirectoryPath,
            String repositoryFilePath) {

        CreateHomePath createHomePath = new CreateHomePath(
                homeDirectoryPath, getFileSystemLayer());
        commands.add(createHomePath);

        RemoveOldFile removeOldFile = new RemoveOldFile(
            repositoryFilePath, getFileSystemLayer());
        commands.add(removeOldFile);

    }

    @Override
    protected void addUnderlyingRepositorySetup(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext,
            String repositoryFilePath) {

        commands.add(
            new MakeDurableRepository(
                fabricationContext, repositoryFilePath));

        commands.add(
            new InitializeDurableRepository(fabricationContext));
    }

    @Override
    protected void addPostAssemblySetup(
            List<AbstractCommand> commands,
            FabricationContext fabricationContext) {

        commands.add(
            new InitializeControlPointState(fabricationContext));
    }
}
