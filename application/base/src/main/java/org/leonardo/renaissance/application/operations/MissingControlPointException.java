package org.leonardo.renaissance.application.operations;

public class MissingControlPointException extends RuntimeException {
    public MissingControlPointException() {
        super("Control point is missing.");
    }
}
