package org.leonardo.renaissance.application;

import java.io.File;

public class FileSystemLayerImpl implements FileSystemLayer {
    @Override
    public boolean directoryExists(File directoryPath) {
        return directoryPath.exists();
    }

    @Override
    public boolean makeDirectories(File directoryPath) {
        return directoryPath.mkdirs();
    }

    @Override
    public boolean fileExists(File filePath) {
        return filePath.exists();
    }

    @Override
    public boolean deleteFile(File filePath) {
        return filePath.delete();
    }
}
