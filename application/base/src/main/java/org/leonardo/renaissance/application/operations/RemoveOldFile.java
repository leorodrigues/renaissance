package org.leonardo.renaissance.application.operations;

import org.leonardo.renaissance.application.FileSystemLayer;
import org.leonardo.renaissance.command.AbstractCommand;

import java.io.File;

public class RemoveOldFile extends AbstractCommand {

    public static final String NOT_FOUND_MSG =
            "Old file not found. nothing to be done.";

    private String repositoryFilePath;

    private FileSystemLayer fileSystemLayer;

    public RemoveOldFile(
            String repositoryFilePath, FileSystemLayer fileSystemLayer) {
        this.repositoryFilePath = repositoryFilePath;
        this.fileSystemLayer = fileSystemLayer;
    }

    @Override
    public void execute() {
        File file = new File(repositoryFilePath);

        if (!fileSystemLayer.fileExists(file)) {
            reportInfo(NOT_FOUND_MSG, repositoryFilePath);
            return;
        }

        reportWarning("Old file found.", repositoryFilePath);

        if (fileSystemLayer.deleteFile(file)) {
            reportInfo("Old file removed.");
            return;
        }

        throw new UnsuitablePathException(file);
    }
}
