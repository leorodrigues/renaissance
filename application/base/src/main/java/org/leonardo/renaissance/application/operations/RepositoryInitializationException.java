package org.leonardo.renaissance.application.operations;

public class RepositoryInitializationException extends RuntimeException {
    public RepositoryInitializationException(Exception cause) {
        super(cause);
    }
}
