package org.leonardo.renaissance.application.operations;

import java.io.File;

import static java.lang.String.format;

public class UnsuitablePathException extends RuntimeException {
    public UnsuitablePathException(File path) {
        super(format("Path '%s' is unsuitable.", path.getAbsolutePath()));
    }
}
