package org.leonardo.renaissance.application.operations;

import org.leonardo.davinci.durability.DurableRepository;
import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.renaissance.utils.Optional;

public class FabricationContext {
    private ControlPointStateBank controlPointStateBank;
    private DurableRepository durableRepository;
    private ControlPoint controlPoint;

    public void setDurableRepository(DurableRepository durableRepository) {
        this.durableRepository = durableRepository;
    }

    public DurableRepository getDurableRepository() {
        return durableRepository;
    }

    public void setControlPointStateBank(
        ControlPointStateBank controlPointStateBank) {
        this.controlPointStateBank = controlPointStateBank;
    }

    public ControlPointStateBank getControlPointStateBank() {
        return controlPointStateBank;
    }

    public void setControlPoint(ControlPoint controlPoint) {
        this.controlPoint = controlPoint;
    }

    public Optional<ControlPoint> getControlPoint() {
        return new Optional<>(controlPoint);
    }

}
