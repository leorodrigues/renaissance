package org.leonardo.renaissance.application.operations;

import org.leonardo.renaissance.application.FileSystemLayer;
import org.leonardo.renaissance.command.AbstractCommand;

import java.io.File;

public class ConfirmHomePathExistence extends AbstractCommand {

    private static final String HOME_DIRECTORY_FOUND_MSG =
            "Home directory found.";

    private final String homePath;

    private final FileSystemLayer fileSystemLayer;

    public ConfirmHomePathExistence(
            String homePath,
            FileSystemLayer fileSystemLayer) {
        this.homePath = homePath;
        this.fileSystemLayer = fileSystemLayer;
    }

    @Override
    public void execute() {
        File homePathFile = new File(homePath);

        if (fileSystemLayer.directoryExists(homePathFile)) {
            reportInfo(HOME_DIRECTORY_FOUND_MSG, homePath);
            return;
        }

        throw new UnsuitablePathException(homePathFile);
    }
}
