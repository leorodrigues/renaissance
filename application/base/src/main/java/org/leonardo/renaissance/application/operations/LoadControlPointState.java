package org.leonardo.renaissance.application.operations;

import org.leonardo.environment.ControlPoint;
import org.leonardo.renaissance.command.AbstractCommand;
import org.leonardo.renaissance.utils.Optional;

public class LoadControlPointState extends AbstractCommand {

    private FabricationContext fabricationContext;

    public LoadControlPointState(FabricationContext fabricationContext) {
        this.fabricationContext = fabricationContext;
    }

    @Override
    public void execute() {
        Optional<ControlPoint> controlPoint =
                fabricationContext.getControlPoint();

        if (!controlPoint.isPresent())
            throw new MissingControlPointException();

        controlPoint.get().loadState();
    }
}
