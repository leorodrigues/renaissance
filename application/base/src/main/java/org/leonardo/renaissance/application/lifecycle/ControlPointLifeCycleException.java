package org.leonardo.renaissance.application.lifecycle;

public class ControlPointLifeCycleException extends RuntimeException {
    public ControlPointLifeCycleException(String message) {
        super(message);
    }
}
