package org.leonardo.renaissance.application.operations;

import org.leonardo.environment.ControlPoint;
import org.leonardo.environment.ControlPointFactory;
import org.leonardo.environment.ControlPointStateBank;
import org.leonardo.renaissance.command.*;

public class MakeControlPoint extends AbstractCommand {

    private ControlPointFactory controlPointFactory;
    private FabricationContext fabricationContext;
    private SerialCommandRunner serialCommandRunner;

    public MakeControlPoint(
            FabricationContext fabricationContext,
            ControlPointFactory controlPointFactory,
            SerialCommandRunner serialCommandRunner) {

        this.controlPointFactory = controlPointFactory;
        this.fabricationContext = fabricationContext;
        this.serialCommandRunner = serialCommandRunner;
    }

    @Override
    public void execute() {
        ControlPointStateBank controlPointStateBank =
            fabricationContext.getControlPointStateBank();

        ControlPoint controlPoint =
            controlPointFactory.newControlPointInstance(
                controlPointStateBank, serialCommandRunner);

        fabricationContext.setControlPoint(controlPoint);
    }
}
